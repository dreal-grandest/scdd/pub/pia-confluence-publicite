export class FormValueConverterHelper {
  static convertToNumber(value: string | number | null | undefined): number {
    if (typeof value === 'number') {
      return value;
    } else if (typeof value === 'string') {
      return Number.parseFloat(value.replace(',', '.'));
    } else {
      return 0;
    }
  }
}
