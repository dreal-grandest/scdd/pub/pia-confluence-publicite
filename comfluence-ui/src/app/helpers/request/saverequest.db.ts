import { Injectable } from '@angular/core';
import Dexie from 'dexie';

export class RequestEntity {
    id?: number;
    action: string;
    data?: any;
}

@Injectable({
    providedIn: 'root'
})
export class RequestDatabase {
    private database: Dexie;
    constructor() {
        this.database = new Dexie('ComfluenceDbRequest');
        this.initV1();
    }
    private initV1() {
        this.database.version(1).stores({
            request: '++id,action,data'
        });
    }

    private initV2() {
        this.database.version(2).stores({
            request: '++id,action,data'
        }).upgrade(transaction => {
            console.log('new db version applyed');
            return transaction;
        });
    }
    get requestTable(): Dexie.Table<RequestEntity, number> {
        return this.database['request'];
    }
}
