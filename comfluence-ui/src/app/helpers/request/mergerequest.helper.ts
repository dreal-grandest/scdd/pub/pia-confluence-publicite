
import { Injectable } from '@angular/core';
import { Device } from 'src/app/models/device/device';
import { ActionsConstantes } from '../action.constants';
import { RequestDatabase } from './saverequest.db';

@Injectable({
    providedIn: 'root'
})
export class MergeRequestHelper {
    constructor(private requestDatabase: RequestDatabase) {
    }
    mergeWithSavedDevices(deviceList: Device[]): Promise<Device[]> {
        const savedDeviceRequest = this.requestDatabase.requestTable.where('action').equals(ActionsConstantes.ACTION_DEVICE_SAVE).toArray();
        return savedDeviceRequest.then(requestList => {
            requestList.forEach(request => {
                const savedDevice = <Device>request.data;
                const deviceIndex = deviceList.findIndex(itDevice => itDevice.id === savedDevice.id);
                if (deviceIndex >= 0) {
                    deviceList[deviceIndex] = savedDevice;
                } else {
                    deviceList.push(savedDevice);
                }
            });
            return deviceList;
        }).catch(error => deviceList);
    }

    mergeWithSavedDevice(device: Device): Promise<Device> {
        const savedDeviceRequest = this.requestDatabase.requestTable.where('action').equals(ActionsConstantes.ACTION_DEVICE_SAVE).toArray();
        return savedDeviceRequest.then(requestList => {
            const foundDevice = requestList.find(request => (<Device>request.data).id === device.id
                || (<Device>request.data).unSyncId === device.unSyncId);
            if (foundDevice) {
                return foundDevice.data;
            } else {
                return device;
            }
        }).catch(error => device);
    }
}
