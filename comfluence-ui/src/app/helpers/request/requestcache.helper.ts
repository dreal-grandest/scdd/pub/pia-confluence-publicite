
import { Injectable } from '@angular/core';
import { finalize, first } from 'rxjs/operators';
import { StateService } from 'src/app/services/state.service';
import { ActionHelper } from '../action.helper';
import { RequestDatabase, RequestEntity } from './saverequest.db';

@Injectable({
    providedIn: 'root'
})
export class RetryRequestHelper {
    private retrying = false;
    constructor(private requestDatabase: RequestDatabase,
        private actionHelper: ActionHelper,
        private stateService: StateService) {
        this.requestDatabase.requestTable.toArray().then((all: RequestEntity[]) => {
            if (all) {
                this.stateService.setUnscynCount(all.length);
            }
        });
    }
    retryRequests() {
        if (!this.retrying) {
            this.retrying = true;
            this.requestDatabase.requestTable.toArray()
                .then((allRequest: RequestEntity[]) => {
                    if (allRequest) {
                        let numberOfRequestsDone = 0;
                        allRequest.forEach(request => {
                            this.actionHelper.doAction(request.action, request.data).pipe(first(), finalize(() => {
                                numberOfRequestsDone++;
                                this.stateService.updateUnsyncValue(false);
                                if (numberOfRequestsDone >= allRequest.length) {
                                    this.retrying = false;
                                }
                            })).subscribe(() => {
                                console.log('[TRY]' + request.id + ': ' + request.action + ' [' + request.data + ']');
                            });
                            this.requestDatabase.requestTable.delete(request.id);
                        });
                    } else {
                        this.retrying = false;
                    }
                })
                .catch(error => console.error('Erreyr while retrieving chached request' + error));
        }
    }
}
