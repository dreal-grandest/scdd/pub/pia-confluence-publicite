export class Coordinates {
    id?: number;
    latitude: number;
    longitude: number;
}
