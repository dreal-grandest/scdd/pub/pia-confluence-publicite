import { Procedure } from '../Procedure';

export class Infrigement {
  id: number;
  natinfCode: string;
  offenseNature: string;
  impliedProcedure?: Procedure;
  comment?: string;
  status: InfrigementStatus;
}

export enum InfrigementStatus {
  SAVED = 'SAVED',
  TO_DELETE = 'TO_DELETE',
  TO_SAVE = 'TO_SAVE'
}
