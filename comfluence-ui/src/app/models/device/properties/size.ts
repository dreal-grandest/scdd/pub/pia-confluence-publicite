export class Size {
  id: number;
  width: number;
  height: number;
  deviceGroundDistance: number;
  mastWidth: number;
  protrusionGroundDistance: number;
  category: Category;
}

export enum Category {
  NONE = 'NONE',
  SMALL = 'SMALL',
  MEDIUM = 'MEDIUM',
  LARGE = 'LARGE',
  XLARGE = 'XLARGE'
}

