import { Dimension } from './dimension';
import { Advertiser } from '../../../shared/services/advertiser.service';

export class Description {
  id: number;
  positions: Position[];
  advertiser: Advertiser;
  announcer: string;
  dimension: Dimension;
  derogatoryType?: DeragotoryType;
}

export enum Position {
  WALL = 'WALL',
  FLOOR = 'FLOOR',
  BRIGHT = 'BRIGHT',
  NUMERIC = 'NUMERIC',
  TREE = 'TREE',
  POLE = 'POLE',
  PUBLIC_FURNITURE = 'PUBLIC_FURNITURE',
  URBAN_FURNITURE= 'URBAN_FURNITURE',
  TARPAULIN = 'TARPAULIN',
  VEHICULE = 'VEHICULE',
  PLANTATION = 'PLANTATION',
  OTHER_SUPPORT = 'OTHER_SUPPORT',
  FACADE = 'FACADE',
  BLIND_WALL = 'BLIND_WALL',
  NOT_BLIND_WALL = 'NOT_BLIND_WALL',
  CEMETERY = 'CEMETERY',
  PUBLIC_GARDEN = 'PUBLIC_GARDEN',
  BLIND_FENCE = 'BLIND_FENCE',
  NOT_BLIND_FENCE = 'NOT_BLIND_FENCE',
  ROOF = 'ROOF'
}

export enum DeragotoryType {
  MH = 'MH',
  TO_DETERMINE = 'TO_DETERMINE',
  CULTURAL_ACTIVITY = 'CULTURAL_ACTIVITY',
  TEMPORARY = 'TEMPORARY'
}
