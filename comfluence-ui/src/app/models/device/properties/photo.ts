export enum PhotoStatus {
  SAVED = 'SAVED',
  TO_DELETE = 'TO_DELETE',
  TO_SAVE = 'TO_SAVE'
}

export class Photo {
  constructor(aspect: Aspect) {
    this.photoType = aspect;
  }

  id: number;
  idDispositif: number;
  photoDate: string;
  photoType: Aspect;
  imageFile: String;
  imageLinked: boolean;
  status = PhotoStatus.TO_SAVE;
  active = true;
}

export enum Aspect {
  ALL = 'ALL',
  NEAR = 'NEAR',
  BUTEAU = 'BUTEAU',
  DOUBLE_FACE = 'DOUBLE_FACE',
  REEXAMINATION = 'REEXAMINATION'
}
