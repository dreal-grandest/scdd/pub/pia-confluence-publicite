import { Size } from './size';

export class Dimension {
    id: number;
    isEstimated: boolean;
    size: Size;
}
