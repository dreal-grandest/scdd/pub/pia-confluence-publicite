export class Filter {
  id: number;
  label: string;
  needReload: boolean;
}
