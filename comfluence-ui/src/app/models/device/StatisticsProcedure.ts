export class StatisticsProcedure {
  label: string;
  quantity: number;
  filterId: number[];
}
