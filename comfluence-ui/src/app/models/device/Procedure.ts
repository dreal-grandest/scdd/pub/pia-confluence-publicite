import { Filter } from './Filter';

export class Procedure {
  id: number;
  label: string;
  selected = false;
  delay: number;
  incompatibleProcedures: Procedure[];
  dispositifFilters: Filter[];
  groupeId: number;
}

export class ProcedureGroupe {
  id: number;
  name: string;
  previousId: number;
}
