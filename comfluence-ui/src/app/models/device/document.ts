import { ChangeType } from '../ChangeType';
import { InformationType } from './InformationType';
import { Procedure } from './Procedure';

export class Document {
  id: number;
  comment: string;
  addedDate: Date;
  type: InformationType;
  idDevice: number;
  file: File;
  hasDoc: boolean;
  changeType: ChangeType;
  delay: number;
  procedure: Procedure;
  closeDate: Date;
}
