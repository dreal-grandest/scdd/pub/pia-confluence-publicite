export class Statistics {
  compliantDevices: number;
  nonCompliantDevices: number;
  todoDevices: number;
  adDevices: number;
  signboardDevices: number;
  preSignboardDevices: number;
  lastCall: Date;
  departementId: number;
}
