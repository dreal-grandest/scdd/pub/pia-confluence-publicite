declare module InseeGeo {

    export interface Geometry {
        type: string; /** Point */
        coordinates: number[
            /** lat */
            /** lon */
        ];
    }

    export interface Properties {
        label: string; /** libellé complet de l’adresse */
        score: number; /** valeur de 0 à 1 indiquant la pertinence du résultat */
        housenumber: string; /** numéro avec indice de répétition éventuel (bis, ter, A, B) */
        id: string; /** identifiant de l’adresse (clef d’interopérabilité)*/
        type: Type; /** type de résultat trouvé */
        name: string; /* numéro éventuel et nom de voie ou lieu dit */
        postcode: number; /**  code postal */
        citycode: string; /** code INSEE de la commune */
        x: number; /**  coordonnées géographique en projection légale */
        y: number; /** coordonnées géographique en projection légale */
        city: string; /** nom de la commune */
        context: string; /** n° de département, nom de département et de région */
        importance: number; /** indicateur d’importance (champ technique) */
        street: string; /** rue */
    }

    export interface Feature {
        type: string; /** Feature */
        geometry: Geometry; /** Coordinate */
        properties: Properties;
    }

    export interface SearchResult {
        type: string; /** Feature Collection */
        version: string; /** version */
        features: Feature[]; /** résultat de la recherche */
        attribution: string; /** BAN ??? */
        licence: string; /** la licence */
        query: string; /** recherche de départ */
        limit: number; /** limit du résultat */
    }

    export enum Type {
        HOUSENUMBER = 'housenumber ', /* numéro « à la plaque » */
        STREET = 'street', /* position « à la voie », placé approximativement au centre de celle-ci */
        LOCALITY = 'locality', /* lieu-dit */
        MUNICIPALITY = 'municipality' /*  numéro « à la commune » */
    }
}

export { InseeGeo };
