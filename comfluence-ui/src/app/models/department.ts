import { Box2154 } from './box';

export class Department {
  code: number;
  name: string;
  box: Box2154;
}

export class TerritorialDirection {
  id: number;
  prefecture: string;
  service: string;
  pole: string;
  city: string;
  director: string;
  direction: string;
  phoneNumber: string;
  faxNumber: string;
  address: string;
  signingAuthority: string;
  firstNameSigningAuthority: string;
  lastNameSigningAuthority: string;
}

export class FullDepartment extends Department {
  territorialDirection: TerritorialDirection;
}
