import { FeatureCollection } from 'geojson';
import { City } from './city';
import { StatisticsControl } from './statisticsControl';

export class ControlPreparation {
  id: number;
  controlDate: Date;
  attendantMail?: string;
  controllerMail: string;
  geoJson: FeatureCollection;
  geoJsonParts: string[];
  statistics?: StatisticsControl;
  cities: City[];
}
