export class StatisticsControl {
  controlledDevices: number;
  startDate?: Date;
  traveledDistance: number;
  duration: number;
}
