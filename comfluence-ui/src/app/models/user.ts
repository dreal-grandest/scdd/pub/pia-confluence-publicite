import { Department } from './department';

export class User {
  mail: string;
  firstName: string;
  lastName: string;
  token?: string;
  departement: Department;
  roles?: Role[];
}

export class FullUser extends User {
  id: number;
  enabled: boolean;
}

export class Role {
  id: number;
  name: string;
}

export class CreateUserDto {
  mail: string;
  firstName: string;
  lastName: string;
  pwd: string;
  dep: number;
  roles: number[];
}
