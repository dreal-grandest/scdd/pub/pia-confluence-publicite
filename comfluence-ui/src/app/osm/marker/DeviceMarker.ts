import * as L from 'leaflet';
import { LatLngExpression, MarkerOptions } from 'leaflet';
import { Device, Status, Type } from 'src/app/models/device/device';

/**
 * class StatusMarker
 */
export class DeviceMarker<P = any> extends L.Marker<P> {
  constructor(device: Device, latlng: LatLngExpression, options?: MarkerOptions) {
    super(latlng, options);
    this.status = device.status;
    this.type = device.type;
    this.id = device.id;
  }

  private readonly status: Status;
  private readonly id: number;
  private readonly type: Type;

  getStatus(): Status {
    return this.status;
  }

  getId(): number {
    return this.id;
  }

  getType(): Type {
    return this.type;
  }
}

export function deviceMarker(device: Device, latlng: LatLngExpression, options?: MarkerOptions): L.Marker {
  return new DeviceMarker(device, latlng, options);
}
