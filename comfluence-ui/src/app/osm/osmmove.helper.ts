import { CoordinatesService } from 'src/app/services/coordinates.service';
import { Coordinates } from 'src/app/models/device/properties/coordinates';
import * as L from 'leaflet';
import * as turfHelpers from '@turf/helpers';
import distance from '@turf/distance';
import { createBox4326 } from '../models/box';

export interface IOsmMoveHelper {
  moveToCoordinate(coordinate: Coordinates, zoom: number);
}

export class OsmMoveHelper implements IOsmMoveHelper {
  constructor(private map: L.Map, private coordinatesService: CoordinatesService) {
    this.map.on('moveend', this.onMapMoveEndEvent, this);
  }

  public moveToLatLng(latLng: L.LatLng, zoom: number) {
    const from = turfHelpers.point([latLng.lat, latLng.lng]);
    const to = turfHelpers.point([this.coordinatesService.coordinatesValue.latitude,
      this.coordinatesService.coordinatesValue.longitude]);
    const distanceVar = distance(from, to);
    // if (distanceVar && distanceVar >= 0.002) {
      this.map.flyTo(latLng, zoom, {animate: false});
    // }
  }

  private onMapMoveEndEvent() {
    const bounds = this.map.getBounds();
    const northEast = bounds.getNorthEast();
    const southWest = bounds.getSouthWest();
    const box = createBox4326({
      minLat: southWest.lat,
      minLong: southWest.lng,
      maxLat: northEast.lat,
      maxLong: northEast.lng
    });
    this.coordinatesService.updateBox(box);
  }

  public moveToCoordinate(coordinate: Coordinates, zoom: number) {
    this.moveToLatLng(new L.LatLng(coordinate.latitude, coordinate.longitude), zoom);
  }
}
