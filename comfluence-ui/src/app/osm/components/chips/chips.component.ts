import { Component, Input, OnDestroy } from '@angular/core';
import { Filter } from 'src/app/models/device/Filter';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ResizedEvent } from 'angular-resize-event';
import { SubjectsService } from 'src/app/services/subjects.service';

@Component({
  selector: 'app-chips',
  templateUrl: './chips.component.html',
  styleUrls: ['./chips.component.scss']
})
export class ChipsComponent implements OnDestroy {

  @Input() filters: Filter[] = [];
  hiddenFilters: Filter[] = [];
  filterContainerSize: number;
  subscription: Subscription;
  mapContainerWidth: number;
  index = 1;

  constructor(private router: Router, private subjectsService: SubjectsService) {
    this.subscription = this.subjectsService.getContainerFilterWidth().subscribe(data => {
      if (data) {
        this.mapContainerWidth = data.width;
      } else {
        this.mapContainerWidth = null;
      }
      this.onResized();
    });
  }

  onResized(event?: ResizedEvent) {
    if (event) {
      this.filterContainerSize = event.newWidth;
    }
    if (this.mapContainerWidth) {
      if ((this.mapContainerWidth - 100) < this.filterContainerSize && this.filters[(this.filters.length - this.index)]) {
        this.filters[this.filters.length - this.index]['isHidden'] = true;
        this.hiddenFilters.push(this.filters[this.filters.length - this.index]);
        this.index += 1;
      }
      if ((this.mapContainerWidth - 350) > this.filterContainerSize && this.hiddenFilters.length > 0) {
        if (this.index > 1) {
          this.index -= 1;
          this.filters[this.filters.length - this.index]['isHidden'] = false;
          this.hiddenFilters.pop();
        }
      }
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public removeTag(filter: Filter): void {
    if (this.router.url.startsWith('/items?filter')) {
      this.filters.splice(this.filters.indexOf(filter), 1);
      filter['selected'] = false;
      const filterIds: number[] = [];
      this.filters.forEach(el => {
        filterIds.push(el.id);
      });
      if (filterIds.length > 0) {
        this.router.navigate(['/items'], { queryParams: { filterId: filterIds } });
      } else {
        this.router.navigate(['/items']);
      }
    }
  }

  openFilterModal(): void {
    this.subjectsService.openFilterModaleFromHiddenList();
  }
}
