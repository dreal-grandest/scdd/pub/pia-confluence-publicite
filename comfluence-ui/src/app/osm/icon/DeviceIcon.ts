import * as L from 'leaflet';
import { DivIconOptions } from 'leaflet';
import { Status, Type } from 'src/app/models/device/device';
import { StatusEnumToCssPipe } from 'src/app/shared/pipes/status-enum-to-css.pipe';

/**
 * class DeviceIcon
 */
export class DeviceIcon extends L.DivIcon {
  constructor(options?: DivIconOptions) {
    super({
      iconSize: [52, 57],
      iconAnchor: [26, 56]
    });
    L.Util.setOptions(this, options);
  }
}

/**
 * Initialize the Map of DeviceIcon available
 */
// FIXME: bad practice to use a pipe like that (externalise the core in an helper so can be called by both a pipe & other source)
const toCssPipe: StatusEnumToCssPipe = new StatusEnumToCssPipe();
const allStatus: Status[] = <Status[]>Object.keys(Status);
const allType: Type[] = <Type[]>Object.keys(Type);
const icons: Map<String, L.DivIcon> = new Map<String, L.DivIcon>();
allStatus.forEach((status) => {
  allType.forEach(type => {
    icons.set(toCssPipe.transform(status) + '-' + type.toString().toLowerCase(), <L.DivIcon>new DeviceIcon({
      html: `<div class="device-div-bg ${toCssPipe.transform(status)}-bg">
                <div class="device-div-icon icon-${type.toString().toLowerCase()}"></div>
            </div>`
    }));
  });
});

export function deviceIcon(status: Status, type: Type): L.DivIcon {
  return icons.get(toCssPipe.transform(status) + '-' + type.toString().toLowerCase());
}

export function deviceClusterIcon(status: Status, clusterCount: number): L.DivIcon {
  return new DeviceIcon({
    html: `<div class="device-div-bg ${toCssPipe.transform(status)}-bg">${clusterCount}</div>`
  });
}
