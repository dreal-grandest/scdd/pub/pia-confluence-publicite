import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChipsComponent } from './components/chips/chips.component';
import { OsmComponent } from './osm.component';
import { AngularResizedEventModule } from 'angular-resize-event';


@NgModule({
  declarations: [
    ChipsComponent,
    OsmComponent
  ],
  imports: [
    CommonModule,
    AngularResizedEventModule // for chips component (resized)
  ],
  exports: [
    OsmComponent
  ]
})
export class OsmModule {
}
