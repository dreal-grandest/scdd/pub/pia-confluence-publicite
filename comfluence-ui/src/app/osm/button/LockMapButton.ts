import * as L from 'leaflet';
import { BehaviorSubject, Observable } from 'rxjs';
import { Coordinates } from '../../models/device/properties/coordinates';

export class LockMapButton extends L.Control {
  static LOCKED_CLASS = 'locked';
  static UNLOCKED_CLASS = 'unlocked';

  private map: L.Map;
  private icon: HTMLAnchorElement;
  private marker: L.Marker;
  private lastMarkerPosition: L.LatLng;
  public locked: Observable<boolean>;
  private lockedSubject: BehaviorSubject<boolean>;
  private updateCoordinates: (coord: Coordinates) => (void);

  constructor(options: LockMapButtonOptions) {
    super(options);
    this.marker = options.trackedMarker;
    this.lastMarkerPosition = options.trackedMarker.getLatLng();
    this.lockedSubject = new BehaviorSubject<boolean>(options.isLocked);
    this.locked = this.lockedSubject.asObservable();
    this.updateCoordinates = options.updateCoordinates;
  }

  onAdd(map: L.Map): HTMLElement {
    this.map = map;

    const container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
    const lockedClass = this.lockedSubject.value ? LockMapButton.LOCKED_CLASS : LockMapButton.UNLOCKED_CLASS;
    this.icon = <HTMLAnchorElement>L.DomUtil.create('a', 'validate-button ' + lockedClass, container);
    this.icon.title = 'Valider position';

    map.addLayer(this.marker);
    this.locked.subscribe({
      next: locking => {
        if (locking) {
          this.resetPosition();
        }
      }
    });

    L.DomEvent.on(container, 'dblclick', L.DomEvent.stopPropagation);
    L.DomEvent.on(container, 'click', this.onClickButtonEvent, this);
    map.on('move', this.onMapMove, this);
    return container;
  }

  onRemove(map: L.Map): void {
    L.DomEvent.off(this.getContainer(), 'dblclick', L.DomEvent.stopPropagation);
    L.DomEvent.off(this.getContainer(), 'click', this.onClickButtonEvent, this);
    map.off('move', this.onMapMove, this);
  }

  private onClickButtonEvent(ev: MouseEvent) {
    ev.preventDefault();
    ev.stopPropagation();
    if (this.lockedSubject.value) {
      this.resetPosition();
    }
    this.toggleDragAndZoom();
    let oldClass = LockMapButton.UNLOCKED_CLASS;
    let newClass = LockMapButton.LOCKED_CLASS;
    if (!this.lockedSubject.value) {
      oldClass = LockMapButton.LOCKED_CLASS;
      newClass = LockMapButton.UNLOCKED_CLASS;
    }
    this.icon.classList.replace(oldClass, newClass);
  }

  private onMapMove() {
    const center = this.map.getCenter();
    if (!this.lockedSubject.value) {
      this.marker.setLatLng(center);
      this.lastMarkerPosition = center;
      this.updateCoordinates(<Coordinates>{
        latitude: this.lastMarkerPosition.lat,
        longitude: this.lastMarkerPosition.lng
      });
    }
  }

  private resetPosition() {
    this.map.setView(this.lastMarkerPosition, this.map.getZoom(), {animate: false});
  }

  private toggleDragAndZoom(force = false, locked = false) {
    if (force) {
      this.lockedSubject.next(locked);
    } else {
      this.lockedSubject.next(!this.lockedSubject.value);
    }
  }
}

export function lockMapButton(options: LockMapButtonOptions): LockMapButton {
  return new LockMapButton(options);
}

export interface LockMapButtonOptions extends L.ControlOptions {
  isLocked: boolean;
  trackedMarker: L.Marker;
  updateCoordinates: (coord: Coordinates) => (void);
}
