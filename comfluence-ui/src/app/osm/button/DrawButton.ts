import * as L from 'leaflet';
import '@geoman-io/leaflet-geoman-free';
import 'leaflet-measure-path';
import * as geojson from 'geojson';

export class DrawButton extends L.Control {
  private active = false;
  private initialize = false;
  private map: L.Map;
  private drawOption: DrawOption = {};

  private drawnLayers: Map<number, L.Layer> = new Map<number, L.Layer>();

  constructor(options?: DrawOption) {
    super(<L.ControlOptions>options);
    this.drawOption = options;
  }

  persistDrawnGeometries() {
    const geometries = [];
    this.drawnLayers.forEach(layer => {
      let geoJSON;
      if (<any>(<L.Polygon>layer).toGeoJSON().type === 'FeatureCollection') {
        geoJSON = (<geojson.FeatureCollection><unknown>(<L.Polygon>layer).toGeoJSON()).features[0].geometry;
      } else {
        geoJSON = (<L.Polygon>layer).toGeoJSON().geometry;
      }
      geometries.push(geoJSON);
    });
    this.drawOption.persistDrawnGeometries(geometries);
  }

  onAdd(map: L.Map): HTMLElement {
    this.map = map;
    const controlButton = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
    const icon: HTMLAnchorElement = <HTMLAnchorElement>L.DomUtil.create('a', 'validate-button draw', controlButton);
    icon.title = 'Afficher les outils de dessins';

    L.DomEvent.on(controlButton, 'dblclick', (event => event.stopPropagation()));
    L.DomEvent.on(controlButton, 'click', this.toogleActiveState, this);
    if (this.drawOption.pathOptions) {
      map.pm.setPathOptions(this.drawOption.pathOptions);
    }

    if (this.drawOption.alwaysOpen) {
      this.toogleActiveState();
      controlButton.style.display = 'none';
    }
    return controlButton;
  }

  onRemove(map: L.Map): void {
    this.toogleActiveState();
    this.drawnLayers.clear();
    this.map.off('pm:create', this.onCreateLayer, this);
    this.map.off('pm:remove', this.onDeleteLayer, this);
    L.DomEvent.on(this.getContainer(), 'dblclick', (event => event.stopPropagation()));
    L.DomEvent.on(this.getContainer(), 'click', this.toogleActiveState, this);
  }

  private toogleActiveState(event?: Event) {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }
    const pm = (<any>this.map.pm);
    if (!this.initialize) {
      pm.setLang('fr');
      this.map.pm.addControls(<L.PM.ToolbarOptions>{
        position: this.options.position ? this.options.position : 'topright',
        drawMarker: !!this.drawOption.drawMarker,
        drawCircleMarker: !!this.drawOption.drawCircleMarker,
        drawRectangle: !!this.drawOption.drawRectangle,
        drawPolygon: !!this.drawOption.drawPolygon,
        drawCircle: !!this.drawOption.drawCircle,
        cutPolygon: !!this.drawOption.cutPolygon,
        drawPolyline: !!this.drawOption.drawPolyline,
      });
      this.map.on('pm:create', this.onCreateLayer, this);
      this.map.on('pm:remove', this.onDeleteLayer, this);
      this.initialize = true;
      this.active = true;
      return;
    }
    if (this.active) {
      this.active = false;
      pm.disableGlobalEditMode();
      pm.disableGlobalDragMode();
      pm.disableGlobalRemovalMode();
      (<any>pm.Draw).Cut.disable();
      pm.toggleControls();
    } else {
      pm.toggleControls();
      this.active = true;
    }
  }

  private onCreateLayer(e) {
    const el = (<any>e);
    const layer = el.layer;
    this.drawnLayers.set(layer._leaflet_id, layer);
    layer.on('pm:edit', this.persistDrawnGeometries, this);
    this.persistDrawnGeometries();
  }

  private onDeleteLayer(e) {
    const el = (<any>e);
    const layer = el.layer;
    this.drawnLayers.delete(layer._leaflet_id);
    layer.off('pm:edit', this.persistDrawnGeometries, this);
    this.persistDrawnGeometries();
  }

  addGeoJSONToMapAsDrawnLayer(geoJSON: geojson.FeatureCollection) {
    geoJSON.features.forEach(feature => {
      const layerGroup = L.geoJSON(feature).setStyle(this.drawOption.pathOptions).addTo(this.map);
      const layer = (<any>(<L.LayerGroup>layerGroup).getLayers()[0]);
      this.drawnLayers.set(layer._leaflet_id, layer);
      this.onCreateLayer({layer: layer});
    });
  }
}

export function drawButton(options?: DrawOption): DrawButton {
  return new DrawButton(options);
}


export interface DrawOption extends L.PM.ToolbarOptions {
  alwaysOpen?: boolean;
  drawCircleMarker?: boolean;
  drawRectangle?: boolean;
  drawCircle?: boolean;
  cutPolygon?: boolean;
  drawPolyline?: boolean;
  pathOptions?: L.PathOptions;
  persistDrawnGeometries?: (geometries: geojson.Geometry[]) => void;
}
