import * as L from 'leaflet';
import { ControlOptions } from 'leaflet';
import { Coordinates } from 'src/app/models/device/properties/coordinates';
import { Observable, Subscription } from 'rxjs';
import { CoordinatesService } from '../../services/coordinates.service';

export class MyPositionButton extends L.Control {
  private map: L.Map;
  private currentLocation: Coordinates;
  private locationSubscription: Subscription;

  constructor(map: L.Map, coordinatesService: CoordinatesService, options?: ControlOptions) {
    super(options);
    this.map = map;
    this.currentLocation = coordinatesService.locationCoordinatesValue;
    this.locationSubscription = coordinatesService.locationCoordinates.subscribe(coordinates => {
      this.currentLocation = coordinates;
    });
  }

  onAdd(map: L.Map): HTMLElement {
    const controlButton = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
    const icon = <HTMLAnchorElement>L.DomUtil.create('a', 'center-my-position-button', controlButton);
    icon.title = 'Centrer sur ma position';

    L.DomEvent.on(controlButton, 'dblclick', (event => event.stopPropagation()));
    L.DomEvent.on(controlButton, 'click', this.flyToMyPosition, this);

    return controlButton;
  }

  onRemove(map: L.Map): void {
    L.DomEvent.on(this.getContainer(), 'dblclick', (event => event.stopPropagation()));
    L.DomEvent.on(this.getContainer(), 'click', this.flyToMyPosition, this);
    this.locationSubscription.unsubscribe();
  }

  private flyToMyPosition(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    this.map.flyTo([this.currentLocation.latitude, this.currentLocation.longitude], this.map.getZoom(), { animate: false });
  }
}

export function myPositionButton(map: L.Map, coordinatesService: CoordinatesService, options?: ControlOptions): MyPositionButton {
  return new MyPositionButton(map, coordinatesService, options);
}
