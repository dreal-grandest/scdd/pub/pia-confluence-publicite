import * as L from 'leaflet';
import '@geoman-io/leaflet-geoman-free';
import 'leaflet-measure-path';

export class MeasurementsButton extends L.Control {
  private active = false;
  private initialize = false;
  private map: L.Map;

  constructor(options?: L.ControlOptions) {
    super(options);
  }

  onAdd(map: L.Map): HTMLElement {
    this.map = map;
    const controlButton = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
    const icon: HTMLAnchorElement = <HTMLAnchorElement>L.DomUtil.create('a', 'validate-button measurement', controlButton);
    icon.title = 'Afficher les outils de mesure';

    L.DomEvent.on(controlButton, 'dblclick', (event => event.stopPropagation()));
    L.DomEvent.on(controlButton, 'click', this.toogleActiveState, this);
    return controlButton;
  }

  private toogleActiveState(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    const pm = (<any>this.map.pm);
    if (!this.initialize) {
      pm.setLang('fr');
      this.map.pm.addControls(<L.PM.ToolbarOptions>{
        position: 'topright',
        drawMarker: false,
        drawCircleMarker: false,
        drawRectangle: false,
        drawPolygon: false,
        drawCircle: false,
        cutPolygon: false,
      });
      this.map.on('pm:create', (e) => {
        const e1 = (<any>e);
        const supportedTypes: string[] = ['Line', 'Polygon', 'Rectangle', 'Circle'];
        if (supportedTypes.includes(e1.shape)) {
          e1.layer.showMeasurements();
        }
      });
      this.initialize = true;
      this.active = true;
      return;
    }
    if (this.active) {
      this.active = false;
      pm.disableGlobalEditMode();
      pm.disableGlobalDragMode();
      pm.disableGlobalRemovalMode();
      (<any>pm.Draw).Cut.disable();
      pm.toggleControls();
    } else {
      pm.toggleControls();
      this.active = true;
    }
  }
}


export function measurementsButton(options?: L.ControlOptions): MeasurementsButton {
  return new MeasurementsButton(options);
}
