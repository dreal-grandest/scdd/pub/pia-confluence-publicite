import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService<T> {
  private lastResultsSubject: ReplaySubject<T[]>;
  public lastResults: Observable<T[]>;

  constructor() {
    this.lastResultsSubject = new ReplaySubject<T[]>(1);
    this.lastResults = this.lastResultsSubject.asObservable();
  }

  public updateLastResultsSubject(items: T[]) {
    this.lastResultsSubject.next(items);
  }
}
