import { TestBed } from '@angular/core/testing';

import { ReplayDevicesService } from './replay-devices.service';

describe('ReplayDevicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReplayDevicesService = TestBed.get(ReplayDevicesService);
    expect(service).toBeTruthy();
  });
});
