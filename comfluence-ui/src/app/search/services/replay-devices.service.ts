import { Injectable } from '@angular/core';
import { ReplayResultsService } from '../../shared/services/replay-results.service';
import { Device } from '../../models/device/device';

@Injectable({
  providedIn: 'root'
})
export class ReplayDevicesService extends ReplayResultsService<Device> {

  constructor() {
    super(1);
  }
}
