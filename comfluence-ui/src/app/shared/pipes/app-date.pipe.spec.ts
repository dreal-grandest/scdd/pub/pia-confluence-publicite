import { AppDatePipe } from './app-date.pipe';

describe('AppDatePipe', () => {
  it('create an instance', () => {
    const pipe = new AppDatePipe();
    expect(pipe).toBeTruthy();
  });

  it('should send default value', () => {
    const pipe = new AppDatePipe();
    expect(pipe.transform(null)).toBe('--.--.--');
  });
});
