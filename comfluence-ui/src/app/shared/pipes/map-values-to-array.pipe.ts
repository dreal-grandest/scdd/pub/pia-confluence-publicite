import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapValuesToArray'
})
export class MapValuesToArrayPipe implements PipeTransform {

  transform<K, V>(map: Map<K, V>): V[] {
    return Array.from(map.values());
  }
}
