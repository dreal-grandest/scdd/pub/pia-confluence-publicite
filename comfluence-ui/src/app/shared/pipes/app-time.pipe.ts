import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appTime'
})
export class AppTimePipe implements PipeTransform {

  private static padDateTwoDigits(date: number): string {
    return date.toString().padStart(2, '0');
  }

  transform(date: number | string | Date): any {
    if (date) {
      let pDate = new Date(date);
      if (date instanceof Date) {
        pDate = date;
      } else {
        pDate = new Date(date);
      }
      return AppTimePipe.padDateTwoDigits(pDate.getHours()) + 'h'
        + AppTimePipe.padDateTwoDigits(pDate.getMinutes());
    } else {
      return '--h--';
    }
  }
}
