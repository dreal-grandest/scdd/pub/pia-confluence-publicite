import { NgModule } from '@angular/core';
/* MODULE */
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
/* PIPE */
import { AppTimePipe } from './pipes/app-time.pipe';
import { AppDatePipe } from './pipes/app-date.pipe';
import { EnumToArrayPipe } from './pipes/enum-to-array.pipe';
import { StatusEnumToCssPipe } from './pipes/status-enum-to-css.pipe';
import { EnumToStringPipe } from './pipes/enum-to-string.pipe';
import { MapValuesToArrayPipe } from './pipes/map-values-to-array.pipe';
import { RoleTranslatePipe } from './pipes/role-translate.pipe';
/* DIRECTIVE */
import { ActionMenuOriginDirective } from './components/action-menu/action-menu-origin';
import { ActionMenuDirective } from './components/action-menu/action-menu';
import { HasRoleDirective } from './hasrole.directive';
/* COMPONENT */
import { InputComponent } from './components/input/input.component';
import { LoaderComponent } from './components/loader/loader.component';
import { HeaderComponent } from './components/header/header.component';
import { ConfirmModalComponent } from './components/confirm-modal/confirm-modal.component';
import { ActionMenuComponent } from './components/action-menu/action-menu.component';
import { SearchInputComponent } from './components/search-input/search-input.component';
import { TextareaComponent } from './components/textarea/textarea.component';
import { RadioGroupComponent } from './components/radio/radio-group.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { TabComponent } from './components/tabs/tab/tab.component';
import { InputAutocompleteComponent } from './components/input-autocomplete/input-autocomplete.component';
import { ItemComponent } from './components/item/item.component';
import { ImageComponent } from './components/image/image.component';
import { DateComponent } from './components/date/date.component';
import { ContextComponent } from './components/context/context.component';
import { DescriptionComponent } from './components/description/description.component';
import { CommentComponent } from './components/comment/comment.component';
import { ModalComponent } from './components/modal/modal.component';
import { NextControlFormComponent } from './components/next-control/next-control.component';
import { NewDeviceFormComponent } from './components/new-device-form/new-device-form.component';
import { OsmModule } from '../osm/osm.module';
import { OsmComponent } from '../osm/osm.component';

@NgModule({
  declarations: [
    AppDatePipe,
    AppTimePipe,
    EnumToArrayPipe,
    StatusEnumToCssPipe,
    EnumToStringPipe,
    MapValuesToArrayPipe,
    RoleTranslatePipe,
    ActionMenuOriginDirective,
    ActionMenuDirective,
    HasRoleDirective,
    InputComponent,
    LoaderComponent,
    HeaderComponent,
    ConfirmModalComponent,
    ItemComponent,
    ImageComponent,
    DateComponent,
    ActionMenuComponent,
    SearchInputComponent,
    TextareaComponent,
    RadioGroupComponent,
    CheckboxComponent,
    InputAutocompleteComponent,
    TabComponent,
    TabsComponent,
    ModalComponent,
    NextControlFormComponent,
    CommentComponent,
    ContextComponent,
    DescriptionComponent,
    NewDeviceFormComponent
  ],
  imports: [
    OsmModule,
    CommonModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    AutocompleteLibModule
  ], exports: [
    AppDatePipe,
    AppTimePipe,
    EnumToArrayPipe,
    StatusEnumToCssPipe,
    EnumToStringPipe,
    MapValuesToArrayPipe,
    RoleTranslatePipe,
    ActionMenuOriginDirective,
    ActionMenuDirective,
    HasRoleDirective,
    InputComponent,
    LoaderComponent,
    HeaderComponent,
    ConfirmModalComponent,
    ItemComponent,
    ImageComponent,
    DateComponent,
    ActionMenuComponent,
    SearchInputComponent,
    TextareaComponent,
    RadioGroupComponent,
    CheckboxComponent,
    InputAutocompleteComponent,
    TabComponent,
    TabsComponent,
    ModalComponent,
    NextControlFormComponent,
    CommentComponent,
    ContextComponent,
    DescriptionComponent,
    NewDeviceFormComponent,
    OsmComponent
  ]
})
export class SharedModule {
}
