import { Observable, ReplaySubject } from 'rxjs';

export abstract class ReplayResultsService<T> {
  private lastResultsSubject: ReplaySubject<T[]>;
  public lastResults: Observable<T[]>;

  protected constructor(bufferSize: number) {
    this.lastResultsSubject = new ReplaySubject<T[]>(bufferSize);
    this.lastResults = this.lastResultsSubject.asObservable();
  }

  public updateLastResultsSubject(items: T[]) {
    this.lastResultsSubject.next(items);
  }
}
