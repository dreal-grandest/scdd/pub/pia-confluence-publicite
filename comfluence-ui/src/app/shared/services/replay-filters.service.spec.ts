import { TestBed } from '@angular/core/testing';

import { ReplayFiltersService } from './replay-filters.service';

describe('ReplayFiltersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReplayFiltersService = TestBed.get(ReplayFiltersService);
    expect(service).toBeTruthy();
  });
});
