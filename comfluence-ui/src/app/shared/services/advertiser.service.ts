import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export class Advertiser {
  id: number;
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class AdvertiserService extends BaseService<Advertiser> {

  constructor(private httpClient: HttpClient) {
    super('advertiser', httpClient);
  }

  public findAllByDepartement(code: number): Observable<Advertiser[]> {
    return this.httpClient.get<Advertiser[]>(`${this.baseUrl}/byDepartement/${code}`);
  }
}
