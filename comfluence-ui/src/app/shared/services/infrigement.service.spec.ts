import { TestBed } from '@angular/core/testing';

import { InfrigementService } from './infrigement.service';

describe('InfrigementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InfrigementService = TestBed.get(InfrigementService);
    expect(service).toBeTruthy();
  });
});
