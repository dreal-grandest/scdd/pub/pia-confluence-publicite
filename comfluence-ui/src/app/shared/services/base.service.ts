import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

export abstract class BaseService<T> {
  private static readonly BASE_URL: String = environment.baseUrlApi;

  private readonly path: string;
  private _httpClient: HttpClient;

  protected constructor(path: string, httpClient: HttpClient) {
    this.path = path;
    this._httpClient = httpClient;
  }

  get baseUrl() {
    return `${BaseService.BASE_URL}/${this.path}`;
  }

  public findAll(): Observable<T[]> {
    return this._httpClient.get<T[]>(this.baseUrl);
  }
}
