import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

/**
 * Inspired by https://blog.strongbrew.io/display-a-component-based-on-role/
 */
@Directive({
  selector: '[appHasRole]'
})
export class HasRoleDirective implements OnInit {
  @Input() appHasRole: string;
  isVisible = false;

  constructor(private viewContainerRef: ViewContainerRef,
              private templateRef: TemplateRef<any>,
              private authenticationService: AuthenticationService) {

  }

  ngOnInit() {
    if (this.appHasRole) {
      const splittedRoles = this.appHasRole.split(',');
      if (this.authenticationService.hasRole(splittedRoles)) {
        if (!this.isVisible) {
          this.isVisible = true;
          this.viewContainerRef.createEmbeddedView(this.templateRef);
        }
      } else {
        this.isVisible = false;
        this.viewContainerRef.clear();
      }
    }
  }
}
