import { Component, ContentChildren, QueryList, AfterContentInit, ViewChildren, ElementRef, AfterViewInit } from '@angular/core';
import { TabComponent } from './tab/tab.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent {
  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  select(index: number, tab: TabComponent) {
    if (!tab.disabled) {
      this.tabs.forEach((tabLocal, tabIndex) => {
        tabLocal.selected = (index === tabIndex);
      });
    }
  }
}
