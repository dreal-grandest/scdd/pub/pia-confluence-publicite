import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Comment } from 'src/app/models/device/properties/comment';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent  implements OnInit, OnDestroy {
  @Output() commentChange = new EventEmitter<Comment>();
  @Input() comment: Comment;
  @Input() readOnly = false;

  commentForm: FormGroup;
  private subscription: Subscription;

  constructor(private formBuilder: FormBuilder) {
    this.commentForm = this.formBuilder.group({
      id: this.formBuilder.control(null),
      text: this.formBuilder.control(null)
    });
  }

  ngOnInit(): void {
    if (this.comment) {
      this.commentForm.setValue({
        id: this.comment.id,
        text: this.comment.text
      });
    }
    this.subscription = this.commentForm.valueChanges.pipe(debounceTime(1000)).subscribe(values => {
      if (!values.text || values.text.trim() === '') {
        values = null;
      }
      this.commentChange.emit(values);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
