import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Device } from 'src/app/models/device/device';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent {
  @Output() itemClicked = new EventEmitter<Device>();
  @Input() item: Device;

  handleClick($event: MouseEvent): void {
    this.itemClicked.emit(this.item);
  }
}
