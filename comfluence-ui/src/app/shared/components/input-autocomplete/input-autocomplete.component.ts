import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-input-autocomplete',
  templateUrl: './input-autocomplete.component.html',
  styleUrls: ['./input-autocomplete.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputAutocompleteComponent),
      multi: true
    }
  ]
})
export class InputAutocompleteComponent implements OnInit, ControlValueAccessor {
  private isInit = false;

  constructor(private formBuilder: FormBuilder) {
  }

  @Input() data: /* T[] */ any[];
  @Input() searchOnField: string;
  private _value: /* T */ any;
  group: FormGroup;

  onChange = (data: any) => {};
  onTouched = () => {};

  selectEvent(value: /* T */ any) {
    if (value !== this._value) {
      this.writeValue(value);
    }
  }

  ngOnInit(): void {
    this.group = this.formBuilder.group({});
    this.group.addControl(this.searchOnField, this.formBuilder.control(''));
  }

  onChangeSearch(value: string) {
    let item;
    if (value != null) {
      item = this.data
        .find(datum => datum[this.searchOnField].toString().toLowerCase().trim() === (value as string).toLowerCase().trim());
    }
    if (!item) {
      item = this.newItem();
      item[this.searchOnField] = value || '';
    }
    this.writeValue(item);
  }

  registerOnChange(fn: any) {
    this.group.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.group.disable();
    } else {
      this.group.enable();
    }
  }

  writeValue(val: /* T */ any): void {
    if (val !== null && val !== undefined) {
      if (!this.isInit) {
        Object.keys(val).forEach(key => {
          if (key !== this.searchOnField) {
            this.group.addControl(key, this.formBuilder.control(null));
          }
        });
        this.isInit = true;
      }
      this._value = val;
      this.group.setValue(val);
    }
  }

  private newItem() {
    const item = {};
    Object.keys(this.group.controls).forEach(key => {
      item[key] = null;
    });
    return item;
  }
}
