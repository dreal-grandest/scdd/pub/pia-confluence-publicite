import { Component, EventEmitter, Inject, Input, LOCALE_ID, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ControlService } from '../../../services/control.service';
import { first, map } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import { Subscription } from 'rxjs';
import { UserService } from '../../../services/user.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { User } from '../../../models/user';
import { GeometryService } from '../../../services/geometry.service';
import { FeatureCollection, Geometry } from 'geojson';
import { Device } from '../../../models/device/device';
import { DeviceService } from '../../../services/device.service';
import { ControlPreparation } from '../../../models/control/controlPreparation';
import { CoordinatesService } from 'src/app/services/coordinates.service';

@Component({
  selector: 'app-next-control-modal',
  templateUrl: './next-control.component.html',
  styleUrls: ['./next-control.component.scss']
})
export class NextControlFormComponent implements OnInit, OnDestroy {
  @Input() control: ControlPreparation = new ControlPreparation();
  @Output() savedControl = new EventEmitter<ControlPreparation>();
  @Output() isOpen = new EventEmitter<boolean>();

  controlForm: FormGroup;
  isLoading = true;
  today: Date;
  readonly = false;
  dateIsSelected: boolean;
  attendants: User[] = [];
  loadingCities = false;

  private createdSubscriptions: Subscription[] = [];
  private NO_ATTENDANT: User = <User>{
    firstName: 'Pas d\'accompagnant',
    lastName: '',
    mail: ''
  };
  geoJsonControls: FeatureCollection[];
  outdatedItems: Device[] = [];

  private currentUser: User;
  noZoneSelected: boolean;

  constructor(private formBuilder: FormBuilder, private controlService: ControlService,
    @Inject(LOCALE_ID) public locale: string, private userService: UserService,
    private authenticationService: AuthenticationService,
    private geometryService: GeometryService,
    private deviceService: DeviceService,
    private coordinateService: CoordinatesService) {
    this.currentUser = authenticationService.currentUserValue;
    this.today = this.todayDate();
    this.controlForm = this.formBuilder.group({
      controlDate: ['', Validators.required],
      attendantMail: ''
    });
    this.initAttendantsSelect();
    this.updateControlGeometry();
  }

  private todayDate(): Date {
    const today = new Date();
    return new Date(today.getFullYear(), today.getMonth(), today.getDate());
  }

  private updateControlGeometry() {
    const subscription = this.geometryService.drawnGeometries
      .pipe(map(geometries => geometries.map((geometry) => JSON.stringify(geometry))))
      .subscribe(geoJsonParts => this.control.geoJsonParts = geoJsonParts);
    this.createdSubscriptions.push(subscription);
  }

  private initAttendantsSelect() {
    this.attendants.push(this.NO_ATTENDANT);
    this.userService.findAllByDepartment(this.currentUser.departement.code).pipe<User[]>(first()).subscribe(users => {
      this.attendants = this.attendants.concat(users.filter(user => user.mail !== this.currentUser.mail));
    });
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.createdSubscriptions.forEach(subscription => subscription.unsubscribe());
    this.createdSubscriptions = [];
  }


  private initForm() {
    if (this.control && this.control.id) {
      this.controlForm.setValue({
        controlDate: formatDate(this.control.controlDate, 'yyyy-MM-dd', this.locale),
        attendantMail: this.control.attendantMail == null ? '' : this.control.attendantMail
      });

      this.readonly = this.control.controlDate.getTime() < this.today.getTime();
      if (this.readonly) {
        this.controlForm.disable();
      }
      this.dateIsSelected = true;
      this.isLoading = false;
    } else {
      this.control = new ControlPreparation();
      this.control.controllerMail = this.currentUser.mail;
      const today = new Date();
      this.control.controlDate = new Date(Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()));
      this.geometryService.updateDrawnGeometries([]);
      this.controlForm.patchValue({ controlDate: formatDate(this.today, 'yyyy-MM-dd', this.locale) });
      this.dateIsSelected = true;
      this.isLoading = false;
    }
    const subscription = this.controlForm.valueChanges.subscribe(values => {
      this.dateIsSelected = !(values.controlDate === '');
      const controlDto = <ControlPreparation>{
        controlDate: new Date(values.controlDate),
        attendantMail: (values.attendantMail === '') ? null : values.attendantMail
      };
      Object.assign(this.control, controlDto);
    });
    this.createdSubscriptions.push(subscription);
  }

  cancel() {
    this.isOpen.emit(false);
  }

  onSubmit() {
    // stop here if form is invalid
    const noSelectedZone = !this.control.cities || this.control.cities.length === 0;
    if (this.controlForm.invalid || noSelectedZone) {
      this.noZoneSelected = noSelectedZone;
      return;
    }
    this.isLoading = true;
    const subscription = this.controlService.save(this.control).subscribe(control => {
      this.isOpen.emit(false);
      this.savedControl.emit(control);
      this.isLoading = false;
    });
    this.createdSubscriptions.push(subscription);
  }

  onShowOldClick(checked: boolean) {
    if (checked) {
      this.geoJsonControls = [];
      this.controlService.findRecentByDepartmentCode(this.currentUser.departement.code, 5, this.today)
        .pipe(first(), map(controls => controls.map(control => control.geoJson))).subscribe(geoJsons => this.geoJsonControls = geoJsons);
    } else {
      this.geoJsonControls = undefined;
    }
  }

  onShowOutdatedClick(checked: boolean) {
    if (checked) {
      this.deviceService.findAllFromBox(this.coordinateService.boxValue)
        .pipe(first())
        .subscribe(devices => {
          this.outdatedItems = devices.filter(device => device.tags && device.tags.some(tag => tag.id === 3));
        });
    } else {
      this.outdatedItems = [];
    }
  }

  showCities($event: Geometry[]) {
    this.loadingCities = true;
    this.geometryService.findIntersectingCities($event).pipe(first()).subscribe(cities => {
      this.loadingCities = false;
      this.control.cities = cities;
      this.noZoneSelected = this.control.cities.length === 0;
    }, error => {
      this.loadingCities = false;
    });
  }
}

