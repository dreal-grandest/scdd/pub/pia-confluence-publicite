import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextControlFormComponent } from './next-control.component';

describe('NextControlFormComponent', () => {
  let component: NextControlFormComponent;
  let fixture: ComponentFixture<NextControlFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextControlFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextControlFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
