import { Component, forwardRef, Input, OnDestroy } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true
    }
  ]
})
export class CheckboxComponent implements ControlValueAccessor, OnDestroy {

  static checkboxGroupCount = 0;

  @Input() inputStyle = 'square';
  @Input() labelStyle?: string;
  @Input() id?: string;
  @Input() label: string;
  @Input() valueSelected: any = true;
  control: FormControl;
  private subscription: Subscription;
  // permit the initialisation of the radio group first value
  private isInit = false;

  onChange = (data: any) => {};
  onTouched = () => {};

  constructor(private formBuilder: FormBuilder) {
    this.control = this.formBuilder.control(false);
    this.id = `checkbox-${CheckboxComponent.checkboxGroupCount++}`;
    this.subscription = this.control.valueChanges.subscribe(value => {
      if (typeof this.valueSelected === 'boolean') {
        this.onChange(value);
      } else {
        if (value) {
          this.onChange(this.valueSelected);
        } else {
          this.onChange(null);
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  writeValue(val: string): void {
    if (val !== null && val !== undefined && !this.isInit) {
      this.control.patchValue(val, {emitEvent: false});
      this.isInit = true;
    }
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.control.disable();
    } else {
      this.control.enable();
    }
  }
}
