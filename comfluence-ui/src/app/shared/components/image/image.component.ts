import { Component, Input, OnChanges, OnInit, SimpleChanges, OnDestroy } from '@angular/core';
import { PhotoService } from 'src/app/services/photo.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit, OnChanges, OnDestroy {

  @Input() id: number;
  @Input() src: string;
  @Input() downloadFull = true;

  imageToShow: any;
  degradedImageToShow: any;
  isImageLoading = true;
  private thumbSubscription: Subscription;
  private fullSubscription: Subscription;

  constructor(private photoService: PhotoService) {
  }

  ngOnInit(): void {
    this.initImages();
  }

  private initImages() {
    if (this.src) {
      this.imageToShow = this.src;
      this.isImageLoading = false;
    } else if (this.id) {
      this.getDegradedImageFromService();
      if (this.downloadFull) {
        this.getImageFromService();
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initImages();
  }

  ngOnDestroy(): void {
    if (this.fullSubscription) {
      this.fullSubscription.unsubscribe();
    }
    if (this.thumbSubscription) {
      this.thumbSubscription.unsubscribe();
    }
  }


  private getImageFromService() {
    this.fullSubscription = this.photoService.getImageBlob(this.id).subscribe(data => {
      this.createImageFromBlob(data);
      this.isImageLoading = false;
    }, error => {
      this.isImageLoading = false;
    });
  }

  private getDegradedImageFromService() {
    this.thumbSubscription = this.photoService.getImageThumbBlob(this.id).subscribe(data => {
      this.createImageFromBlob(data, true);
    });
  }

  private createImageFromBlob(image: Blob, isDegraded = false) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      if (isDegraded) {
        this.degradedImageToShow = reader.result;
      } else {
        this.imageToShow = reader.result;
      }
    }, false);
    if (image) {
      reader.readAsDataURL(image);
    }
  }
}
