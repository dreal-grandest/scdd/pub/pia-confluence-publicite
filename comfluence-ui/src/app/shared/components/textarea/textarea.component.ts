import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: TextareaComponent,
    multi: true
  }]
})
export class TextareaComponent implements OnInit, ControlValueAccessor {

  @Input() placeholder?: string;
  @Input() maxLength = 255;
  @Input() showCount = false;
  disabled: boolean;
  decreasingCount: number;

  private _value: string;

  onChange = (_: any) => { };
  onTouched = () => { };

  ngOnInit(): void {
    this.decreasingCount = this.maxLength;
  }

  writeValue(obj: string): void {
    this.value = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  get value() {
    return this._value;
  }

  set value(value: string) {
    if (value === null) {
      value = '';
    }
    if (value !== undefined && value !== this._value) {
      this._value = value;
      this.decreasingCount = this.maxLength - this._value.length;
      this.onChange(value);
    }
  }
}
