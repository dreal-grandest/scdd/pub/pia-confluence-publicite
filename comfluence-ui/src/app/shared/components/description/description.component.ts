import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DeragotoryType, Description, Position } from 'src/app/models/device/properties/description';
import { Category } from 'src/app/models/device/properties/size';
import { FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Type } from 'src/app/models/device/device';
import { Radio } from 'src/app/shared/components/radio/radio-group.component';
import { FormValueConverterHelper } from 'src/app/helpers/form-value-converter.helper';
import { Advertiser, AdvertiserService } from 'src/app/shared/services/advertiser.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {

  @Output() descriptionChange = new EventEmitter<Description>();
  @Input() type: Type;
  @Input() description: Description;
  @Input() readOnly = false;

  checkboxPositions: Radio[] = [
    {
      label: 'Sur mur ou clôture',
      valueSelected: Position.WALL
    },
    {
      label: 'Lumineux',
      valueSelected: Position.BRIGHT
    },
    {
      label: 'Scellé au sol',
      valueSelected: Position.FLOOR
    },
    {
      label: 'Sur autre support',
      valueSelected: Position.OTHER_SUPPORT
    },
    {
      label: 'Numérique',
      valueSelected: Position.NUMERIC
    },
  ];
  inputNumberPattern: String = '^[0-9]{1,2}([,.][0-9]{1,2})?$';
  categories: Category[] = <Category[]>Object.keys(Category).filter((category) => !(category === Category[Category.NONE]));
  descriptionForm: FormGroup;

  derogatoryControl: FormControl;

  allTypes = Type;

  radiosIsEstimated: Radio[] = [
    {
      label: 'Appréciées',
      valueSelected: true,
      click: value => this.patchDimensionValues(value)
    },
    {
      label: 'Mesurées',
      valueSelected: false,
      click: value => this.patchDimensionValues(value)
    }
  ];
  radiosDerogatory = [
    {
      label: 'MH',
      valueSelected: DeragotoryType.MH
    },
    {
      label: 'Activité culturelle',
      valueSelected: DeragotoryType.CULTURAL_ACTIVITY
    },
    {
      label: 'À déterminer',
      valueSelected: DeragotoryType.TO_DETERMINE
    },
    {
      label: 'Temporaire',
      valueSelected: DeragotoryType.TEMPORARY
    }
  ];
  radiosOnWall: Radio[] = [
    {
      label: 'Sur façade',
      valueSelected: Position.FACADE,
      click: value => {
        this.checkboxPositions[0] = this.radiosOnWall.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(0).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur mur aveugle',
      valueSelected: Position.BLIND_WALL,
      click: value => {
        this.checkboxPositions[0] = this.radiosOnWall.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(0).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur mur non aveugle',
      valueSelected: Position.NOT_BLIND_WALL,
      click: value => {
        this.checkboxPositions[0] = this.radiosOnWall.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(0).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur mur de cimetière',
      valueSelected: Position.CEMETERY,
      click: value => {
        this.checkboxPositions[0] = this.radiosOnWall.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(0).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur mur de jardin public',
      valueSelected: Position.PUBLIC_GARDEN,
      click: value => {
        this.checkboxPositions[0] = this.radiosOnWall.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(0).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur clôture aveugle',
      valueSelected: Position.BLIND_FENCE,
      click: value => {
        this.checkboxPositions[0] = this.radiosOnWall.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(0).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur clôture non aveugle',
      valueSelected: Position.NOT_BLIND_FENCE,
      click: value => {
        this.checkboxPositions[0] = this.radiosOnWall.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(0).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur toiture',
      valueSelected: Position.ROOF,
      click: value => {
        this.checkboxPositions[0] = this.radiosOnWall.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(0).setValue(value);
        this.openChoices = -1;
      }
    }
  ];
  radiosOtherSupport: Radio[] = [
    {
      label: 'Sur arbre',
      valueSelected: Position.TREE,
      click: value => {
        this.checkboxPositions[3] = this.radiosOtherSupport.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(3).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur poteau',
      valueSelected: Position.POLE,
      click: value => {
        this.checkboxPositions[3] = this.radiosOtherSupport.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(3).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur équipement public',
      valueSelected: Position.PUBLIC_FURNITURE,
      click: value => {
        this.checkboxPositions[3] = this.radiosOtherSupport.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(3).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur mobilier urbain',
      valueSelected: Position.URBAN_FURNITURE,
      click: value => {
        this.checkboxPositions[3] = this.radiosOtherSupport.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(3).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur bâche',
      valueSelected: Position.TARPAULIN,
      click: value => {
        this.checkboxPositions[3] = this.radiosOtherSupport.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(3).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur véhicule terrestre',
      valueSelected: Position.VEHICULE,
      click: value => {
        this.checkboxPositions[3] = this.radiosOtherSupport.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(3).setValue(value);
        this.openChoices = -1;
      }
    },
    {
      label: 'Sur plantation',
      valueSelected: Position.PLANTATION,
      click: value => {
        this.checkboxPositions[3] = this.radiosOtherSupport.find(onWall => onWall.valueSelected === value);
        (this.descriptionForm.controls.positions as FormArray).at(3).setValue(value);
        this.openChoices = -1;
      }
    }
  ];
  openChoices: number;
  advertisers: Advertiser[] = [];

  constructor(private formBuilder: FormBuilder,
              private authenticationService: AuthenticationService,
              private advertiserService: AdvertiserService) {
    const positionsFormArray = this.checkboxPositions.map(() => this.formBuilder.control(null));
    this.derogatoryControl = this.formBuilder.control(false);
    this.descriptionForm = this.formBuilder.group({
      derogatory: this.derogatoryControl,
      derogatoryType: this.formBuilder.control(DeragotoryType.MH),
      id: this.formBuilder.control(null),
      dimension: this.formBuilder.group({
        id: this.formBuilder.control(null),
        isEstimated: this.formBuilder.control(true.toString()),
        size: this.formBuilder.group({
          id: this.formBuilder.control(null),
          width: this.formBuilder.control(0),
          height: this.formBuilder.control(0),
          deviceGroundDistance: this.formBuilder.control(0),
          mastWidth: this.formBuilder.control(0),
          protrusionGroundDistance: this.formBuilder.control(0),
          category: this.formBuilder.control(this.categories[0])
        })
      }),
      advertiser: this.formBuilder.control({id: null, name: ''}, DescriptionComponent.advertiserHaveName()),
      announcer: this.formBuilder.control('', Validators.required),
      positions: this.formBuilder.array(positionsFormArray, DescriptionComponent.requiredOneChecked())
    });
    this.advertiserService.findAllByDepartement(this.authenticationService.currentUserValue.departement.code).subscribe(advertisers => {
      this.advertisers = advertisers;
    });
  }

  static advertiserHaveName(): ValidatorFn {
    return function (formControl: FormControl): ValidationErrors | null {
      const checkIfHaveName = !!(formControl.value.name) // type coercion make "" conversed as false
        && formControl.value.name.trim().length > 0;
      return checkIfHaveName ? null : {advertiserHaveName: null};
    };
  }

  static requiredOneChecked(): ValidatorFn {
    return function (formArray: FormArray): ValidationErrors | null {
      const checkIfOneTrue = formArray.controls.map(control => control.value).reduce((prevValue, value) => prevValue || value);
      return checkIfOneTrue ? null : {requiredOneChecked: null};
    };
  }

  ngOnInit(): void {
    if (this.description && this.description.dimension) {
      const positions = [];
      const firstItem = this.radiosOnWall.find(onWall =>
        onWall.valueSelected === this.description.positions.find(position => onWall.valueSelected === position));
      if (firstItem) {
        this.checkboxPositions[0] = firstItem;
      }
      const secondItem = this.radiosOtherSupport.find(otherSupport =>
        otherSupport.valueSelected === this.description.positions.find(position => otherSupport.valueSelected === position));
      if (secondItem) {
        this.checkboxPositions[3] = secondItem;
      }
      Array.from(this.checkboxPositions).forEach(checkbox => {
        if (this.description.positions.some(position => position === checkbox.valueSelected)) {
          positions.push(checkbox.valueSelected);
        } else {
          positions.push(null);
        }
      });
      this.descriptionForm.patchValue({
        id: this.description.id,
        derogatory: !!this.description.derogatoryType,
        derogatoryType: this.description.derogatoryType,
        dimension: {
          id: this.description.dimension.id,
          isEstimated: this.description.dimension.isEstimated,
          size: {
            id: this.description.dimension.size.id,
            width: this.description.dimension.size.width || 0,
            height: this.description.dimension.size.height || 0,
            deviceGroundDistance: this.description.dimension.size.deviceGroundDistance || 0,
            mastWidth: this.description.dimension.size.mastWidth * 100 || 0,
            protrusionGroundDistance: this.description.dimension.size.protrusionGroundDistance * 100 || 0,
            category: this.description.dimension.size.category
          }
        },
        announcer: this.description.announcer,
        advertiser: this.description.advertiser || {id: null, name: ''},
        positions: positions
      });
    }
    this.formChange();
  }

  formChange() {
    this.descriptionForm.valueChanges.subscribe(values => {
      this.description = values;
      if (!values.derogatory) {
        this.description.derogatoryType = null;
      }
      this.description.positions = values.positions.filter(position => position !== null);
      this.description.dimension.size.width = FormValueConverterHelper.convertToNumber(values.dimension.size.width);
      this.description.dimension.size.height = FormValueConverterHelper.convertToNumber(values.dimension.size.height);
      this.description.dimension.size.deviceGroundDistance =
        FormValueConverterHelper.convertToNumber(values.dimension.size.deviceGroundDistance);
      this.description.dimension.size.mastWidth = FormValueConverterHelper.convertToNumber(values.dimension.size.mastWidth) / 100;
      this.description.dimension.size.protrusionGroundDistance =
        FormValueConverterHelper.convertToNumber(values.dimension.size.protrusionGroundDistance) / 100;

      this.descriptionChange.emit(this.description);
    });
  }

  patchDimensionValues(isEstimated: boolean) {
    if (isEstimated) {
      this.descriptionForm.patchValue({
        dimension: {
          size: {
            category: this.categories[0]
          }
        }
      });
    } else {
      this.descriptionForm.patchValue({
        dimension: {
          size: {
            width: 0,
            height: 0,
            deviceGroundDistance: 0,
            mastWidth: 0,
            protrusionGroundDistance: 0,
            category: Category.NONE
          }
        }
      });
    }
  }

  openChoicesOf(index: number) {
    if (index === 0) {
      const positions = this.descriptionForm.controls.positions as FormArray;
      if (positions.at(0).value === null) {
        this.openChoices = 0;
      } else {
        this.checkboxPositions[0] = {
          label: 'Sur mur ou clôture',
          valueSelected: Position.WALL,
          id: 'wall'
        };
        positions.at(0).setValue(null);
      }
    }
    if (index === 3) {
      const positions = this.descriptionForm.controls.positions as FormArray;
      if (positions.at(3).value === null) {
        this.openChoices = 3;
      } else {
        this.checkboxPositions[3] = {
          label: 'Sur autre support',
          valueSelected: Position.OTHER_SUPPORT,
          id: 'other-support'
        };
        positions.at(3).setValue(null);
      }
    }
  }
}
