import { Component } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: SearchInputComponent,
    multi: true
  }]
})
export class SearchInputComponent implements ControlValueAccessor {
  disabled: boolean;
  private _value: string;

  onChange = (_: any) => { };
  onTouched = () => { };

  writeValue(obj: string): void {
    this.value = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  get value() {
    return this._value;
  }

  set value(value: string) {
    if (value !== undefined && value !== this._value) {
      this._value = value;
      this.onChange(value);
    }
  }
}
