import { Component, EventEmitter, Input, Output } from '@angular/core';
import { transition, trigger } from '@angular/animations';
import { SubjectsService } from 'src/app/services/subjects.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  animations: [
    trigger('modal', [
      transition('void => *', [
      ]),
      transition('* => void', [
      ])
    ])
  ]
})
/** @deprecated use {@link confirm-modal} instead */
export class ModalComponent {
  @Input() closable = true;
  @Input() visible: boolean;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private subjectsService: SubjectsService) { }

  close() {
    this.visible = false;
    this.visibleChange.emit(this.visible);
    this.subjectsService.openItemModaleForPhoto(false);
  }

}
