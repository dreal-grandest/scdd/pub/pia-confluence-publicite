import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-loader',
  styleUrls: ['./loader.component.scss'],
  templateUrl: './loader.component.html',
})
export class LoaderComponent {
  static readonly DEFAULT_LABEL = 'Un instant ...';
  @Input() modaly = false;
  @Input() iconOnly = false;
  @Input() label = LoaderComponent.DEFAULT_LABEL;
}
