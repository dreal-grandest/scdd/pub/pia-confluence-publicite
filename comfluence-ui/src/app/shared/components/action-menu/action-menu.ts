import { Directive, HostBinding, ElementRef } from '@angular/core';

@Directive({
    selector: '[appActionMenu]'
})
export class ActionMenuDirective {
    @HostBinding('style.display') display = 'none';

    constructor(public host: ElementRef) {
    }

    get element() {
        return this.host.nativeElement;
    }

}
