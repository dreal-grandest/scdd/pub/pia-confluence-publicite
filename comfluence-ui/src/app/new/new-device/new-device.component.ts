import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LatLng, LatLngBounds } from 'leaflet';
import { Subscription } from 'rxjs';

import { Device, Status } from 'src/app/models/device/device';
import { Aspect, Photo } from 'src/app/models/device/properties/photo';
import { Address } from 'src/app/models/device/properties/address';
import { Context } from 'src/app/models/device/properties/context';
import { Description } from 'src/app/models/device/properties/description';
import { GeoApi } from 'src/app/models/geoapi';

import { InseeService } from 'src/app/services/insee.service';
import { CoordinatesService } from 'src/app/services/coordinates.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { PhotoService } from 'src/app/services/photo.service';
import { DeviceService } from 'src/app/services/device.service';
import { first } from 'rxjs/operators';
import { createBox4326 } from '../../models/box';

@Component({
  selector: 'app-new-device',
  templateUrl: './new-device.component.html',
  styleUrls: ['./new-device.component.scss']
})
export class NewDeviceComponent implements OnInit, OnDestroy {
  id: number;
  item: Device;
  modalIsOpen: boolean;
  isCompleted = false;
  newDevice = true;
  nearbyDevices: Device[] = [];
  subscribedList: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private photoService: PhotoService,
    private deviceService: DeviceService,
    private inseeService: InseeService,
    private coordinatesService: CoordinatesService,
    private authenticationService: AuthenticationService) {
  }

  ngOnInit(): void {
    const subscription = this.route.queryParams.subscribe(params => {
      if (params['id']) {
        this.newDevice = false;
        this.id = params['id'];
        this.deviceService.get(this.id).subscribe((data) => {
            this.item = data;
            this.coordinatesService.updateCoordinates(this.item.coordinates);
            this.subscribedList.push(this.coordinatesService.coordinates.subscribe(coordinates => {
              this.item.coordinates = coordinates;
              this.findNearbyDevices();
            }));
            this.photoService.getPhotosBy(this.id).subscribe((listPhoto) => {
                this.item.photos = listPhoto;
              },
              (error: any) => console.error(error)
            );
          },
          (error: any) => console.error(error)
        );
        return;
      }
      if (params['guidance'] && params['guidance'] !== '' && params['type'] && params['type'] !== '') {
        const item = new Device();
        item.status = Status.TODO;
        item.type = params['type'];
        item.guidance = params['guidance'];
        item.description = new Description();
        item.address = new Address();
        item.context = new Context();
        this.item = item;
        this.coordinatesService.locationCoordinates.pipe(first()).subscribe({
          next: location => {
            this.coordinatesService.updateCoordinates(location);
            this.coordinatesService.coordinates.subscribe(coordinates => {
              this.item.coordinates = coordinates;
              this.findNearbyDevices();
            });
          }
        });
        this.modalIsOpen = false;
      } else {
        this.modalIsOpen = true;
      }
    });
    this.subscribedList.push(subscription);
  }

  private findNearbyDevices() {
    const nearbyBounds: LatLngBounds = new LatLng(this.item.coordinates.latitude, this.item.coordinates.longitude)
      .toBounds(20 * 2); // multiply by 2 as to bounds is dividing by 2
    const box = createBox4326({
      minLat: nearbyBounds.getSouthWest().lat,
      minLong: nearbyBounds.getSouthWest().lng,
      maxLat: nearbyBounds.getNorthEast().lat,
      maxLong: nearbyBounds.getNorthEast().lng
    });
    const subscription = this.deviceService.findAllFromBox(box).subscribe(items => {
      this.nearbyDevices = items.filter(item => item.id !== this.item.id);
    });
    this.subscribedList.push(subscription);
  }

  handlePhotos($event: Photo[]): void {
    this.item.photos = $event;
    this.onCheckIfFormIsCompleted();
  }

  handleAddress(address: Address): void {
    this.item.address = address;
    if (this.item.address && this.item.address.inseeCode) {
      this.inseeService.searchCommuneByCodeInsee(this.item.address.inseeCode).pipe(first())
        .subscribe((commune: GeoApi.Commune) => {
          if (commune) {
            this.item.context = Object.assign({}, this.item.context, {
              population: commune.population,
              town: commune.nom
            });
            const uniteUrbaine = this.inseeService.searchUniteUrbaineLabel(commune.code);
            this.item.context = Object.assign({}, this.item.context, {
              unit: uniteUrbaine
            });
            this.onCheckIfFormIsCompleted();
          }
        });
    }
    this.onCheckIfFormIsCompleted();
  }

  handleDescription(description: Description): void {
    this.item.description = description;
    this.onCheckIfFormIsCompleted();
  }

  handleContext(context: Context) {
    this.onCheckIfFormIsCompleted();
  }

  handleItem(device: Device) {
    this.item = device;
  }

  onCheckIfFormIsCompleted() {
    this.isCompleted = this.checkAdress()
      && this.checkPhotos()
      && this.checkContext()
      && this.checkDescription();
  }

  checkAdress(): boolean {
    if (this.item.address) {
      return this.item.address.city !== ''
        && this.item.address.code && this.item.address.code.toString() !== '';
    } else {
      return false;
    }
  }

  checkDescription(): boolean {
    if (this.item.description) {
      return this.item.description.announcer !== ''
        && !!(this.item.description.advertiser.name) && this.item.description.advertiser.name.trim().length > 0
        && this.item.description.dimension !== null
        && this.item.description.positions !== null;
    } else {
      return false;
    }
  }

  checkContext(): boolean {
    if (this.item.context) {
      return this.item.context.unit !== ''
        && this.item.context.town !== ''
        && this.item.context.population.toString() !== '';
    } else {
      return false;
    }
  }

  checkPhotos(): boolean {
    let result = false;
    if (this.item.photos.length > 0) {
      const photoButeau: Photo[] = this.item.photos.filter((photo) => {
        return photo.photoType === Aspect.BUTEAU;
      });
      result = (photoButeau.length > 0) && (!photoButeau[0].active || !!photoButeau[0].imageLinked);
      const nearAndAllPhoto: Photo[] = this.item.photos.filter((photo) => {
        return photo.photoType === Aspect.NEAR || photo.photoType === Aspect.ALL;
      });
      result = result && nearAndAllPhoto.every(photo => {
        return photo.imageLinked;
      });
    }
    return result;
  }

  ngOnDestroy(): void {
    this.subscribedList.forEach(subscription => subscription.unsubscribe());
  }
}
