import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Address } from 'src/app/models/device/properties/address';
import { Device } from 'src/app/models/device/device';
import { FormControl, FormGroup } from '@angular/forms';
import { InseeService } from 'src/app/services/insee.service';
import { DeviceService } from 'src/app/services/device.service';
import { CoordinatesService } from 'src/app/services/coordinates.service';
import { Coordinates } from 'src/app/models/device/properties/coordinates';
import { InseeGeo } from 'src/app/models/inseegeo';
import { GeoApi } from 'src/app/models/geoapi';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {
  @Output() addressEmitter = new EventEmitter<Address>();
  @Input() item: Device;
  @Input() locked: Boolean = true;
  @Input() nearbyDevices: Device[] = [];


  itemStatusType = {
    status: '',
    type: '',
    id: 0
  };
  addressForm = new FormGroup({
    city: new FormControl(''),
    code: new FormControl(''),
    number: new FormControl(''),
    street: new FormControl(''),
    inseeCode: new FormControl(''),
  });

  constructor(private inseeService: InseeService, private deviceService: DeviceService, private coordinatesService: CoordinatesService) {
  }

  ngOnInit(): void {
    if (this.item) {
      this.itemStatusType.status = this.item.status;
      this.itemStatusType.type = this.item.type;
      this.itemStatusType.id = this.item.id;
    }
    if (this.item.address) {
      this.addressForm.patchValue({
        city: this.item.address.city,
        code: this.item.address.code,
        number: this.item.address.number,
        street: this.item.address.street,
        inseeCode: this.item.address.inseeCode,
      });
    }
    this.coordinatesService.coordinates.subscribe(coordinates => {
      this.handleEndCoordinates(coordinates);
    });
    this.onChanges();
  }

  onChanges() {
    this.addressForm.valueChanges.subscribe(values => {
      let address = <Address>{
        city: values.city,
        code: values.code,
        number: values.number,
        street: values.street,
        inseeCode: values.inseeCode,
      };
      if (this.item.address) {
        address = Object.assign(this.item.address, address);
      }
      this.addressEmitter.emit(address);
    });
  }

  handleEndCoordinates(coordinates: Coordinates): void {
    this.inseeService.searchAddressByCoordinate(coordinates.latitude, coordinates.longitude).subscribe((result: InseeGeo.SearchResult) => {
      if (result.features.length > 0) {
        const feature = result.features[0];
        this.addressForm.setValue({
          city: feature.properties.city ? feature.properties.city : '',
          code: feature.properties.postcode ? feature.properties.postcode : '',
          number: feature.properties.housenumber ? feature.properties.housenumber : '',
          street: feature.properties.street ? feature.properties.street : '',
          inseeCode: feature.properties.citycode ? feature.properties.citycode : '',
        });
      } else {
        this.inseeService.searchCommuneByCoordinate(coordinates.latitude, coordinates.longitude).subscribe((communes: GeoApi.Commune[]) => {
          if (communes.length > 0) {
            const commune = communes[0];
            this.addressForm.setValue({
              city: commune.nom,
              code: commune.codesPostaux[0],
              number: '',
              street: '',
              inseeCode: commune.code,
            });
          } else {
            this.addressForm.setValue({
              city: '',
              code: '',
              number: '',
              street: '',
              inseeCode: '',
            });
          }
        });
      }
    }, function (error) {
        this.toastrSerivce.error('Le service de l\'insee est injoignable', 'Erreur de récupération de l\'adresse', {
            positionClass: 'toast-top-full-width'
        });
      console.error(error);
    });
  }
}
