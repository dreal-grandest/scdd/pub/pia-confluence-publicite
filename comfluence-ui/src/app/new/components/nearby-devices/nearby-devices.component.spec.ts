import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearbyDevicesComponent } from './nearby-devices.component';

describe('NearbyDevicesComponent', () => {
  let component: NearbyDevicesComponent;
  let fixture: ComponentFixture<NearbyDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NearbyDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearbyDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
