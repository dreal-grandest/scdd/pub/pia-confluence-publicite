import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Device } from 'src/app/models/device/device';
import { Aspect, Photo } from 'src/app/models/device/properties/photo';
import { PhotoService } from 'src/app/services/photo.service';
import { forkJoin, Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-nearby-devices',
  templateUrl: './nearby-devices.component.html',
  styleUrls: ['./nearby-devices.component.scss']
})
export class NearbyDevicesComponent implements OnChanges {
  @Input() warningMessage: boolean;
  @Input() nearbyDevices: Device[] = [];
  photos: Photo[] = [];

  constructor(private photoService: PhotoService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.nearbyDevices && this.nearbyDevices.length > 0) {
      const photosObservable$: Observable<Photo[]>[] = [];
      this.nearbyDevices.forEach(device =>
        photosObservable$.push(this.photoService.getPhotosBy(device.id))
      );
      forkJoin(photosObservable$).pipe(take(1)).subscribe(allPhotos => {
        this.photos = [];

        allPhotos.forEach(photos => {
          const photoFound = photos.find(photo => photo.photoType === Aspect.NEAR);
          if (photoFound) {
            this.photos.push(photoFound);
          }
        });
      });
    } else {
      this.photos = [];
    }
  }
}
