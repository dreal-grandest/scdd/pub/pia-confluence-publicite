import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Aspect, Photo, PhotoStatus } from '../../../models/device/properties/photo';

const MANDATORY_ASPECT = [Aspect.ALL, Aspect.NEAR, Aspect.BUTEAU];

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss']
})
export class PhotoComponent {

  @Input() photo: Photo;
  @Output() photoEmitter = new EventEmitter<Photo>();
  @Input() deactivable = false;
  modalIsOpen = false;
  previousPhotoLink: String;

  isMandatory() {
    return MANDATORY_ASPECT.filter(aspect => this.photo.photoType === aspect).length > 0;
  }

  handleFile(file: String) {
    if (file === null) {
      if (this.isMandatory()) {
        this.photo = new Photo(this.photo.photoType);
      } else {
        this.photo.status = PhotoStatus.TO_DELETE;
      }
      this.photo.imageLinked = false;
    } else {
      this.photo.imageFile = file;
      this.photo.imageLinked = true;
      this.photo.status = PhotoStatus.TO_SAVE;
      this.photo.photoDate = new Date().toISOString();
    }
  }

  showModal(): void {
    if (this.photo.imageLinked) {
      this.previousPhotoLink = this.photo.imageFile;
    } else {
      this.previousPhotoLink = null;
    }

    this.modalIsOpen = true;
  }

  handleClose(toggle: boolean): void {
    this.photoEmitter.emit(this.photo);
    this.modalIsOpen = toggle;
  }

  toggleRadio($event: MouseEvent) {
    this.photo.active = !this.photo.active;
    this.photo.status = PhotoStatus.TO_SAVE;
    this.photoEmitter.emit(this.photo);
  }
}
