import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotosBlockComponent } from './photos-block.component';

describe('PhotosBlockComponent', () => {
  let component: PhotosBlockComponent;
  let fixture: ComponentFixture<PhotosBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotosBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotosBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
