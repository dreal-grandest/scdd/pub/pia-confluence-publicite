import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Aspect, Photo, PhotoStatus } from 'src/app/models/device/properties/photo';

@Component({
  selector: 'app-photos-block',
  templateUrl: './photos-block.component.html',
  styleUrls: ['./photos-block.component.scss']
})
export class PhotosBlockComponent implements OnInit, OnChanges {
  private static readonly MANDATORY_ASPECT = [Aspect.ALL, Aspect.NEAR, Aspect.BUTEAU];

  modalIsOpen = false;
  @Output() photosEmitter = new EventEmitter<Photo[]>();
  @Input() photos: Photo[];

  photosMap: Map<Aspect, Photo>;
  buteauAspect = Aspect.BUTEAU;

  constructor() {
    this.initMap();
  }

  ngOnInit() {
    this.initPhotosMap();
  }

  ngOnChanges() {
    this.initPhotosMap();
  }

  private initMap() {
    this.photosMap = new Map<Aspect, Photo>();
    PhotosBlockComponent.MANDATORY_ASPECT.filter(aspect => this.photosMap.set(aspect, new Photo(aspect)));
  }

  private initPhotosMap() {
    if (this.photos) {
      this.initMap();
      this.photos.filter(photo => photo.status !== PhotoStatus.TO_DELETE).forEach(photo => {
        this.photosMap.set(photo.photoType, photo);
      });
      this.photosMap = new Map<Aspect, Photo>(this.photosMap);
    }
  }

  createNewPhoto() {
    this.modalIsOpen = true;
  }

  closeModal(toggle: boolean): void {
    this.modalIsOpen = toggle;
  }

  handlePhoto(photo: Photo) {
    this.photosMap.set(photo.photoType, photo);
    this.photosEmitter.emit(Array.from(this.photosMap.values()));
  }
}

