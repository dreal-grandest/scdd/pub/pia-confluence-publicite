import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { ActionHelper, CachedHttpError } from 'src/app/helpers/action.helper';
import { Device, Status } from 'src/app/models/device/device';
import { InfrigementStatus } from 'src/app/models/device/properties/infrigement';
import { DeviceService } from 'src/app/services/device.service';
import { COLOR_SET } from 'src/app/shared/components/confirm-modal/confirm-modal.component';
import { ActionsConstantes } from 'src/app/helpers/action.constants';

@Component({
  selector: 'app-edit-header',
  templateUrl: './edit-header.component.html',
  styleUrls: ['./edit-header.component.scss']
})
export class EditHeaderComponent {
  @Output() itemEmitter = new EventEmitter<Device>();
  @Input() item: Device;
  @Input() isValidate: boolean;
  isLoading = false;
  modalOpen = false;

  popupStates = [
    {
      id: 0,
      context: COLOR_SET.INFORMATION,
      title: 'Confirmation',
      content: 'Mon contrôle est terminé ?',
      confirmLabel: 'Oui, contrôle terminé',
      cancelLabel: 'Non, contrôle à terminer'
    },
    {
      id: 1,
      context: COLOR_SET.WARNING,
      title: 'Attention',
      content: 'Souhaitez-vous vraiment annuler votre saisie et ne pas enregistrer les données de ce dispositif ?',
      confirmLabel: 'Oui',
      cancelLabel: 'Non'
    },
    {
      id: 2,
      context: COLOR_SET.WARNING,
      title: 'Attention',
      content: 'Souhaitez-vous vraiment supprimer ce dispositif ?',
      confirmLabel: 'Oui',
      cancelLabel: 'Non'
    }
  ];
  selectedState: { context: COLOR_SET; id: number; title: string; content: string, confirmLabel: string, cancelLabel: string };

  constructor(private router: Router,
    private toastrSerivce: ToastrService,
    private actionHelper: ActionHelper,
    private deviceService: DeviceService) {
  }

  cancel() {
    if (this.selectedState && this.selectedState.id === 0) {
      this.submit(Status.TODO);
    }
  }

  submit(status: Status) {
    this.isLoading = true;
    this.item.operationDate = new Date();
    this.item.status = status;

    this.actionHelper.doAction(ActionsConstantes.ACTION_DEVICE_SAVE, this.item).pipe(first()).subscribe(savedDevice => {
      this.itemEmitter.emit(savedDevice);
      this.isLoading = false;
      // to stay in edit on the current device
      this.router.navigate(['/items']);
      this.toastrSerivce.success('Le dispositif a correctement été sauvegardé');
    }, (error: CachedHttpError) => {
      if (!error.cached) {
        this.isLoading = false;
        // to stay in edit on the current device
        this.toastrSerivce.error('Erreur de téléchargement');
      } else {
        this.itemEmitter.emit(this.item);
        this.router.navigate(['/items']);
        this.toastrSerivce.warning('Dispositif en attente de téléchargement');
      }
      this.isLoading = false;
    });
  }

  showModal(modalToOpen: number) {
    this.selectedState = this.popupStates.find(popup => popup.id === modalToOpen);
    this.modalOpen = true;
  }

  submitDone() {
    if (this.selectedState) {
      switch (this.selectedState.id) {
        case 0:
          let status;
          if (this.item.infrigements && this.item.infrigements.filter(infr => infr.status !== InfrigementStatus.TO_DELETE).length > 0) {
            status = Status.NONCOMPLIANT;
          } else {
            status = Status.COMPLIANT;
          }
          this.submit(status);
          break;
        case 1:
          this.router.navigate(['/items']);
          break;
        case 2:
          this.deviceService.delete(this.item.id).subscribe(() => this.router.navigate(['/items']));
          break;
      }
    }
  }
}
