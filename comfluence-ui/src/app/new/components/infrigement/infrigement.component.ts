import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Infrigement, InfrigementStatus } from 'src/app/models/device/properties/infrigement';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { InfrigementService } from 'src/app/shared/services/infrigement.service';
import { debounceTime, first } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-infrigement',
  templateUrl: './infrigement.component.html',
  styleUrls: ['./infrigement.component.scss']
})
export class InfrigementComponent {

  @Input() infrigements?: Infrigement[] = [];
  @Output() infrigementsChange: EventEmitter<Infrigement[]> = new EventEmitter<Infrigement[]>();

  foundInfrigements: Infrigement[] = [];
  modalOpen = false;
  searchTextControl: FormControl;

  hasMoreResult = false;
  private valueChanges: Subscription;
  modalRemove = false;
  removeFormGroup: FormGroup;
  toDeleteStatus = InfrigementStatus.TO_DELETE;

  constructor(private infrigementService: InfrigementService,
    private formBuilder: FormBuilder) {
    this.searchTextControl = this.formBuilder.control(null);
    this.removeFormGroup = this.formBuilder.group({
      id: this.formBuilder.control(null),
      comment: this.formBuilder.control(null)
    });
  }

  openModalAdd() {
    this.searchTextControl.reset(null, { emitEvent: false });
    this.initValueChanges();
    this.foundInfrigements = [];
    this.hasMoreResult = false;
    this.modalOpen = true;
  }

  // init valueChanges everytime the popin is opened up
  // {emitEvent: false} still emit event with custom implementation of ControlValueAccessor (SearchInputComponent)
  // FIXME : in future, if angular fix the problem
  private initValueChanges() {
    if (this.valueChanges) {
      this.valueChanges.unsubscribe();
    }
    this.valueChanges = this.searchTextControl.valueChanges.pipe(debounceTime(1000)).subscribe({
      next: (text: string) => this.infrigementService.findAll().pipe(first())
        .subscribe({
          next: infrigements => {
            let keyWords = [];
            if (text) {
              keyWords = text.split(/ /)
                .filter(keyWord => keyWord.trim().length !== 0)
                .map(keyWord => keyWord.toLowerCase());
            }
            const filtered = infrigements.filter(infrigement => {
              return keyWords.every(keyWord => new RegExp(keyWord)
                .test(infrigement.offenseNature.toLowerCase()) || keyWord === infrigement.natinfCode)
                && !this.infrigements.find(selected => selected.id === infrigement.id
                  && selected.status !== InfrigementStatus.TO_DELETE);
            });
            this.foundInfrigements = filtered.slice(0, 5);
            this.hasMoreResult = filtered.length > 5;
          }
        })
    });
  }

  showRemovalPopin(infrigement: Infrigement) {
    if (infrigement.status === InfrigementStatus.TO_SAVE) {
      const indexFound = this.infrigements.indexOf(infrigement);
      this.infrigements.splice(indexFound, 1);
    } else {
      this.removeFormGroup.setValue({
        id: infrigement.id,
        comment: null
      });
      this.modalRemove = true;
    }
  }

  // popin action
  selectInfrigement(infrigement: Infrigement) {
    const found = this.infrigements.find(infr => infr.id === infrigement.id);
    if (found) {
      found.comment = null;
      found.status = InfrigementStatus.SAVED;
    } else {
      infrigement.status = InfrigementStatus.TO_SAVE;
      this.infrigements.push(infrigement);
    }
    this.infrigementsChange.emit(this.infrigements);
    this.modalOpen = false;
  }

  acceptRemoval(value: any) {
    const removed = this.infrigements.find(infrigement => infrigement.id === value.id);
    removed.comment = value.comment;
    removed.status = InfrigementStatus.TO_DELETE;
    this.modalRemove = false;
  }
}
