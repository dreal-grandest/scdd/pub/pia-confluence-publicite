import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Aspect, Photo, PhotoStatus } from '../../../models/device/properties/photo';

@Component({
  selector: 'app-new-photo',
  templateUrl: './new-photo.component.html',
  styleUrls: ['./new-photo.component.scss']
})
export class NewPhotoComponent implements OnInit {
  static readonly DEFAULT_ASPECTS = [Aspect.DOUBLE_FACE, Aspect.REEXAMINATION];

  @Output() isOpen = new EventEmitter<boolean>();
  @Output() newPhoto = new EventEmitter<Photo>();

  optionalAspects = NewPhotoComponent.DEFAULT_ASPECTS;
  selectedType: Aspect;
  photo: Photo;

  ngOnInit(): void {
    this.photo = new Photo(this.selectedType);
  }

  onCloseModal(): void {
    this.isOpen.emit(false);
  }

  handleFile(file: String) {
    if (file) {
      this.photo.imageFile = file;
      this.photo.status = PhotoStatus.TO_SAVE;
      this.photo.imageLinked = true;
      this.photo.photoType = this.selectedType;
      this.photo.photoDate = new Date().toISOString();
      this.newPhoto.emit(this.photo);
    }
  }
}
