import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/auth.guard';
import { NgModule } from '@angular/core';

/* ROUTES */
const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'items',
    loadChildren: () => import('./items/items.module').then(m => m.ItemsModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'search',
    loadChildren: () => import('./search/search.module').then(m => m.SearchModule),
    canActivate: [AuthGuard],
    data: {authRole: ['USER']}
  },
  {
    path: 'new',
    loadChildren: () => import('./new/new.module').then(m => m.NewModule),
    canActivate: [AuthGuard],
    data: {authRole: ['USER']}
  },
  {
    path: 'history',
    loadChildren: () => import('./history/history.module').then(m => m.HistoryModule),
    canActivate: [AuthGuard],
    data: {authRole: ['USER']}
  },
  {
    path: 'userboard',
    loadChildren: () => import('./userboard/userboard.module').then(m => m.UserboardModule),
    canActivate: [AuthGuard],
    data: {authRole: ['ADMIN']}
  },
  {
    path: 'department',
    loadChildren: () => import('./department/department.module').then(m => m.DepartmentModule),
    canActivate: [AuthGuard],
    data: {authRole: ['ADMIN']}
  },
  // otherwise redirect to home
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
