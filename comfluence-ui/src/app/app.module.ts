/* @ANGULAR */
import { NgModule } from '@angular/core';
import localeFr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
/* APP */
import { AppComponent } from './app.component';
/* COMPOSANT */
import { ToastrModule } from 'ngx-toastr';
/* external SERVICES */
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HttpErrorInterceptor } from './services/http-error-interceptor';
/* INTERCEPTOR */
import { JwtInterceptor } from './helpers/jwt.interceptor';
/* ROUTING */
import { AppRoutingModule } from './app-routing.module';
/* internal MODULE */
import { SharedModule } from './shared/shared.module';
import { HttpConvertInterceptor } from './services/http-convert-interceptor';

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    SharedModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    ToastrModule.forRoot({
      preventDuplicates: true,
      countDuplicates: true,
      positionClass: 'toast-bottom-right'
    })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpConvertInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
