import { Component, OnInit } from '@angular/core';
import { Department, FullDepartment } from '../models/department';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { DepartementService } from '../services/departement.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss']
})
export class DepartmentComponent {
  departments: FullDepartment[] = [];
  columnModeFlex = ColumnMode.flex;
  selected: Department[] = [];
  selectionTypeSingle = SelectionType.single;
  modalEditDepartment = false;
  editedDepartmentForm: FormGroup;
  createMode: boolean;

  constructor(private departementService: DepartementService, private formBuilder: FormBuilder) {
    this.departementService.findAllFull().pipe(first()).subscribe(departments => this.departments = departments);
    this.initEditedDepartmentForm();
  }

  initEditedDepartmentForm() {
    this.editedDepartmentForm = this.formBuilder.group({
      code: this.formBuilder.control(undefined),
      name: this.formBuilder.control(''),
      box: this.formBuilder.group({
        minLat: this.formBuilder.control(undefined),
        maxLat: this.formBuilder.control(undefined),
        minLong: this.formBuilder.control(undefined),
        maxLong: this.formBuilder.control(undefined)
      }),
      territorialDirection: this.formBuilder.group({
        id: this.formBuilder.control(undefined),
        prefecture: this.formBuilder.control(undefined),
        service: this.formBuilder.control(undefined),
        pole: this.formBuilder.control(undefined),
        city: this.formBuilder.control(undefined),
        director: this.formBuilder.control(undefined),
        direction: this.formBuilder.control(undefined),
        phoneNumber: this.formBuilder.control(undefined),
        faxNumber: this.formBuilder.control(undefined),
        address: this.formBuilder.control(undefined),
        signingAuthority: this.formBuilder.control(undefined),
        firstNameSigningAuthority: this.formBuilder.control(undefined),
        lastNameSigningAuthority: this.formBuilder.control(undefined),
      })
    });
  }

  onSelect($event: any) {
    this.selected[0] = $event;
  }

  editDepartement(code: number) {
    this.createMode = false;
    const editedDepartment = this.departments.find(department => department.code === code);
    this.editedDepartmentForm.patchValue(editedDepartment);
    this.modalEditDepartment = true;
  }

  saveDepartement(department: FullDepartment) {
    this.departementService.saveFull(department).subscribe(savedDepartment => {
      const found = this.departments.findIndex(list => list.code === savedDepartment.code);
      if (found >= 0) {
        this.departments.splice(found, 1, savedDepartment);
      } else {
        this.departments.push(savedDepartment);
      }
      this.departments = [...this.departments];
    });
  }

  createDepartement() {
    this.createMode = true;
    this.initEditedDepartmentForm();
    this.modalEditDepartment = true;
  }
}
