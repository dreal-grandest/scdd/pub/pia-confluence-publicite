import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { DocumentService } from '../../shared/services/document.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-doc-viewer',
  templateUrl: './document-viewer.component.html',
  styleUrls: ['./document-viewer.component.scss'],
})
export class DocumentViewerComponent implements OnInit, OnChanges {
  @Input() id: number;
  @Input() src: string;

  docToShow: any;
  isDocLoading = true;

  constructor(private documentService: DocumentService, private authenticationService: AuthenticationService) {
  }

  ngOnInit(): void {
  }

  private initDoc() {
    if (this.src) {
      this.docToShow = this.src;
      this.isDocLoading = false;
    } else if (this.id) {
      this.getDocFromService();
    }
  }

  refresh() {
    this.initDoc();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initDoc();
  }


  private getDocFromService() {
    this.docToShow = {
      url: this.documentService.urlPreviewDocument(this.id) + '?contentType=application/pdf',
      httpHeaders: {
        Authorization: `Bearer ${this.authenticationService.currentUserValue.token}`
      }
    };
  }

  onDocLoaded() {
    this.isDocLoading = false;
  }
}
