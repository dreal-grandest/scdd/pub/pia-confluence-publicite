import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemsComponent } from './items.component';
import { SharedModule } from '../shared/shared.module';
import { SortContainerComponent } from './sort-container/sort-container.component';
import { RouterModule, Routes } from '@angular/router';
import { DocumentsComponent } from './documents/documents.component';
import { DocumentComponent } from './document/document.component';
import { AddressPhotosComponent } from './address-photos/address-photos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentViewerComponent } from './document-viewer/document-viewer.component';
import { VisualizeItemComponent } from './visualize-item/visualize-item.component';
import { FiltersComponent } from './components/filters/filters.component';
import { UploadOdtComponent } from './upload-odt/upload-odt.component';
import { NewProcedureComponent } from './new-procedure/new-procedure.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

const routes: Routes = [
  {path: '', component: ItemsComponent},
];


@NgModule({
  declarations: [
    ItemsComponent,
    SortContainerComponent,
    DocumentsComponent,
    DocumentComponent,
    AddressPhotosComponent,
    DocumentViewerComponent,
    VisualizeItemComponent,
    FiltersComponent,
    UploadOdtComponent,
    NewProcedureComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PdfViewerModule
  ]
})
export class ItemsModule { }
