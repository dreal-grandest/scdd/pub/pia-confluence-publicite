import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadOdtComponent } from './upload-odt.component';

describe('UploadOdtComponent', () => {
  let component: UploadOdtComponent;
  let fixture: ComponentFixture<UploadOdtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadOdtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadOdtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
