import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FilterService } from 'src/app/services/filter.service';
import { Filter } from 'src/app/models/device/Filter';
import 'rxjs/add/observable/forkJoin';
import { first } from 'rxjs/operators';
import { ReplayFiltersService } from '../../../shared/services/replay-filters.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss', '../../../shared/components/modal/modal.component.scss']
})
export class FiltersComponent implements OnInit {

  constructor(private replayFiltersService: ReplayFiltersService,
    private filterService: FilterService) {
  }

  private static readonly MAX_KEEP_RESULT = 5;

  @Output() cancel = new EventEmitter();
  @Output() filtersCreated = new EventEmitter<Filter[]>();
  @Input() idDevice: number;

  @Input() selectedFilters: Filter[] = [];

  filters: Filter[] = [];
  pendingFilters: Filter[] = [];
  lastUsedFilters: Filter[] = [];

  public ngOnInit(): void {
    this.filterService.findAll().pipe(first()).subscribe({
      next: allFilters => {
        this.filters = allFilters.sort((filterA, filterB) => this.alphabeticalCompare(filterA.label, filterB.label));
        this.filters.forEach(filter => {
          const isSelected = !!this.selectedFilters.find(selected => selected.id === filter.id);
          filter['selected'] = isSelected;
          if (isSelected) {
            this.pendingFilters.push(filter);
          }
        });
        this.replayFiltersService.lastResults.pipe(first()).subscribe({
          next: filters => {
            this.lastUsedFilters = filters;
            this.filters = this.filters.filter(findFilter => !this.lastUsedFilters.find(filter => filter.id === findFilter.id));
          }
        });
      }
    });
  }

  private alphabeticalCompare(strA: string, strB: string) {
    return strA.toLowerCase() < strB.toLowerCase() ? -1 : 1;
  }

  public addTag(filter: Filter): void {
    this.pendingFilters.push(filter);
    filter['selected'] = true;
  }

  public removeTag(filter: Filter): void {
    this.pendingFilters.splice(this.pendingFilters.indexOf(filter), 1);
    filter['selected'] = false;
  }

  public removeAllTags(): void {
    this.pendingFilters = [];
    [].concat(this.filters, this.lastUsedFilters).forEach(filter => {
      this.removeTag(filter);
    });
  }

  public saveFilters(): void {
    const newReplayResults = this.lastUsedFilters;
    this.selectedFilters = this.pendingFilters;
    this.selectedFilters.forEach(filter => {
      const indexOfLastUsed = newReplayResults.indexOf(filter);
      if (indexOfLastUsed > -1) {
        newReplayResults.splice(indexOfLastUsed, 1);
      }
      newReplayResults.push(filter);
      newReplayResults.splice(0, newReplayResults.length - FiltersComponent.MAX_KEEP_RESULT);
    });
    this.filtersCreated.emit(this.selectedFilters);
    this.replayFiltersService.updateLastResultsSubject(newReplayResults);
  }
}
