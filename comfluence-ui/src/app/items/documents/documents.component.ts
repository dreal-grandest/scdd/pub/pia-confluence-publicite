import { Component, Inject, Input, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { saveAs } from 'file-saver';
import { first } from 'rxjs/operators';
import { ServiceHelper } from 'src/app/helpers/service-helper';
import { Procedure, ProcedureGroupe } from 'src/app/models/device/Procedure';
import { ProcedureService } from 'src/app/services/procedure.service';
import { Device } from '../../models/device/device';
import { Document } from '../../models/device/document';
import { InformationType } from '../../models/device/InformationType';
import { DocumentService } from '../../shared/services/document.service';
import { DocumentViewerComponent } from '../document-viewer/document-viewer.component';
import { Observable } from 'rxjs';
import { formatDate } from '@angular/common';


@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {
  InformationType = InformationType;
  @Input() item: Device;
  file: File;
  donwloadURL: String;
  openModalFor: String;
  document: Document = new Document();
  selectedForPreview: Document;
  @ViewChild('fileElement', { static: false }) fileElement;
  @ViewChild('loader', { static: false }) loader;
  @ViewChild('docviewer', { static: false }) docViewer: DocumentViewerComponent;
  modalIsOpen = false;
  isWaitingResponse = false;
  closeProcedureModalVisible = false;
  procedures: Procedure[] = [];
  groups: ProcedureGroupe[] = [];

  maxCount = 255;
  commentControl: FormControl;

  get comment() {
    return this.documentForm.get('comment');
  }

  get error() {
    const comment = this.documentForm.get('comment');
    if (comment.invalid && (comment.dirty || comment.touched)) {
      if (comment.errors.required) {
        return 'Veuillez saisir un commentaire.';
      } else if (comment.errors.pattern) {
        return 'Le commentaire saisi ne doit pas être vide.';
      }
    }
    return ' ';
  }

  filters: Filter[] = [
    {
      label: 'Tous',
      selected: true,
      className: ''
    },
    {
      label: 'Procédures',
      selected: false,
      className: 'procedures',
      informationType: InformationType.PROCEDURE
    },
    {
      label: 'Commentaires',
      selected: false,
      className: '',
      informationType: InformationType.COMMENT
    }];

  documents: Document[] = [];
  addDelayModal = false;
  today = new Date();
  addDelayControl: FormControl;
  documentForm: FormGroup;
  terminatePVModalVisible = false;
  terminateSansSuitePVModalVisible = false;
  terminateDateControl: FormControl;
  confirmTerminateModal = false;

  constructor(private documentService: DocumentService,
    private formBuilder: FormBuilder,
    private procedureService: ProcedureService,
    @Inject(LOCALE_ID) public locale: string) {
    this.documentForm = this.formBuilder.group({
      comment: this.formBuilder.control(null,
        [Validators.required, Validators.pattern(/.*[^\s].*/)]),
      file: this.formBuilder.control(null),
    });
    this.addDelayControl = this.formBuilder.control(1, Validators.required);
    this.commentControl = this.formBuilder.control(null, Validators.required);
    this.terminateDateControl = this.formBuilder.control(formatDate(new Date(), 'yyyy-MM-dd', this.locale), Validators.required);
  }

  ngOnInit(): void {
    this.loadCommentsFrom();
    this.documentForm.valueChanges.subscribe(values => {
      this.document.comment = values.comment;
    });
    this.procedureService.findAllNext(this.item.id).subscribe(procedures => this.procedures = procedures);
    this.procedureService.groups().pipe(first()).subscribe(procGroupe => this.groups = procGroupe);
  }

  chooseType(documentsType): void {
    this.filters.forEach((docType) => {
      docType.selected = false;
    });
    documentsType.selected = true;
  }

  updateFile($event): void {
    this.document.file = $event.target.files[0];
  }

  deleteFile(): void {
    this.fileElement.value = null;
    this.document.file = undefined;
  }

  saveComment($event: MouseEvent): void {
    if (this.documentForm.valid) {
      this.isWaitingResponse = true;
      this.document.idDevice = this.item.id;
      this.documentService.save(this.document).subscribe(() => {
        this.documentService.findAllByDevice(this.item.id)
          .subscribe(documents => {
            this.documents = documents;
            this.deleteFile();
            this.documentForm.reset();
            this.isWaitingResponse = false;
          }, error1 => console.error(error1)
          );
      });
    }
  }

  closeModal(groupProcedureId: number[]): void {
    this.modalIsOpen = false;
    groupProcedureId.forEach(groupeId => {
      if (!this.item.lastProcGroupId.some(id => id === groupeId)) {
        this.item.lastProcGroupId.push(groupeId);
      }
    });
    this.loadCommentsFrom();
  }

  closeView($event): void {
    this.selectedForPreview = null;
  }

  newProcedure($event: MouseEvent): void {
    this.openModalFor = 'procedure';
    this.modalIsOpen = true;
  }

  onUploadOdt(upload: boolean, download: boolean, $event: MouseEvent): void {
    if (upload) {
      this.openModalFor = 'upload';
      this.modalIsOpen = true;
    }
    if (download) {
      this.documentService.getForDownload(this.selectedForPreview.id).subscribe(response => {
        const fileName = ServiceHelper.getFileNameFromResponseContentDisposition(response);
        saveAs(response.body, fileName);
      });
    }
  }

  closeProcedure($event: MouseEvent) {
    if (this.selectedForPreview) {
      this.documentService.closeProcedure(this.selectedForPreview.idDevice, this.selectedForPreview.id).subscribe(result => {
        if (result) {
          this.selectedForPreview.closeDate = new Date();
          this.loadCommentsFrom();
        }
      });
    }
  }

  private loadCommentsFrom(): void {
    this.documentService.findAllByDevice(this.item.id).subscribe(
      documents => this.documents = documents,
      error1 => console.error(error1));
  }

  public filteredDocuments(): Document[] {
    const filter: Filter[] = this.filters.filter(value => value.selected);
    let documents = this.documents.sort((a, b) => b.addedDate.getTime() - a.addedDate.getTime());
    if (filter && filter.length > 0 && filter[0].informationType) {
      documents = this.documents.filter(document => document.type === filter[0].informationType);
    }
    return documents;
  }

  public onSelectDocument(document: Document): void {
    this.selectedForPreview = document;
    this.donwloadURL = this.documentService.urlDownloadDocument(this.selectedForPreview.id);
  }

  public onEditedDoc(editedDoc: Document): void {
    this.documents.forEach(document => {
      if (document.id === editedDoc.id) {
        document = editedDoc;
        this.onSelectDocument(document);
        this.docViewer.refresh();
        return false;
      }
    });
  }

  openAddDelayModal() {
    this.addDelayControl.setValue(1);
    this.addDelayModal = true;
  }

  updateDocumentDelay(selectedForPreview: Document) {
    const subscriptions = Array.of(
      this.documentService.update(selectedForPreview.id, <Document>{ delay: this.addDelayControl.value + selectedForPreview.delay }));
    const doc = new Document();
    doc.comment = this.commentControl.value;
    doc.idDevice = this.item.id;
    subscriptions.push(this.documentService.save(doc));
    Observable.forkJoin(subscriptions).subscribe({
      next: () => {
        this.selectedForPreview = null;
        this.loadCommentsFrom();
      }
    });
  }

  terminatePV(selectedForPreview: Document) {
    this.documentService.closeProcedure(this.item.id, selectedForPreview.id, new Date(this.terminateDateControl.value), false).subscribe({
      next: () => {
        this.selectedForPreview = null;
        this.loadCommentsFrom();
        this.confirmTerminateModal = true;
      }
    });
  }
  terminateSansSuitePV(selectedForPreview: Document) {
    this.documentService.closeProcedure(this.item.id, selectedForPreview.id, new Date(this.terminateDateControl.value), true).subscribe({
      next: () => {
        this.selectedForPreview = null;
        this.loadCommentsFrom();
      }
    });
  }
}

class Filter {
  label: string;
  selected: boolean;
  className: string;
  informationType?: InformationType;
}
