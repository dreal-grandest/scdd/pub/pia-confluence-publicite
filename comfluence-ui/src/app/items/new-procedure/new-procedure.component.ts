import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Procedure, ProcedureGroupe } from 'src/app/models/device/Procedure';
import { DocumentService } from 'src/app/shared/services/document.service';
import { Document } from 'src/app/models/device/document';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/forkJoin';

@Component({
  selector: 'app-new-procedure',
  templateUrl: './new-procedure.component.html',
  styleUrls: ['../../shared/components/modal/modal.component.scss']
})
export class NewProcedureComponent {
  @Output() procedureCreated = new EventEmitter<number[]>();
  @Input() idDevice: number;
  @Input() lastProcedureGroupId: number[] = [];
  @Output() cancel = new EventEmitter();
  @Input() procedures: Procedure[] = [];
  @Input() groups: ProcedureGroupe[] = [];

  selectedProcedures: Procedure[] = [];
  isLoading = false;

  constructor(private documentService: DocumentService) {
  }

  public addTag(procedure: Procedure): void {
    const toRemove = this.selectedProcedures.filter(value => value.incompatibleProcedures
      .some(incompatible => procedure.id === incompatible.id));
    toRemove.forEach(value => this.removeTag(value));
    this.selectedProcedures.push(procedure);
    procedure.selected = true;
  }

  public removeTag(procedure: Procedure): void {
    this.selectedProcedures.splice(this.selectedProcedures.indexOf(procedure), 1);
    procedure.selected = false;
  }

  public saveProcedure(): void {
    const tasks$: Observable<Document>[] = [];
    const procedureGrpId: number[] = [];
    this.isLoading = true;
    this.selectedProcedures.forEach(procedure => {
      const document = new Document();
      if (!procedureGrpId.some(listId => listId === procedure.groupeId)) {
        procedureGrpId.push(procedure.groupeId);
      }
      document.procedure = procedure;
      document.idDevice = this.idDevice;
      document.comment = procedure.label;
      document.delay = procedure.delay;
      tasks$.push(this.documentService.save(document));
    });
    Observable.forkJoin(tasks$).subscribe((document) => {
      this.procedureCreated.emit(procedureGrpId);
      this.isLoading = false;
    });
  }

  public cancelClick(): void {
    this.cancel.emit();
  }

  public filteredProcedure(groupId: number): Procedure[] {
    if (this.procedures) {
      return this.procedures.filter(procedure => procedure.groupeId === groupId);
    } else {
      return [];
    }
  }

  public allowProcGroupe(groupe: ProcedureGroupe): boolean {
    let result = false;
    if (groupe.previousId === -1 ||
      (this.lastProcedureGroupId && this.lastProcedureGroupId.some(id => id === groupe.previousId))) {
      result = true;
    }
    return result;
  }
}
