import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Device } from '../../models/device/device';
import { Photo } from '../../models/device/properties/photo';
import { SubjectsService } from 'src/app/services/subjects.service';

@Component({
  selector: 'app-address-photos',
  templateUrl: './address-photos.component.html',
  styleUrls: ['./address-photos.component.scss']
})
export class AddressPhotosComponent implements OnInit, OnChanges {

  @Input() item: Device;
  @Input() lastUpdate: Date;
  modalIsOpen = false;

  photos: Photo[] = [];
  hideThumb = false;

  constructor(private subjectsService: SubjectsService) {
  }

  ngOnInit(): void {
    this.initPhotosSlider();
  }

  private initPhotosSlider() {
    if (this.item && this.item.photos) {
      this.photos = Array.from(this.item.photos.filter(photo => photo.active));
    } else {
      this.photos = [];
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initPhotosSlider();
  }

  nextPhoto() {
    this.hideThumb = false;
    const currentPhoto = this.photos.shift();
    this.photos.push(currentPhoto);
  }

  previousPhoto() {
    this.hideThumb = false;
    const currentPhoto = this.photos.pop();
    this.photos.unshift(currentPhoto);
  }

  previewPhoto() {
    this.modalIsOpen = true;
    this.subjectsService.openItemModaleForPhoto(true);
  }

  changeVisibility($event: MouseEvent) {
    this.hideThumb = true;
  }
}
