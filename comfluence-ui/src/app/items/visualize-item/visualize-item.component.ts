import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { ActionHelper, CachedHttpError } from 'src/app/helpers/action.helper';
import { Device } from '../../models/device/device';
import { ActionsConstantes } from 'src/app/helpers/action.constants';

@Component({
  selector: 'app-visualize-item',
  templateUrl: './visualize-item.component.html',
  styleUrls: ['./visualize-item.component.scss']
})
export class VisualizeItemComponent implements OnInit {

  @Output() returnClick = new EventEmitter();
  @Output() closeProcedure = new EventEmitter();
  removeDeviceModalOpen = false;

  @Input() item: Device;
  loading = false;

  constructor(private actionHelper: ActionHelper, private toastrService: ToastrService) {
  }

  ngOnInit(): void {

  }

  showItems($event: MouseEvent) {
    this.returnClick.emit($event);
  }
  handleRemoveClick() {
    this.loading = true;
    this.item.removed = true;
    this.actionHelper.doAction(ActionsConstantes.ACTION_DEVICE_REMOVE, this.item.id).pipe(first()).subscribe(newItem => {
      this.loading = false;
      if (newItem) {
        this.item = newItem;
        this.closeProcedure.emit(newItem);
      }
      this.toastrService.success('Le dispositif a été déposé');
    },
      (error: CachedHttpError) => {
        if (error.cached) {
          this.closeProcedure.emit(this.item);
          this.toastrService.warning('Dépose mis à jour localement, mais pas encore téléchargée sur le serveur');
        }
        this.loading = false;
      });
  }
}
