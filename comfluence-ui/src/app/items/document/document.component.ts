import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Type } from '../../models/device/device';
import { Document } from '../../models/device/document';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent {

  @Input() document: Document;
  @Input() type: Type;
  @Output() selectDocEmitter = new EventEmitter<Document>();
  @Input() selected: boolean;

  previewDocument($event) {
    this.selectDocEmitter.emit(this.document);
  }

  get outdatedDays(): number {
    const addedDate = this.document.addedDate;
    const dateMax = new Date(addedDate);
    dateMax.setDate(addedDate.getDate() + this.document.delay);
    return Math.floor((new Date().getTime() - dateMax.getTime()) / (1000 * 60 * 60 * 24));
  }
}
