import { Component, OnInit } from '@angular/core';
import { CreateUserDto, FullUser, Role } from '../models/user';
import { Department } from '../models/department';
import { UserService } from 'src/app/services/user.service';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DepartementService } from '../services/departement.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-userboard',
  templateUrl: './userboard.component.html',
  styleUrls: ['./userboard.component.scss']
})
export class UserboardComponent implements OnInit {
  editUserForm: FormGroup;
  users: FullUser[] = [];
  selected = [];
  selectedUser: FullUser = new FullUser();
  availableDepartement: Department[] = [];
  roles: Role[] = [];
  editUserModalIsOpen = false;
  editPwdModalIsOpen = false;
  deleteModalIsOpen = false;
  actionMenuIsOpen = false;
  actionDisabled = true;
  loading = false;
  editPwdForm: FormGroup;
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;

  constructor(private userService: UserService, private formBuilder: FormBuilder,
              private departementService: DepartementService, private toastrService: ToastrService,
              private roleService: RoleService) {
    this.editPwdForm = this.formBuilder.group({
      id: this.formBuilder.control(null, [Validators.required]),
      pwd: this.formBuilder.control('', [Validators.required, Validators.maxLength(255)]),
      mail: this.formBuilder.control(''),
    });
    this.editUserForm = this.formBuilder.group({
      id: this.formBuilder.control(null),
      mail: this.formBuilder.control('', [Validators.required, Validators.maxLength(512)]),
      firstName: this.formBuilder.control('', [Validators.required, Validators.maxLength(512)]),
      lastName: this.formBuilder.control('', [Validators.required, Validators.maxLength(512)]),
      departement: this.formBuilder.control('', [Validators.required]),
      pwd: this.formBuilder.control('', [this.validatePwd, Validators.maxLength(255)]),
    });
  }

  ngOnInit() {
    this.getUsers();
    this.departementService.findAll().pipe(first()).subscribe(depResult => {
      this.availableDepartement = depResult;
    });
    this.roleService.findAll().pipe(first()).subscribe(roles => {
      roles = roles.sort((r1: Role, r2: Role) => r1.id - r2.id);
      this.editUserForm.addControl('roles',
        this.formBuilder.array(roles.map(role => this.formBuilder.control(null)), [Validators.required]));
      this.roles = roles;
    });
  }

  deleteUser(user: FullUser) {
    this.loading = true;
    this.userService.delete(user.id).pipe(first()).subscribe(success => {
      const indexToDelete = this.users.findIndex(userInList => user.id === userInList.id);
      if (indexToDelete && indexToDelete >= 0) {
        this.users.splice(indexToDelete, 1);
      }
      this.users = [...this.users];
      this.deSelect();
      this.loading = false;
      this.toastrService.success('L\'utilisateur ' + user.mail + ' a correctement été supprimé.');
    }, error => {
      this.loading = false;
      this.toastrService.error('L\'utilisateur ' + user.mail + ' n\'a pas été supprimé.');
    });
  }

  compareDepartement(valueA: Department, valueB: Department): number {
    return valueA.code - valueB.code;
  }

  private getUsers() {
    this.userService.findAllFull().subscribe((currentUsers: FullUser[]) => {
      this.users = currentUsers;
    });
  }

  onSelect({selected}) {
    this.actionDisabled = false;
    this.selectedUser = selected[0];
  }

  deSelect() {
    this.selectedUser = null;
    this.actionDisabled = true;
  }

  openModifyPwd(selectedUser: FullUser) {
    this.editPwdForm.setValue({
      id: selectedUser.id,
      pwd: '',
      mail: selectedUser.mail
    });
    this.editPwdModalIsOpen = true;
  }

  openModifyUser(selectedUser: FullUser) {
    if (selectedUser) {
      const rolesControlValue = this.roles.map(role => {
        return (selectedUser.roles && selectedUser.roles.some(userRole => userRole && userRole.id === role.id)) ? role : null;
      });
      selectedUser.roles = selectedUser.roles.sort((r1: Role, r2: Role) => r1.id - r2.id);
      this.editUserForm.patchValue({
        id: selectedUser.id,
        mail: selectedUser.mail,
        firstName: selectedUser.firstName,
        lastName: selectedUser.lastName,
        departement: selectedUser.departement,
        pwd: '',
        roles: rolesControlValue
      });
    }
    this.editUserModalIsOpen = true;
  }

  updateUser(editUser: UserEditRequest) {
    if (this.editUserForm.valid) {
      const user = this.users.find(userTmp => userTmp.id === editUser.id);
      const updatedUser = new FullUser();
      if (user) {
        updatedUser.id = user.id;
      }
      updatedUser.firstName = editUser.firstName;
      updatedUser.lastName = editUser.lastName;
      updatedUser.mail = editUser.mail;
      updatedUser.departement = editUser.departement;
      updatedUser.roles = editUser.roles;
      this.loading = true;
      if (updatedUser.id) {
        // update
        this.userService.update(updatedUser.id, updatedUser).pipe(first()).subscribe(data => {
          user.departement = updatedUser.departement;
          user.firstName = updatedUser.firstName;
          user.lastName = updatedUser.lastName;
          user.mail = updatedUser.mail;
          user.roles = updatedUser.roles.filter(role => role !== null);
          this.loading = false;
          this.toastrService.success('L\'utilisateur ' + updatedUser.mail + ' a correctement été sauvegardé.');
        }, error => {
          this.loading = false;
          this.toastrService.error('Erreur lors de la sauvegarde de l\'utilisateur: ' + updatedUser.mail);
        });
      } else {
        // creation
        const createUserReq = new CreateUserDto();
        createUserReq.firstName = updatedUser.firstName;
        createUserReq.lastName = updatedUser.lastName;
        createUserReq.dep = updatedUser.departement.code;
        createUserReq.mail = updatedUser.mail;
        createUserReq.pwd = editUser.pwd;
        if (updatedUser.roles) {
          createUserReq.roles = updatedUser.roles.map(role => role ? role.id : null);
        }
        this.userService.create(createUserReq).subscribe(resultUser => {
          this.users.push(resultUser);
          this.users = [...this.users];
          this.loading = false;
          this.toastrService.success('L\'utilisateur ' + updatedUser.mail + ' a correctement été ajouté.');
        }, error => {
          this.loading = false;
          this.toastrService.error('Erreur lors de l\'ajout de l\'utilisateur: ' + updatedUser.mail);
        });
      }
      this.editUserModalIsOpen = false;
      this.editUserForm.reset();
    }
  }

  cancelUpdateUser() {
    this.editUserModalIsOpen = false;
    this.editUserForm.reset();
  }

  updatePwd(pwdEdit: PwdEditRequest) {
    if (this.editPwdForm.valid) {
      this.loading = true;
      this.userService.updatePwd(pwdEdit.id, pwdEdit.pwd).pipe(first()).subscribe(data => {
        this.toastrService.success('Le mot de passe a été correctement modifié');
        this.loading = false;
      }, error => {
        this.toastrService.error('Une erreur est survenue, le mot de passe n\'a pas été modifié');
        this.loading = false;
      });
      this.closeEditPwdModal();
    }
  }

  closeEditPwdModal() {
    this.editPwdModalIsOpen = false;
    this.editPwdForm.reset();
  }

  toggleEnable(selectedUser: FullUser) {
    this.loading = true;
    const activatedStr = selectedUser.enabled ? 'Désactivé' : 'Activé';
    this.userService.enable(selectedUser.id, !selectedUser.enabled).pipe(first()).subscribe(data => {
      this.selectedUser.enabled = !selectedUser.enabled;
      this.toastrService.success('L\' utilisateur a été ' + activatedStr);
      this.loading = false;
    }, error => {
      this.toastrService.error('L\' utilisateur n\'a pas été ' + activatedStr);
      this.loading = false;
    });
  }

  compareDepEquality(dep1: Department, dep2: Department) {
    return (dep1 && dep2 && dep1.code === dep2.code);
  }

  validatePwd(group: FormGroup): { [key: string]: any } {
    if (group.parent && group.parent.controls) {
      const id = group.parent.controls['id'];
      const pwd = group.parent.controls['pwd'];
      if (!id.value && pwd.value === '') {
        return {'validatePwd': true};
      }
    }
    return null;
  }
}

class UserEditRequest {
  id: number;
  mail: string;
  firstName: string;
  lastName: string;
  departement: Department;
  pwd: string;
  roles: Role[];
}

class PwdEditRequest {
  id: number;
  pwd: string;
  mail: string;
}
