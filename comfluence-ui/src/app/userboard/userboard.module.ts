import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { UserboardComponent } from './userboard.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DatatableModule } from '../datatable/datatable.module';


const routes: Routes = [
  {path: '', component: UserboardComponent},
];

@NgModule({
  declarations: [
    UserboardComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    DatatableModule,
  ]
})

export class UserboardModule {
}
