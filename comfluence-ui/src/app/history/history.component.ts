import { Component, OnInit } from '@angular/core';
import { ControlPreparation } from '../models/control/controlPreparation';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { ControlService } from '../services/control.service';
import { AuthenticationService } from '../services/authentication.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  filteredControls: ControlPreparation[] = [];
  historyControls: ControlPreparation[] = [];
  nextControls: ControlPreparation[] = [];

  modalIsOpen = false;
  allControllers = true;
  showingMore = false;
  cancelPlanifiedControlModalVisible = false;

  private currentUserValue: User;
  selectedControl: ControlPreparation;

  constructor(private router: Router, private controlService: ControlService,
              private authenticationService: AuthenticationService) {
    const today = new Date();
    this.currentUserValue = this.authenticationService.currentUserValue;
    this.controlService.findRecentByDepartmentCode(this.currentUserValue.departement.code)
      .pipe(first()).subscribe(controls => {
      this.nextControls = controls.filter((aControl) => this.isPlanified(aControl, today)).sort(this.sortControlDateFunction(false));
      if (this.nextControls && this.nextControls.length > 0) {
        this.selectedControl = this.nextControls[0];
      }
      const dateMillis = Date.parse(new Date(today).toLocaleDateString('en'));
      this.historyControls = controls.filter(aControl => !this.nextControls.includes(aControl)
        && dateMillis >= Date.parse(new Date(aControl.controlDate.getTime()).toLocaleDateString('en')))
        .sort(this.sortControlDateFunction());
      this.filterHistory();
    });
  }

  addOrUpdateNextControls(savedControl: ControlPreparation) {
    const controlPreparations = this.nextControls.filter(aControl => aControl.id !== savedControl.id);
    controlPreparations.push(savedControl);
    this.nextControls = controlPreparations.sort(this.sortControlDateFunction(false));
    this.selectedControl = savedControl;
  }

  sortControlDateFunction(desc: boolean = true): (controlA: ControlPreparation, controlB: ControlPreparation) => number {
    return (controlA: ControlPreparation, controlB: ControlPreparation) =>
      (desc ? 1 : -1) * (controlB.controlDate.getTime() - controlA.controlDate.getTime());
  }

  isPlanified(aControl: ControlPreparation, date: Date): boolean {
    const dateMillis = Date.parse(new Date(date).toLocaleDateString('en'));
    const controlDateMillis = Date.parse(new Date(aControl.controlDate).toLocaleDateString('en'));
    return aControl.controllerMail === this.currentUserValue.mail
      && (dateMillis < controlDateMillis
        || (dateMillis === controlDateMillis && aControl.statistics.controlledDevices === null));
  }

  ngOnInit() {
  }

  closeModal() {
    this.modalIsOpen = false;
  }

  showModal(control?: ControlPreparation) {
    this.selectedControl = control;
    this.modalIsOpen = true;
  }

  showMoreHistories() {
    this.showingMore = true;
    this.filterHistory();
  }

  get showMoreButtonVisible() {
    return this.filteredControls && this.filteredControls.length > 3 && !this.showingMore;
  }

  toggleSeeFromAllControllers() {
    this.filterHistory();
  }

  private filterHistory() {
    let filteredControls = [];
    if (this.allControllers) {
      filteredControls = this.historyControls;
    } else {
      filteredControls = this.historyControls.filter(control => control.controllerMail === this.currentUserValue.mail);
    }
    this.filteredControls = filteredControls;
  }

  get visibleControls() {
    if (!this.showingMore) {
      const maxIndex = Math.min(3, this.filteredControls.length);
      return this.filteredControls.slice(0, maxIndex);
    }
    return this.filteredControls;
  }

  cancelControl(cancelControl: ControlPreparation) {
    this.controlService.delete(cancelControl.id).pipe(first()).subscribe(() => {
      this.nextControls = this.nextControls.filter(control => cancelControl.id !== control.id);
      if (this.nextControls.length > 0) {
        this.selectedControl = this.nextControls[0];
      }
    });
  }

  showWarningPopin(idControl: number) {
    this.selectedControl = this.nextControls.find(control => control.id === idControl);
    this.cancelPlanifiedControlModalVisible = true;
  }

  chooseControl(control: ControlPreparation) {
    this.router.navigate(['/items'], { queryParams: { idControl: control.id } });
  }
}
