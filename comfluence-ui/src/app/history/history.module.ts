import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HistoryComponent } from './history.component';
import { SharedModule } from '../shared/shared.module';
import { ControlComponent } from './components/control/control.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {path: '', component: HistoryComponent},
];

@NgModule({
  declarations: [
    HistoryComponent,
    ControlComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    CommonModule,
    FormsModule
  ]
})
export class HistoryModule {
}
