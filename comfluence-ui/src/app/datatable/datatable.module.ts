import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

/**
 * Module to override some configuration globally in the NgxDatatableModule
*/

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxDatatableModule.forRoot({
      messages: {
        emptyMessage: 'Aucun élément à afficher',
        totalMessage: 'éléments',
        selectedMessage: '' // empty string seems to hide the number
      }
    })
  ],
  exports: [
    NgxDatatableModule
  ]
})
export class DatatableModule { }
