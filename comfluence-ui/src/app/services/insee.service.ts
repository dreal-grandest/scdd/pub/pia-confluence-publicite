import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { InseeGeo } from '../models/inseegeo';
import { GeoApi } from '../models/geoapi';
import communeData from '../../assets/data/unite_urbaine_2010_until_01_2019.json';

@Injectable({
  providedIn: 'root'
})
export class InseeService {
  private static readonly SEARCH_ADDRESS_GPS_API: String = environment.searchAddressGpsApiRoot;
  private static readonly SEARCH_COMMUNE_API: String = environment.searchCommuneApiRoot;

  private readonly communes: Commune[] = communeData;

  constructor(private httpClient: HttpClient) {
  }

  searchAddressByCoordinate(lat: number, lon: number): Observable<InseeGeo.SearchResult> {
    const params = new HttpParams().set('lon', lon.toString())
      .set('lat', lat.toString());
    return this.httpClient.get<InseeGeo.SearchResult>(`${InseeService.SEARCH_ADDRESS_GPS_API}/`, { params: params });
  }

  searchCommuneByCoordinate(lat: number, lon: number): Observable<GeoApi.Commune[]> {
    const params = new HttpParams().set('lon', lon.toString())
      .set('lat', lat.toString())
      .set('fields', 'code,nom,codesPostaux');
    return this.httpClient.get<GeoApi.Commune[]>(`${InseeService.SEARCH_COMMUNE_API}/`, { params: params });
  }

  searchCommuneByCodeInsee(codeInsee: string): Observable<GeoApi.Commune> {
    const params = new HttpParams()
      .set('fields', 'code,nom,codesPostaux,population');
    return this.httpClient.get<GeoApi.Commune>(`${InseeService.SEARCH_COMMUNE_API}/${codeInsee}`, { params: params });
  }

  searchUniteUrbaineLabel(codeCommune: string): string {
    const result = this.communes.filter(commune => commune.code === codeCommune);
    if (result.length === 0) {
      return '';
    } else {
      return result.shift().uniteUrbaine.label;
    }
  }
}

class Commune {
  code: string;
  label: string;
  uniteUrbaine: {
    code: string;
    label: string;
  };
}
