import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Procedure, ProcedureGroupe } from '../models/device/Procedure';

@Injectable({
  providedIn: 'root'
})
export class ProcedureService {
  static readonly BASE_URL: String = environment.baseUrlApi;

  constructor(private httpClient: HttpClient) {
  }

  findAllNext(idDevice: number): Observable<Procedure[]> {
    return this.httpClient.get<Procedure[]>(`${ProcedureService.BASE_URL}/procedure/byDevice/${idDevice}/next`);
  }

  groups(): Observable<ProcedureGroupe[]> {
    return this.httpClient.get<ProcedureGroupe[]>(`${ProcedureService.BASE_URL}/procedure/groups`);
  }
}
