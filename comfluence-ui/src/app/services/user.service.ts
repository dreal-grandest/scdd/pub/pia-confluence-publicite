import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { User, FullUser, CreateUserDto } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  static readonly BASE_URL: String = environment.baseUrlApi;

  constructor(private httpClient: HttpClient) {
  }

  findAll(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${UserService.BASE_URL}/user`);
  }

  findAllFull(): Observable<FullUser[]> {
    let params = new HttpParams();
    params = params.set('showDisabled', 'true');
    return this.httpClient.get<FullUser[]>(`${UserService.BASE_URL}/user/full`, { params: params });
  }

  get(id: number): Observable<User> {
    return this.httpClient.get<User>(`${UserService.BASE_URL}/user/${id}`);
  }

  findAllByDepartment(departmentCode: number): Observable<User[]> {
    return this.httpClient.get<User[]>(`${UserService.BASE_URL}/user/byDepartmentCode/${departmentCode}`);
  }

  findByMail(mail: string): Observable<User> {
    return this.httpClient.get<User>(`${UserService.BASE_URL}/user/byMail/${mail}`);
  }
  create(user: CreateUserDto): Observable<FullUser> {
    const formData = new FormData();
    return this.httpClient.post<FullUser>(`${UserService.BASE_URL}/user`, user);
  }

  update(id: number, user: FullUser): Observable<boolean> {
    return this.httpClient.put<boolean>(`${UserService.BASE_URL}/user/${id}`, user);
  }

  enable(id: number, enable: boolean): Observable<boolean> {
    const params = new FormData();
    params.append('enable', enable.toString());
    return this.httpClient.put<boolean>(`${UserService.BASE_URL}/user/${id}/enable`, params);
  }

  updatePwd(id: number, pwd: string): Observable<boolean> {
    const formData = new FormData();
    formData.append('pwd', pwd);
    return this.httpClient.put<boolean>(`${UserService.BASE_URL}/user/${id}/pwd`, formData);
  }

  delete(id: number): Observable<any> {
    return this.httpClient.delete(`${UserService.BASE_URL}/user/${id}`);
  }
}
