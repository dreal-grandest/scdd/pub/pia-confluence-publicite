import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EnvironmentalContraintService {
  private environmentalContraintsSubject: BehaviorSubject<EnvironmentalContraint[]>;
  public environmentalContraints: Observable<EnvironmentalContraint[]>;

  constructor() {
    this.environmentalContraintsSubject = new BehaviorSubject<EnvironmentalContraint[]>([]);
    this.environmentalContraints = this.environmentalContraintsSubject.asObservable();
  }

  public get environmentalContraintsValue(): EnvironmentalContraint[] {
    return this.environmentalContraintsSubject.value;
  }

  public updateEnvironmentalContraints(environmentalContraints: EnvironmentalContraint[]): void {
    this.environmentalContraintsSubject.next(environmentalContraints);
  }
}

export class EnvironmentalContraint {
  id: string;
  label: string;
}
