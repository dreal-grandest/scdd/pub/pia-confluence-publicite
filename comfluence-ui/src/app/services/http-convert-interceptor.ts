import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Device } from '../models/device/device';

@Injectable({
  providedIn: 'root'
})
export class HttpConvertInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((val: HttpEvent<any>) => {
        if (val instanceof HttpResponse) {
          this.fixDate(val.body);
        }

        return val;
      })
    );
  }

  fixDate(value: any): boolean {
    let converted = false;
    if (value instanceof Array) {
      const notManaged = value.some(val => {
        return !this.fixDate(val);
      });
      converted = !notManaged;
    } else if (this.isDevice(value)) {
      this.fixDeviceDate(value);
      converted = true;
    }
    return converted;
  }

  fixDeviceDate(value: Device) {
    value.operationDate = this.convertStrToDate(value.operationDate);
  }

  convertStrToDate(value: any): Date | undefined {
    let result;
    if (typeof value === 'string') {
      result = new Date(value);
    } else {
      result = value;
    }
    return result;
  }

  isDevice(value: any): boolean {
    return value && (value instanceof Device || (typeof value === 'object' && value.hasOwnProperty('reference')));
  }
}
