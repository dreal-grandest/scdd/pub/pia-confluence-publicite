import {Injectable} from '@angular/core';
import {LayerService} from './layer.service';
import {DeviceService} from './device.service';
import {ProcedureService} from './procedure.service';
import {BehaviorSubject, from, Observable} from 'rxjs';
import {PhotoService} from './photo.service';
import {FilterService} from './filter.service';
import {Device} from '../models/device/device';
import {finalize, first, mergeMap, tap} from 'rxjs/operators';
import {Box2154, Box4326} from '../models/box';
import * as L from 'leaflet';
import {LeafletEvent} from 'leaflet';
import {AdvertiserService} from '../shared/services/advertiser.service';
import area from '@turf/area';
import bboxPolygon from '@turf/bbox-polygon';
import {ToastrService} from 'ngx-toastr';
import {OsmComponent} from "../osm/osm.component";
import {HttpClient, HttpHeaders} from "@angular/common/http";

export enum PrefetchStatus {
  STARTED, DONE, ERROR
}

export interface PrefetchState {
  init: boolean;
  deviceStatus: PrefetchStatus;
  layersState?: {
    layersStatus: PrefetchStatus,
    totalLayers: number,
    layersDone: number
  };
  filterStatus: PrefetchStatus;
  nbDeviceToUpdate: number;
  nbDeviceUpdated: number;
  advertiserStatus: PrefetchStatus;
}
interface ProgressLayerEvent {
    bbox: any,
    minZoom: number,
    maxZoom: number,
    queueLength: number,
    remainingLength: number,
}

@Injectable({
  providedIn: 'root'
})
export class PrefetchService {
  private stateSubject: BehaviorSubject<PrefetchState>;
  public state: Observable<PrefetchState>;

  constructor(
    private layerService: LayerService,
    private deviceService: DeviceService,
    private procedureService: ProcedureService,
    private photoService: PhotoService,
    private filterSerivce: FilterService,
    private advertiserService: AdvertiserService,
    private toastrService: ToastrService,
    private httpClient: HttpClient) {
    this.stateSubject = new BehaviorSubject<PrefetchState>({
      init: true,
      deviceStatus: PrefetchStatus.DONE,
      layersState: {
        layersStatus: PrefetchStatus.DONE,
        totalLayers: 0,
        layersDone: 0
      },
      filterStatus: PrefetchStatus.DONE,
      nbDeviceToUpdate: 0,
      nbDeviceUpdated: 0,
      advertiserStatus: PrefetchStatus.DONE,
    });
    this.state = this.stateSubject.asObservable();
  }

  prefetchRessources(box: Box4326, departementId: number) {
    this.stateSubject.next({
      init: false,
      deviceStatus: PrefetchStatus.STARTED,
      filterStatus: PrefetchStatus.STARTED,
      layersState: {
        layersStatus: PrefetchStatus.STARTED,
        totalLayers: 0,
        layersDone: 0
      },
      nbDeviceToUpdate: 0,
      nbDeviceUpdated: 0,
      advertiserStatus: PrefetchStatus.STARTED,
    });
    this.prefetchDevice(box);
    this.prefetchStatistics(box);
    this.prefetechAdvertiser(departementId);
    this.prefetechFilter();
    // this.prefetchLayers(box);
  }

  private prefetchDevice(box: Box4326) {
    // récupération de tous les dispositifs
    const subscription = this.deviceService.findAllFromBox(box, [], true).pipe(first()).subscribe(data => {
            this.updateNbDeviceToUpdate(data.length);
            // Récupération du détail d'un dispositif
            data.forEach(device => {
                // récupération des photos
                this.prefetchPhotos(device);
                const subscription1 = this.deviceService.get(device.id).subscribe(
                    deviceResp => {
                        this.incUpdatedDevice();
                        subscription1.unsubscribe();
                    }, error => {
                        this.incUpdatedDevice();
                        subscription1.unsubscribe();
                    }
                );
            });
            
        },
        error => {
            this.updateDeviceStatus(PrefetchStatus.ERROR);
        },
        () => {
            if (this.stateSubject.value.deviceStatus !== PrefetchStatus.ERROR) {
                this.updateDeviceStatus(PrefetchStatus.DONE);
            }
            subscription.unsubscribe();
        });
  }

  private prefetchPhotos(device: Device) {
    const subscription = this.photoService.getPhotosBy(device.id).subscribe(
      photosData => {
        photosData.forEach(photo => {
          const subscription1 = this.photoService.getImageThumbBlob(photo.id).subscribe(
            data => {
              subscription1.unsubscribe();
            }, error => {
              subscription1.unsubscribe();
            }
          );
          const subscription2 = this.photoService.getImageBlob(photo.id).subscribe(
            data => {
              subscription2.unsubscribe();
            }, error => {
              subscription2.unsubscribe();
            }
          );
        });
        subscription.unsubscribe();
      }, photosError => {
        subscription.unsubscribe();
      }
    );
  }

  private prefetchLayers(box: Box4326) {
    const boxPolygon = bboxPolygon([box.minLat, box.minLong, box.maxLat, box.maxLong]);
    const boxArea = area(boxPolygon);
    const maxArea = 16000000;
    if (boxArea > maxArea) {
      this.toastrService.warning('Les couches géographiques n\'ont pas été enregistrées car la zone à couvrir est trop grande');
      this.updateDefaultLayerState(PrefetchStatus.DONE);
    } else {
      this.layerService.findAll(Box2154.fromBox4326(box)).pipe(first()).subscribe(layers => {
        const crs = L.CRS.EPSG3857; // default from leaflet
        const northEast: L.LatLng = L.latLng(box.maxLat, box.maxLong);
        const southWest: L.LatLng = L.latLng(box.minLat, box.minLong);

        const tileLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', <any>{
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
          zIndex: 0,
          useCache: true,
            queueLength: 1000
        });

        (<any>tileLayer).seed = this.getSeed(crs, false);
          (<any>tileLayer)._seedOneTile = this.getOneTile();
        (<any>tileLayer)._crs = crs;
        (<any>tileLayer)._tileCoordsToNwSe = function (coords) {
          const tileSize = this.getTileSize();
          const nwPoint = coords.scaleBy(tileSize);
          const sePoint = nwPoint.add(tileSize);
          const nw = crs.pointToLatLng(nwPoint, coords.z);
          const se = crs.pointToLatLng(sePoint, coords.z);
          return [nw, se];
        };
        tileLayer.on('seedstart', (event: LeafletEvent) => this.addLayerToLayersState(event), this);
        tileLayer.on('seedend', (event: LeafletEvent) => this.addLayerDoneToLayersState(event), this);
        (<any>tileLayer).seed(L.latLngBounds(northEast, southWest), 13, 18);
        
        layers.forEach(layerDef=> {
            let layerUrl;
            if(layerDef.needProxy) {
                layerUrl = `${OsmComponent.BASE_URL}/layer/proxy/${layerDef.id}`;
            } else {
                layerUrl = layerDef.url;
            }
          const wmsLayer = L.tileLayer.wms(layerUrl, <any>{
            layers: layerDef.parameters.typeName,
            format: layerDef.parameters.outputFormat,
            transparent: true,
            opacity: 0.2,
            useCache: true,
              queueLength: 1000
          });

          (<any>wmsLayer).seed = this.getSeed(crs);
            (<any>wmsLayer)._seedOneTile = this.getOneTile();
          (<any>wmsLayer)._crs = crs;

          const projectionKey = parseFloat(wmsLayer.wmsParams.version) >= 1.3 ? 'crs' : 'srs';
          wmsLayer.wmsParams[projectionKey] = crs.code;
          (<any>wmsLayer)._tileCoordsToNwSe = function (coords) {
            const tileSize = this.getTileSize();
            const nwPoint = coords.scaleBy(tileSize);
            const sePoint = nwPoint.add(tileSize);
            const nw = crs.pointToLatLng(nwPoint, coords.z);
            const se = crs.pointToLatLng(sePoint, coords.z);
            return [nw, se];
          };
          wmsLayer.on('seedstart', (event: LeafletEvent) => this.addLayerToLayersState(event), this);
          // wmsLayer.on('seedend', (event: LeafletEvent) => this.addLayerDoneToLayersState(event), this);
            wmsLayer.on('seedprogress', (event: LeafletEvent) =>
                    this.addLayerDoneToLayersState(event)
            , this);
          (<any>wmsLayer).seed(L.latLngBounds(northEast, southWest), 13, 18);
        });
      });
    }
  }

  private getSeed(crs: L.CRS, wmsTile: boolean = true) {
    return function (bbox, minZoom, maxZoom) {
      if (!this.options.useCache) {
        return;
      }
      if (minZoom > maxZoom) {
        return;
      }
      const queue = [];
      for (let z = minZoom; z <= maxZoom; z++) {
        // Geo bbox to pixel bbox (as per given zoom level)...
        const northEastPoint: L.Point = crs.latLngToPoint(bbox.getNorthEast(), z);
        const southWestPoint: L.Point = crs.latLngToPoint(bbox.getSouthWest(), z);

        // Then to tile coords bounds, as per GridLayer
        const tileBounds = this._pxBoundsToTileRange(
          L.bounds([northEastPoint, southWestPoint])
        );

        for (let j = tileBounds.min.y; j <= tileBounds.max.y; j++) {
          for (let i = tileBounds.min.x; i <= tileBounds.max.x; i++) {
            const point = new L.Point(i, j);
            (<L.Coords>point).z = z;
            if (wmsTile) {
              queue.push(this.getTileUrl(point));
            } else {
              queue.push(this._getTileUrl(point));
            }
          }
        }
      }

      const seedData = {
        bbox: bbox,
        minZoom: minZoom,
        maxZoom: maxZoom,
        queueLength: queue.length,
      };
      this.fire('seedstart', seedData);
      const tile = this._createTile();
      tile._layer = this;
      this._seedOneTile(tile, queue, seedData);
      return this;
    };
  }
  
  getOneTile() {
      const tmpHttpClient = this.httpClient;
      return function (tile, remaining, seedData) {
          if (!remaining.length) {
              this.fire("seedend", seedData);
              return;
          }
          const progressEvent: ProgressLayerEvent = {
              bbox: seedData.bbox,
              minZoom: seedData.minZoom,
              maxZoom: seedData.maxZoom,
              queueLength: seedData.queueLength,
              remainingLength: remaining.length,
          };
          this.fire("seedprogress", progressEvent);
            from(remaining)
                .pipe(
                    mergeMap(url =>
                        new Observable<true>(observer => {
                            this._db.get(
                                url,
                                function(err, data) {
                                    if (!data) {
                                        /// FIXME: Do something on tile error!!
                                        tmpHttpClient.get(url as string, {responseType: 'blob', headers: new HttpHeaders({timeout: '2000'})}).subscribe(result => {
                                            tile.crossOrigin = "Anonymous";
                                            tile.src = URL.createObjectURL(result);
                                            this._saveTile(tile, url, null); //(ev)
                                            observer.next(true);
                                            observer.complete();
                                        }, error => {
                                            observer.next(true);
                                            observer.complete();
                                        })
                                        // tile.onload = function (ev) {
                                        //     observer.next(true);
                                        //     observer.complete();
                                        //     this._saveTile(tile, url, null); //(ev)
                                        //     // this._seedOneTile(tile, [url], seedData);
                                        // }.bind(this);
                                        //
                                        // tile.crossOrigin = "Anonymous";
                                        // tile.src = url;
                                    } else {
                                        observer.next(true);
                                        observer.complete();
                                    }
                                    
                                }.bind(this)
                            );
                            
                    }),
                        3
                ),
                    tap(() => {
                        progressEvent.remainingLength--;
                        this.fire("seedprogress", progressEvent);
                    })
                ).subscribe(() => {
                    // nothing to do
            }, error => {
                // nothing to do
            }, () => {
                // if(progressEvent.remainingLength <= 0) {
                    this.fire("seedend", seedData);
                // }
            });
    
          
      };
  }

  private prefetchStatistics(box: Box4326) {

  }

  private prefetechFilter() {
    const subscription = this.filterSerivce.findAll().subscribe(
      data => {
        this.updateFilterStatus(PrefetchStatus.DONE);
        if (subscription) {
          subscription.unsubscribe();
        }
      }, error => {
        this.updateFilterStatus(PrefetchStatus.ERROR);
        subscription.unsubscribe();
      }
    );
  }

  private prefetechAdvertiser(departementId: number) {
    this.advertiserService.findAllByDepartement(departementId).pipe(first()).subscribe(data => {
      this.updateAdvertiserStatus(PrefetchStatus.DONE);
    }, error => {
      this.updateAdvertiserStatus(PrefetchStatus.ERROR);
    });
  }

  private updateDeviceStatus(status: PrefetchStatus) {
    console.log('Changement du statut Device: ' + status);
    const state = this.stateSubject.value;
    state.deviceStatus = status;
    this.stateSubject.next(state);
  }

  private updateFilterStatus(status: PrefetchStatus) {
    console.log('Changement du statut des filtres: ' + status);
    const state = this.stateSubject.value;
    state.filterStatus = status;
    this.stateSubject.next(state);
  }

  private updateNbDeviceToUpdate(nbDeviceToUpdate: number) {
    console.log('Nombre de dispositif à mettre à jour: ' + nbDeviceToUpdate);
    const state = this.stateSubject.value;
    state.nbDeviceToUpdate = nbDeviceToUpdate;
    this.stateSubject.next(state);
  }

  private incUpdatedDevice() {
    const state = this.stateSubject.value;
    state.nbDeviceUpdated++;
    console.log('Nombre de dispositif mis à jour: ' + state.nbDeviceUpdated);
    this.stateSubject.next(state);
  }

  private updateAdvertiserStatus(status: PrefetchStatus) {
    console.log('Changement du statut des annonceurs: ' + status);
    const state = this.stateSubject.value;
    state.advertiserStatus = status;
    this.stateSubject.next(state);
  }

  private addLayerToLayersState(event: any) {
    const state = this.stateSubject.value;
    state.layersState = {
      layersStatus: PrefetchStatus.STARTED,
      totalLayers: state.layersState.totalLayers + event.queueLength,
      layersDone: state.layersState.layersDone
    };
    console.log('Changement du statut Layer: ', state.layersState);
    this.stateSubject.next(state);
  }

  private addLayerDoneToLayersState(event: any) {
    const state = this.stateSubject.value;
    let status = PrefetchStatus.STARTED;
    const layersDone = state.layersState.layersDone + 1
      if (state.layersState.layersDone === state.layersState.totalLayers) {
          status = PrefetchStatus.DONE;
      }
    state.layersState = {
      layersStatus: status,
      totalLayers: state.layersState.totalLayers,
      layersDone: layersDone
    };
    console.log('Changement du statut Layer: ', state.layersState);
    this.stateSubject.next(state);
  }

  private updateDefaultLayerState(status: PrefetchStatus) {
    const state = this.stateSubject.value;
    state.layersState = {
      layersStatus: status,
      totalLayers: 0,
      layersDone: 0
    };
    this.stateSubject.next(state);
  }
}
