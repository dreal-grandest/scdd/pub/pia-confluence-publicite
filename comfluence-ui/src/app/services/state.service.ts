import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { MergeRequestHelper } from '../helpers/request/mergerequest.helper';
import { Box4326 } from '../models/box';
import { Device } from '../models/device/device';

export interface IBoxState {
    preloadBox: Box4326;
    lastBox: Box4326;
    updatePreloadBox(box: Box4326);
    updateLastBox(box: Box4326);
}

export interface IDevicesState {
    lastDevices: Device[];
    updatelastDevices(deviceList: Device[]);
}

@Injectable({
    providedIn: 'root'
})
export class StateService implements IBoxState, IDevicesState {
    public unsyncValueObs: Observable<number>;
    private preloadBoxSubject: BehaviorSubject<Box4326>;
    private lastBoxSubject: BehaviorSubject<Box4326>;
    private lastDeviceList: BehaviorSubject<Device[]>;
    private unsyncDeviceCountSubject: BehaviorSubject<number>;

    constructor(private mergeRequestHelper: MergeRequestHelper) {
        this.preloadBoxSubject = new BehaviorSubject<Box4326>(new Box4326());
        this.lastBoxSubject = new BehaviorSubject<Box4326>(new Box4326());
        this.lastDeviceList = new BehaviorSubject<Device[]>([]);
        this.unsyncDeviceCountSubject = new BehaviorSubject<number>(0);
        this.unsyncValueObs = this.unsyncDeviceCountSubject.asObservable();
    }

    public updatePreloadBox(box: Box4326) {
        this.preloadBoxSubject.next(box);
    }
    public updateLastBox(box: Box4326) {
        this.lastBoxSubject.next(box);
    }

    public addOrUpdateDevice(device: Device, unscyncId: number) {
        let tmpDeviceList = this.lastDeviceList.value;
        const index = tmpDeviceList.findIndex(deviceItem => {
            if (device.id && deviceItem.id === device.id) {
                return true;
            } else if (deviceItem.unSyncId === unscyncId) {
                return true;
            } else {
                return false;
            }
        });
        if (index < 0) {
            device.unSyncId = unscyncId;
            tmpDeviceList.push(device);
        } else {
            const newList = tmpDeviceList.splice(0, index);
            newList.push(device);
            tmpDeviceList = newList.concat(tmpDeviceList.splice(1, tmpDeviceList.length + 1));
        }
        this.lastDeviceList.next(tmpDeviceList);
    }

    public findDevice(deviceId: number): Device {
        return this.lastDeviceList.value.find(device => device.id === deviceId);
    }
    public get preloadBox(): Box4326 {
        return this.preloadBoxSubject.value;
    }
    public get lastBox(): Box4326 {
        return this.lastBoxSubject.value;
    }

    public get lastDevices(): Device[] {
        return this.lastDeviceList.value;
    }

    public updatelastDevices(deviceList: Device[]) {
        this.mergeRequestHelper.mergeWithSavedDevices(deviceList).then(mergedDeviceList => {
            let nbUnsync = 0;
            mergedDeviceList.forEach(device => {
                if (!device.sync) {
                    nbUnsync++;
                }
            });
            this.unsyncDeviceCountSubject.next(nbUnsync);
            this.lastDeviceList.next(deviceList);
        });
    }

    public setUnscynCount(unsyncCount: number) {
        this.unsyncDeviceCountSubject.next(unsyncCount);
    }
    public updateUnsyncValue(inc: boolean) {
        let unsyncValue = this.unsyncDeviceCountSubject.value;
        if (inc) {
            unsyncValue++;
        } else if (unsyncValue > 0) {
            unsyncValue--;
        }
        this.unsyncDeviceCountSubject.next(unsyncValue);
    }
}
