import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {
  private openItemPhotoSubject = new Subject<boolean>();
  private containerFilterWidthSubject = new Subject<any>();
  private openFilterWidthSubject = new Subject<boolean>();

  constructor() { }

  // Filters subject
  sendContainerFilterWidth(length: string): void {
    this.containerFilterWidthSubject.next({ width: length });
  }
  getContainerFilterWidth(): Observable<any> {
    return this.containerFilterWidthSubject.asObservable();
  }
  openFilterModaleFromHiddenList(): void {
    this.openFilterWidthSubject.next(true);
  }
  getFilterModaleFromHiddenList(): Observable<boolean> {
    return this.openFilterWidthSubject.asObservable();
  }

  // Item's photos subject
  openItemModaleForPhoto(isOpen: boolean): void {
    this.openItemPhotoSubject.next(isOpen);
  }
  getItemModalForPhoto(): Observable<boolean> {
    return this.openItemPhotoSubject.asObservable();
  }

}
