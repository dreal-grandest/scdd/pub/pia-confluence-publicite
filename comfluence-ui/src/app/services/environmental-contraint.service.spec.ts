import { TestBed } from '@angular/core/testing';

import { EnvironmentalContraintService } from './environmental-contraint.service';

describe('EnvironmentalContraintService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnvironmentalContraintService = TestBed.get(EnvironmentalContraintService);
    expect(service).toBeTruthy();
  });
});
