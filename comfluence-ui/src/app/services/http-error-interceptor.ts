import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/retry'; // don't forget the imports
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
    constructor(
        public toastrService: ToastrService,
        private authenticationService: AuthenticationService,
        private router: Router) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
            .catch((err: HttpErrorResponse) => {
                if (err.status === 401) {
                    this.authenticationService.logout();
                    this.router.navigate(['/login']);
                } else if (err.status !== 404 && err.status !== 400) {
                    if (err.error instanceof Error) {

                        // this.toastrService.error('Erreur de communication avec le serveur', '');
                        // A client-side or network error occurred. Handle it accordingly.
                        // console.error('An error occurred:', err.error.message);
                    } else {
                        // this.toastrService.error('Erreur de communication avec le serveur', '');
                        // The backend returned an unsuccessful response code.
                        // The response body may contain clues as to what went wrong,
                        // console.error('Backend returned code ' + err.status + ', body was: ' + err.error);
                    }
                }

                // ...optionally return a default fallback value so app can continue (pick one)
                // which could be a default value (which has to be a HttpResponse here)
                // return Observable.of(new HttpResponse({body: [{name: "Default value..."}]}));
                // or simply an empty observable
                return next.handle(request);
            });
    }
}
