import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Filter } from '../models/device/Filter';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  static readonly BASE_URL: String = environment.baseUrlApi;
  private filtersSubject: BehaviorSubject<Filter[]>;
  public filters: Observable<Filter[]>;

  constructor(private httpClient: HttpClient) {
    this.filtersSubject = new BehaviorSubject<Filter[]>([]);
    this.filters = this.filtersSubject.asObservable();
  }

  public get filtersValue(): Filter[] {
    return this.filtersSubject.value;
  }


  findAll(): Observable<Filter[]> {
    if (this.filtersValue.length === 0) {
      const observable = this.httpClient.get<Filter[]>(`${FilterService.BASE_URL}/dispositif/filters`);
      observable.subscribe(filters => {
        this.filtersSubject.next(filters);
      });
      return observable;
    }
    return this.filters;
  }

  findByIds(ids: number[]): Observable<Filter[]> {
    return this.findAll().pipe(map(filters => {
      return filters.filter(el => ids.includes(el.id));
    }));
  }
}
