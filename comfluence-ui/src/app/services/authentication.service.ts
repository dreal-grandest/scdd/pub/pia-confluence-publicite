import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Box2154, Box4326 } from '../models/box';
import proj4 from 'proj4';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  static readonly BASE_URL: String = environment.baseUrlApi;
  static readonly WEB_CLIENT = environment.clientId;
  static readonly WEB_SECRET = environment.clientKey;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private httpClient: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public get currentUserBox2154(): Box2154 {
    return this.currentUserValue.departement.box;
  }

  public get currentUserBox4326(): Box4326 {
    const resultBox = new Box4326();
    const userBox: Box2154 = this.currentUserValue.departement.box;
    const southWest = proj4('EPSG:2154', 'EPSG:4326', [userBox.minLong, userBox.minLat]);
    resultBox.minLong = southWest[0];
    resultBox.minLat = southWest[1];
    const northEast = proj4('EPSG:2154', 'EPSG:4326', [userBox.maxLong, userBox.maxLat]);
    resultBox.maxLong = northEast[0];
    resultBox.maxLat = northEast[1];
    return resultBox;
  }

  login(username: string, password: string): Observable<User> {
    const httpOptions = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        'Authorization': 'Basic ' + btoa(`${AuthenticationService.WEB_CLIENT}:${AuthenticationService.WEB_SECRET}`)
      }
    };
    const body = new URLSearchParams();
    body.set('username', username);
    body.set('password', password);
    body.set('grant_type', 'password');
    return this.httpClient.post<JwtToken>(`${AuthenticationService.BASE_URL}/oauth/token`,
      body.toString(), httpOptions)
      .pipe(map(jwtToken => {
        // login successful if there's a jwt token in the response
        const user = jwtToken.user;
        if (user) {
          user.token = jwtToken.access_token;
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
        }

        return <User>user;
      }));
  }

  /**
   * remove user from local storage to log user out
   */
  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  get authenticated() {
    return this.currentUserValue && !!this.currentUserValue.token;
  }

  hasRole(roleNameList: string[]): boolean {
    if (!this.currentUserValue) {
      return false;
    }
    const availablesRole = roleNameList.filter(roleName => {
      let result = false;
      if (this.currentUserValue.roles && this.currentUserValue.roles.some(role => role.name === roleName)) {
        result = true;
      }

      return result;
    });
    return roleNameList.length === availablesRole.length;
  }
}

class JwtToken {
  access_token: string;
  token_type: string;
  refresh_token: string;
  scope: string;
  user: User;
  jti: string;
}
