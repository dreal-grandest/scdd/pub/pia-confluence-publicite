import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Photo } from '../models/device/properties/photo';
import { environment } from '../../environments/environment';
import { ServiceHelper } from '../helpers/service-helper';


@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  static readonly BASE_URL: String = environment.baseUrlApi;

  public getImageBlob(id: number): Observable<Blob> {
    return this.httpClient.get(`${PhotoService.BASE_URL}/photo/download/${id}`, { responseType: 'blob' });
  }

  public getImageThumbBlob(id: number): Observable<Blob> {
    return this.httpClient.get(`${PhotoService.BASE_URL}/photo/download/thumb/${id}`, { responseType: 'blob' });
  }

  constructor(private httpClient: HttpClient) {
  }

  public postPhoto(photo: Photo): Observable<Photo> {
    const form: FormData = new FormData();
    form.append('file-content', ServiceHelper.dataURItoBlob(photo.imageFile));
    form.append('idDispositif', photo.idDispositif.toString());
    form.append('type', photo.photoType);
    form.append('active', photo.active.toString());
    return this.httpClient.post<Photo>(`${PhotoService.BASE_URL}/photo/upload`, form);
  }

  public getPhotosBy(idDispositif: number): Observable<Photo[]> {
    return this.httpClient.get<Photo[]>(`${PhotoService.BASE_URL}/photo/file/byDispositif/${idDispositif}`);
  }

  getPhoto(id: number): Observable<File> {
    const headers: HttpHeaders = new HttpHeaders({
      'Content-Type': 'image/png'
    });
    const httpOptions = { headers: headers };
    return this.httpClient.get<File>(`${PhotoService.BASE_URL}/photo/download/${id}`, httpOptions);
  }

  deletePhoto(id: number): Observable<any> {
    return this.httpClient.delete<Photo>(`${PhotoService.BASE_URL}/photo/${id}`);
  }
}
