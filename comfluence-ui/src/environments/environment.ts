// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  baseUrlApi: 'http://localhost:9100/comfluencews',
  // baseUrlApi: 'http://10.33.20.157:9100/comfluencews',
  // baseUrlApi: 'http://10.33.20.168:9100/comfluencews',
  version: require('../../package.json').version + '-DEV',
  searchAddressGpsApiRoot: 'https://api-adresse.data.gouv.fr/reverse',
  searchCommuneApiRoot: 'https://geo.api.gouv.fr/communes',
  clientId: 'COMFLUENCE_WEB_CLIENT',
  clientKey: 'rt9MZtEFkKBzZC5BvG8nSRqcE9uJaycV',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
