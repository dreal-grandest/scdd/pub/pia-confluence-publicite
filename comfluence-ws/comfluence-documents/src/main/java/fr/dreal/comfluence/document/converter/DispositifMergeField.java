package fr.dreal.comfluence.document.converter;

import fr.dreal.comfluence.business.data.dispositif.Category;
import fr.dreal.comfluence.business.data.dispositif.PhotoType;
import fr.dreal.comfluence.business.data.dispositif.Position;
import fr.dreal.comfluence.business.dto.dispositif.DispositifDTO;
import fr.dreal.comfluence.business.dto.dispositif.properties.CoordinatesDTO;
import fr.dreal.comfluence.business.dto.dispositif.properties.InfrigementDTO;
import org.apache.commons.lang3.tuple.Pair;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DispositifMergeField extends MergeField<DispositifDTO> {
	
	
	private final List<Position> positions = Arrays.asList(Position.BRIGHT, Position.NUMERIC);
	
	@Override
	protected Map<String, Pair<Function<DispositifDTO, Boolean>,Function<DispositifDTO, Object>>> buildRules() {
		Map<String, Pair<Function<DispositifDTO, Boolean>,Function<DispositifDTO, Object>>> rules = new HashMap<>();

		rules.put("qualification_dispositif", buildUncheckPair(dispositif -> new TypeConverter().convert(dispositif.getType())));
		rules.put("numero_suivi_interne", buildUncheckPair(DispositifDTO::getReference));
		rules.put("dispositif_numeric_bright",
				buildPair(dispositifDTO -> dispositifDTO.getDescription() != null && dispositifDTO.getDescription().getPositions() != null,
                        dispositifDTO -> dispositifDTO.getDescription().getPositions().stream().filter(positions::contains)
                                .map(new PositionConverter()::convert).collect(Collectors.toList())
                        )
				);
		rules.put("dispositif_positions", buildPair(dispositifDTO -> dispositifDTO.getDescription() != null && dispositifDTO.getDescription().getPositions() != null,
                dispositifDTO -> dispositifDTO.getDescription().getPositions().stream()
                        .filter(position -> !positions.contains(position)).map(new PositionConverter()::convert).collect(Collectors.toList())
                )
        );
		rules.put("est_double_face", buildPair(dispositifDTO -> dispositifDTO.getPhotos() != null,
                dispositifDTO -> dispositifDTO.getPhotos().stream()
                        .anyMatch(photoDto -> PhotoType.DOUBLE_FACE.equals(photoDto.getPhotoType()))
                )
        );
		// Address info
		rules.put("commune_dispositif", buildPair(dispositifDTO -> dispositifDTO.getAddress() != null,
                dispositif -> dispositif.getAddress().getCity()
                )
        );
		rules.put("cp_dispositif", buildPair(dispositifDTO -> dispositifDTO.getAddress() != null,
                dispositif -> dispositif.getAddress().getCode()
            )
        );
		rules.put("coordonnees_gps_dispositif", buildUncheckPair(dispositif -> convert(dispositif.getCoordinates())));
		rules.put("adresse_dispositif",buildPair(dispositifDTO -> dispositifDTO.getAddress() != null,
                dispositif -> String
				.format("%s %s", dispositif.getAddress().getNumber(), dispositif.getAddress().getStreet())
            )
        );
		rules.put("code_departement", buildPair(dispositifDTO -> dispositifDTO.getAddress() != null,
                dispositif -> dispositif.getAddress().getCodeDepartement()
                )
        );
		rules.put("dispositif_agglomeration", buildPair(dispositifDTO -> dispositifDTO.getContext() != null,
                dispositif -> new LocationConverter().convert(dispositif.getContext().getLocation())
                )
        );
		// advertiser info
		rules.put("announcer", buildPair(dispositifDTO -> dispositifDTO.getDescription() != null,
                dispositif -> dispositif.getDescription().getAnnouncer()
                )
        );
		// todo mec -> add info in advertiser model, quid des nouveaux publicataires ajouté automatiquement
		rules.put("societe_mec",buildPair(dispositifDTO -> dispositifDTO.getDescription() != null && dispositifDTO.getDescription().getAdvertiser() != null,
				dispositif -> dispositif.getDescription().getAdvertiser().getName() != null ? dispositif
						.getDescription().getAdvertiser().getName() : dispositif.getDescription().getAnnouncer()
                )
        );
		rules.put("cp_mec", buildUncheckPair(d -> null));
		rules.put("commune_mec", buildUncheckPair(d -> null));
		rules.put("adresse_mec", buildUncheckPair(d -> null));
		rules.put("num_siret_mec", buildUncheckPair(d -> null));
		// procedure info
		rules.put("date_courrier", buildUncheckPair(d -> LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
		rules.put("date_constat_infraction", buildUncheckPair(
		        d -> d.getOperationDate() == null ? "": new SimpleDateFormat("dd/MM/yyyy").format(d.getOperationDate())
        ));
		rules.put("montant_astreinte", buildUncheckPair(d -> null)); // todo paramétré en base car national ?

		rules.put("num_pv", buildUncheckPair(d -> null)); // todo générer ?
		rules.put("date_cloture_pv",
                buildUncheckPair(d -> null)); // todo info -> proc == pv.openedProcedure (ajout dun champ "dateClosed" a la place ?)
		rules.put("date_control", buildUncheckPair(d -> d.getOperationDate() == null ? "": new SimpleDateFormat("dd/MM/yyyy").format(d.getOperationDate())));
		// vip info
		// todo ajout dans territorial
		rules.put("civilite_procureur", buildUncheckPair(d -> null));
		rules.put("prefet_titres", buildUncheckPair(d -> null));
		rules.put("adresse_ta", buildUncheckPair(d -> null));
		rules.put("cp_ta", buildUncheckPair(d -> null));
		rules.put("commune_ta", buildUncheckPair(d -> null));
		// infractions list
		rules.put("infractions", buildUncheckPair(DispositifDTO::getInfrigements));
        rules.put("article_ce", buildUncheckPair(dispositifDTO -> {
            if(dispositifDTO.getInfrigements() != null) {
                return dispositifDTO.getInfrigements().stream().map(InfrigementDTO::getCodeArticle).collect(Collectors.joining(", "));
            } else {
                return "";
            }
        }));
        rules.put("nature_infraction", buildUncheckPair(dispositifDTO -> {
            if(dispositifDTO.getInfrigements() != null) {
                return dispositifDTO.getInfrigements().stream().map(InfrigementDTO::getOffenseNature).collect(Collectors.joining(", "));
            } else {
                return "";
            }
        }));
		rules.put("heure_constat_infraction", buildUncheckPair(d -> null)); // todo where to have the info ?
		
		rules.put("beneficie_societe", buildUncheckPair(d -> null));
		rules.put("decret_arretes_nomination_delegations", buildUncheckPair(d -> null));
		rules.put("competence_territoriale_proc", buildUncheckPair(d -> null));
		rules.put("num_dossier", buildUncheckPair(d -> null));
		rules.put("num_amed", buildUncheckPair(d -> null));
		rules.put("num_pv_initial", buildUncheckPair(d -> null));
		rules.put("date_pv_initial", buildUncheckPair(d -> null));
		rules.put("num_parquet", buildUncheckPair(d -> null));
		rules.put("date_notification", buildUncheckPair(d -> null));
		rules.put("num_parquet_initial", buildUncheckPair(d -> null));
		rules.put("date_fin_procedure_contradictoire", buildUncheckPair(d -> null));
		rules.put("jours_depassement_amed", buildUncheckPair(d -> null));
		rules.put("montant_lettres", buildUncheckPair(d -> null));
		rules.put("montant_chiffres", buildUncheckPair(d -> null));
		rules.put("date_depart_calcul_astreintes", buildUncheckPair(d -> null));
		rules.put("date_premiere_liquidation_astreintes", buildUncheckPair(d -> null));
		rules.put("jours_retard_conformite", buildUncheckPair(d -> null));
		
		// dimension info
		rules.put("hauteur_dimension_dispositif", buildPair(d -> d.getDescription() != null && d.getDescription().getDimension() != null &&  d.getDescription().getDimension().getSize() != null,
				d -> !d.getDescription().getDimension().getIsEstimated() ? d.getDescription().getDimension().getSize().getHeight() : convert(d.getDescription().getDimension().getSize().getCategory())));
		rules.put("largeur_dimension_dispositif", buildPair(d -> d.getDescription() != null && d.getDescription().getDimension() != null &&  d.getDescription().getDimension().getSize() != null,
				d -> !d.getDescription().getDimension().getIsEstimated() ? d.getDescription().getDimension().getSize().getWidth() : convert(d.getDescription().getDimension().getSize().getCategory())));
		rules.put("hauteur_sol_dimension_dispositif", buildPair(d -> d.getDescription() != null && d.getDescription().getDimension() != null &&  d.getDescription().getDimension().getSize() != null,
				d -> d.getDescription().getDimension().getSize().getProtrusionGroundDistance()));
		rules.put("surface_dimension_dispositif", buildPair(d -> d.getDescription() != null && d.getDescription().getDimension() != null &&  d.getDescription().getDimension().getSize() != null,
				d -> !d.getDescription().getDimension().getIsEstimated() ? d.getDescription().getDimension().getSize().getHeight() * d.getDescription().getDimension().getSize().getWidth() : convert(d.getDescription().getDimension().getSize().getCategory())));
		rules.put("facade_dimension_dispositif", buildUncheckPair(d -> null)); //TODO on ne connait pas
		rules.put("limite_dimension_dispositif", buildUncheckPair(d -> null)); //TODO on ne connait pas
		rules.put("chaussee_dimension_dispositif", buildUncheckPair(d -> null)); //TODO on ne connait pas
		rules.put("building_dimension_dispositif", buildUncheckPair(d -> null)); //TODO on ne connait pas
		
		return rules;
	}

	private String convert(CoordinatesDTO coordinates) {
		NumberFormat formatter = new DecimalFormat("#0.000000");
		return String.format("%s, %s", formatter.format(coordinates.getLatitude()),
				formatter.format(coordinates.getLongitude()));
	}

	private String convert(Category category) {
	    String result;
	    if(category == null) {
	        result = "";
        } else {
            switch (category) {
                case LARGE:
                    result = "Grand";
                    break;
                case SMALL:
                    result = "Petit";
                    break;
                case MEDIUM:
                    result = "Moyen";
                    break;
                case XLARGE:
                    result = "Très grand";
                    break;
                default:
                    result = "";
            }
        }
	    return result;
    }
}
