package fr.dreal.comfluence.document.dto;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;
import fr.dreal.comfluence.business.entities.Control;
import fr.dreal.comfluence.business.entities.User;
import fr.dreal.comfluence.business.entities.dispositif.Dispositif;
import fr.dreal.comfluence.document.converter.StatusConverter;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter @Setter(AccessLevel.PROTECTED)
public class LicorneEntry {

    /**
     * Domaine/Thème/Action du plan de contrôle
     * Obligatoire
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.DOMAIN_CONTROL_LABEL, required = true)
    private String domainControl;

    /**
     * Objet du contrôle
     * Obligatoire
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.OBJECT_CONTROL_LABEL, required = true)
    private String objectControl;

    /**
     * Date
     * Obligatoire
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.DATE_CONTROL_LABEL, required = true)
    @CsvDate(writeFormat = Constants.PATTERN_LOCAL_DATE_FORMAT, writeFormatEqualsReadFormat = false)
    private LocalDate dateControl;

    /**
     * SIRET de l’intervenant contrôlé
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.SIRET_LABEL)
    private String siret;

    /**
     * Code INSEE de la commune principale
     * Obligatoire
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.INSEE_CODE_LABEL, required = true)
    private String inseeCode;

    /**
     * Code de la masse d’eau principale
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.WATER_PRINCIPAL_CODE_LABEL)
    private String waterPrincipalCode;

    /**
     * Annoncé ou Inopiné
     * Obligatoire
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.CONTROL_PLANNING_LABEL, required = true)
    private String controlPlanning;

    /**
     * Bureau ou Terrain
     * Obligatoire
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.CONTROLLED_PLACE_LABEL, required = true)
    private String controlledPlace;

    /**
     * Contrôleur principal
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.CONTROLLER_LABEL)
    private String controller;

    /**
     * Autre contrôleur
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.ATTENDANT_LABEL)
    private String attendant;

    /**
     * Résultat (Conforme / non conforme)
     * Obligatoire
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.DISPOSITIF_STATUS_LABEL, required = true)
    private String dispositifStatus;

    /**
     * Référence CASCADE
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.CASCADE_REFERENCE_LABEL)
    private String cascadeReference;

    /**
     * Temps de préparation (en jours-homme)
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.PREPARATION_TIME_LABEL, locale = "fr-FR")
    private Double preparationTime;

    /**
     * Temps de contrôle (en jours-homme)
     */
    @CsvBindByName(column = LicorneColumnHeader.Constants.CONTROL_TIME_LABEL, locale = "fr-FR")
    private Double controlTime;

    public static LicorneEntryBuilder builder() {
        return new LicorneEntryBuilder();
    }

    public static final class LicorneEntryBuilder {
        private Dispositif dispositif;
        private Control control;

        private LicorneEntryBuilder() {
        }

        public LicorneEntryBuilder dispositif(Dispositif dispositif) {
            this.dispositif = dispositif;
            return this;
        }

        public LicorneEntryBuilder control(Control control) {
            this.control = control;
            return this;
        }

        public LicorneEntry build() {
            if (dispositif == null) {
                throw new IllegalArgumentException("A dispositif is needed to build a LicorneEntry");
            }
            if (control == null) {
                throw new IllegalArgumentException("A control is needed to build a LicorneEntry");
            }
            final LicorneEntry licorneEntry = new LicorneEntry();
            // mandatory fields
            licorneEntry.setDomainControl(
                    "Protection des habitats et patrimoine naturel / Contrôle d'activités humaines réglementées / Publicité");
            licorneEntry.setObjectControl("Publicité");
            licorneEntry.setDateControl(control.getControlDate());
            licorneEntry.setInseeCode(dispositif.getAddress().getInseeCode());
            licorneEntry.setControlPlanning("Annoncé");
            licorneEntry.setControlledPlace("Terrain");
            licorneEntry.setDispositifStatus(new StatusConverter().convert(dispositif.getStatus()));
            // optional fields
            licorneEntry.setSiret(null);
            licorneEntry.setWaterPrincipalCode(null);
            licorneEntry.setCascadeReference(null);
            licorneEntry.setPreparationTime(0.5);
            licorneEntry.setControlTime(0.5);
            licorneEntry.setController(control.getController().getLastName());
            final User attendant = control.getAttendant();
            if (attendant != null) {
                licorneEntry.setAttendant(attendant.getLastName());
                licorneEntry.setControlTime(1D);
            }
            return licorneEntry;
        }
    }

    private static class Constants {
        private static final String PATTERN_LOCAL_DATE_FORMAT = "dd/MM/yy";

        private Constants() {
        }
    }
}
