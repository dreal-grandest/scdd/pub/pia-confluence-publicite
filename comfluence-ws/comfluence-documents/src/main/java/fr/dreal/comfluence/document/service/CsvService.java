package fr.dreal.comfluence.document.service;

import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import fr.dreal.comfluence.business.entities.Control;
import fr.dreal.comfluence.document.dto.FileWrapper;
import fr.dreal.comfluence.document.dto.LicorneColumnHeader;
import fr.dreal.comfluence.document.dto.LicorneEntry;
import fr.dreal.comfluence.service.ControlService;
import fr.dreal.comfluence.service.FileHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class CsvService {
    /**
     * Logger
     **/
    private static final Logger LOGGER = LoggerFactory.getLogger(CsvService.class);
    private static final String CHARSET_NAME = "windows-1252";
    private static final char COLUMN_SEPARATOR = ';';

    private final FileHelper fileHelper;
    private final ControlService controlService;


    public CsvService(FileHelper fileHelper, ControlService controlService) {
        this.fileHelper = fileHelper;
        this.controlService = controlService;
    }

    public FileWrapper generateLicorneDocument(Long departementCode, LocalDate localDateMin,
            LocalDate localDateMax) throws IOException {
        String generatedOutputDir = this.fileHelper.getGeneratedOutputDir();
        String fileSuffix = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String fileName = String.format("export_LICORNE_%d_%s.csv", departementCode, fileSuffix);
        Path filePath = Paths.get(generatedOutputDir, "licorne", fileName);

        if (Files.notExists(filePath.getParent())) {
            Files.createDirectories(filePath.getParent());
        }
        if (Files.exists(filePath)) {
            Files.delete(filePath);
        }

        try (CSVWriter writer = new CSVWriter(new FileWriter(filePath.toString(), Charset.forName(CHARSET_NAME)),
                COLUMN_SEPARATOR, ICSVWriter.NO_QUOTE_CHARACTER, ICSVWriter.DEFAULT_ESCAPE_CHARACTER,
                ICSVWriter.DEFAULT_LINE_END)) {

            HeaderColumnNameMappingStrategy<LicorneEntry> strategy = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(LicorneEntry.class);
            strategy.setColumnOrderOnWrite(Comparator.<String>comparingInt(headerLabel ->
                            LicorneColumnHeader.getByLabel(headerLabel).getOrder()));

            StatefulBeanToCsv<LicorneEntry> beanToCsv = new StatefulBeanToCsvBuilder<LicorneEntry>(writer)
                    .withMappingStrategy(strategy)
                    .build();
            List<LicorneEntry> licorneEntries = entries(departementCode, localDateMin, localDateMax);
            beanToCsv.write(licorneEntries);
        } catch (CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
            LOGGER.error(e.getMessage(), e);
            fileHelper.deletePhysical(filePath);
            throw new IOException(e.getMessage());
        }

        return new FileWrapper(new File(filePath.toString()), fileName);
    }

    private List<LicorneEntry> entries(Long departementCode, LocalDate localDateMin, LocalDate localDateMax) {
        List<Control> controls = this.controlService
                .findAllForLicorneExport(departementCode, localDateMin, localDateMax);
        return controls.stream().flatMap(control -> control.getDispositifs().stream()
                .map(dispositif -> LicorneEntry.builder().control(control).dispositif(dispositif).build()))
            .collect(Collectors.toList());
    }
}
