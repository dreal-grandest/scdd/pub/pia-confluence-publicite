package fr.dreal.comfluence.document.converter;

import fr.dreal.comfluence.business.data.dispositif.Type;

public class TypeConverter implements EnumConverter<Type> {
	@Override
	public String convert(Type toConvert) {
		String result = null;
		if(toConvert != null) {
            switch (toConvert) {
                case AD:
                    result = "publicité";
                    break;
                case SIGNBOARD:
                    result = "enseigne";
                    break;
                case PRESIGNBOARD:
                    result = "préenseigne";
                    break;
            }
        }
		return result;
	}
}
