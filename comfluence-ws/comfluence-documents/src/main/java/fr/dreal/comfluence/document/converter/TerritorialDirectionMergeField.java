package fr.dreal.comfluence.document.converter;

import fr.dreal.comfluence.business.dto.TerritorialDirectionDto;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class TerritorialDirectionMergeField extends MergeField<TerritorialDirectionDto> {
	
	@Override
	protected Map<String, Pair<Function<TerritorialDirectionDto, Boolean>,Function<TerritorialDirectionDto, Object>>> buildRules() {
		Map<String, Pair<Function<TerritorialDirectionDto, Boolean>,Function<TerritorialDirectionDto, Object>>> rules = new HashMap<>();
		rules.put("prefecture", buildUncheckPair(TerritorialDirectionDto::getPrefecture));
		rules.put("service", buildUncheckPair(TerritorialDirectionDto::getService));
		rules.put("pole", buildUncheckPair(TerritorialDirectionDto::getPole));
		rules.put("ville", buildUncheckPair(TerritorialDirectionDto::getCity));
		rules.put("la_ddt", buildUncheckPair(TerritorialDirectionDto::getDirector));
		rules.put("tel_ddt", buildUncheckPair(TerritorialDirectionDto::getPhoneNumber));
		rules.put("direction_departementale_territoires", buildUncheckPair(TerritorialDirectionDto::getDirection));
		rules.put("fax_ddt", buildUncheckPair(TerritorialDirectionDto::getFaxNumber));
		rules.put("adresse_ddt", buildUncheckPair(TerritorialDirectionDto::getAddress));
		rules.put("autorite_signataire", buildUncheckPair(TerritorialDirectionDto::getSigningAuthority));
		rules.put("prenom_signataire", buildUncheckPair(TerritorialDirectionDto::getFirstNameSigningAuthority));
		rules.put("nom_signataire", buildUncheckPair(TerritorialDirectionDto::getLastNameSigningAuthority));
		return rules;
	}
}
