package fr.dreal.comfluence.document.converter;

import fr.dreal.comfluence.business.data.dispositif.DispositifStatus;

public class StatusConverter implements EnumConverter<DispositifStatus> {
	@Override
	public String convert(DispositifStatus toConvert) {
		String result = null;
		if(toConvert != null) {
            switch (toConvert) {
                case COMPLIANT:
                    result = "Conforme";
                    break;
                case NONCOMPLIANT:
                    result = "Non conforme";
                    break;
                case TODO:
                    result = "A déterminer";
                    break;
            }
        }
		return result;
	}
}
