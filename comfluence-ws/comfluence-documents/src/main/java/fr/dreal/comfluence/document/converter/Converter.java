package fr.dreal.comfluence.document.converter;

public interface Converter<T,R> {
	
	R convert(T toConvert);
}
