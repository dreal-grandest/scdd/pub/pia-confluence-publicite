package fr.dreal.comfluence.document.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.File;

/**
 * @author SopraGroup
 */
@Getter @Setter
@AllArgsConstructor
public class FileWrapper {
    private File file;
    private String name;
}
