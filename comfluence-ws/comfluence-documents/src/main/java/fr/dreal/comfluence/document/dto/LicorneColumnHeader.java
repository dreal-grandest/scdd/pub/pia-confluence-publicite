package fr.dreal.comfluence.document.dto;

import com.opencsv.bean.HeaderNameBaseMappingStrategy;
import lombok.Getter;

import java.util.stream.Stream;

@Getter
public enum LicorneColumnHeader {
    DOMAIN_CONTROL(Constants.DOMAIN_CONTROL_LABEL, 0),
    OBJECT_CONTROL(Constants.OBJECT_CONTROL_LABEL, 1),
    DATE_CONTROL(Constants.DATE_CONTROL_LABEL, 2),
    SIRET(Constants.SIRET_LABEL, 3),
    INSEE_CODE(Constants.INSEE_CODE_LABEL, 4),
    WATER_PRINCIPAL_CODE(Constants.WATER_PRINCIPAL_CODE_LABEL, 5),
    CONTROL_PLANNING(Constants.CONTROL_PLANNING_LABEL, 6),
    CONTROLLED_PLACE(Constants.CONTROLLED_PLACE_LABEL, 7),
    CONTROLLER(Constants.CONTROLLER_LABEL, 8),
    ATTENDANT(Constants.ATTENDANT_LABEL, 9),
    DISPOSITIF_STATUS(Constants.DISPOSITIF_STATUS_LABEL, 10),
    CASCADE_REFERENCE(Constants.CASCADE_REFERENCE_LABEL, 11),
    PREPARATION_TIME(Constants.PREPARATION_TIME_LABEL, 12),
    CONTROL_TIME(Constants.CONTROL_TIME_LABEL, 13);

    private final int order;
    private final String label;

    LicorneColumnHeader(String label, int order) {
        this.label = label;
        this.order = order;
    }

    public static LicorneColumnHeader getByLabel(String label) {
        return Stream.of(values()).filter(header -> header.getLabel().equals(label)).findFirst().orElseThrow();
    }

    /**
     * Constant label must be trim & uppercase for the header ordering to work
     * @see HeaderNameBaseMappingStrategy#findField(int)
     */
    public static class Constants {
        public static final String DOMAIN_CONTROL_LABEL = "DOMAIN_CONTROL_LABEL";
        public static final String OBJECT_CONTROL_LABEL = "OBJECT_CONTROL_LABEL";
        public static final String DATE_CONTROL_LABEL = "DATE_CONTROL_LABEL";
        public static final String SIRET_LABEL = "SIRET_LABEL";
        public static final String INSEE_CODE_LABEL = "INSEE_CODE_LABEL";
        public static final String WATER_PRINCIPAL_CODE_LABEL = "WATER_PRINCIPAL_CODE_LABEL";
        public static final String CONTROL_PLANNING_LABEL = "CONTROL_PLANNING_LABEL";
        public static final String CONTROLLED_PLACE_LABEL = "CONTROLLED_PLACE_LABEL";
        public static final String CONTROLLER_LABEL = "CONTROLLER_LABEL";
        public static final String ATTENDANT_LABEL = "ATTENDANT_LABEL";
        public static final String DISPOSITIF_STATUS_LABEL = "DISPOSITIF_STATUS_LABEL";
        public static final String CASCADE_REFERENCE_LABEL = "CASCADE_REFERENCE_LABEL";
        public static final String PREPARATION_TIME_LABEL = "PREPARATION_TIME_LABEL";
        public static final String CONTROL_TIME_LABEL = "CONTROL_TIME_LABEL";
    }
}
