package fr.dreal.comfluence.document.converter;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.function.Function;

public abstract class MergeField<D> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MergeField.class);
	
	private Map<String, Pair<Function<D, Boolean>,Function<D, Object>>> rules;
	
	public Map<String, Pair<Function<D, Boolean>,Function<D, Object>>> getRules() {
		if (this.rules == null) {
			this.rules = buildRules();
		}
		if(this.rules.isEmpty()){
			LOGGER.warn("MergeFieldRules are empty for {}", this.getClass());
		}
		return rules;
	}

	protected Pair<Function<D, Boolean>,Function<D, Object>> buildPair(Function<D, Boolean> funcCheck, Function<D, Object> funcDo) {
	    return new ImmutablePair<>(funcCheck, funcDo);
    }
    protected Pair<Function<D, Boolean>,Function<D, Object>> buildUncheckPair(Function<D, Object> funcDo) {
        return new ImmutablePair<>(obj -> true, funcDo);
    }
	protected abstract Map<String, Pair<Function<D, Boolean>,Function<D, Object>>> buildRules();
}
