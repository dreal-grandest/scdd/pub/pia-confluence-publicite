package fr.dreal.comfluence.ws;


import fr.dreal.comfluence.business.data.ProcedureGroupe;
import fr.dreal.comfluence.business.dto.ProcedureDto;
import fr.dreal.comfluence.business.dto.ProcedureGroupeDto;
import fr.dreal.comfluence.service.ProcedureService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/procedure")
@Api(value = "Gestion des procedures pour l'application Comfluence")
public class ProcedureController {
	
	private final ProcedureService procedureService;
	
	public ProcedureController(ProcedureService procedureService) {
		this.procedureService = procedureService;
	}
	
	@ApiOperation(value = "Permet de récupérer les procédures applicable au dispositif", response = ProcedureDto.class, responseContainer = "List")
	@GetMapping(value = "/byDevice/{idDevice}/next")
	public ResponseEntity<List<ProcedureDto>> findAllNext(@PathVariable("idDevice") Long idDevice) {
		List<ProcedureDto> procedureDtos = procedureService.findAllNext(idDevice);
		return new ResponseEntity<>(procedureDtos, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Permet de récupérer tous les groupes de procédures", response = ProcedureGroupeDto.class, responseContainer = "List")
	@GetMapping(value = "/groups")
	public ResponseEntity<List<ProcedureGroupeDto>> findAllGroups() {
		return new ResponseEntity<>(Arrays.stream(ProcedureGroupe.values()).
				map(procedureGroupe -> new ProcedureGroupeDto(procedureGroupe.getId(), procedureGroupe.getName(),
						procedureGroupe.getPreviousId())).
				collect(Collectors.toList()), HttpStatus.OK);
	}
}
