package fr.dreal.comfluence.ws;

import fr.dreal.comfluence.business.data.dispositif.PhotoType;
import fr.dreal.comfluence.business.dto.photo.PhotoDto;
import fr.dreal.comfluence.business.entities.Photo;
import fr.dreal.comfluence.service.PhotoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/photo")
@Api("Api permettant de gérer les photos")
public class PhotoController {


    /**
     * Permet l'utilisation de logBack à travers le logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PhotoController.class);


    /** Service pour la gestion des photos **/
    private final PhotoService photoService;
    
    /**
     * Initialisation du user service.
     * @param photoService, service de gestion d'utilisateur.
     */
    public PhotoController(PhotoService photoService) {
        this.photoService = photoService;
    }


    @ApiOperation("Méthode permettant de déposer une photo")
    @PostMapping(path="/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PhotoDto> uploadPhoto(@RequestParam("file-content") MultipartFile multipartFile,
            Long idDispositif, PhotoType type, Boolean active) {
        PhotoDto dto;
        try {
            dto = photoService.savePhoto(idDispositif, multipartFile, type, active);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (IOException e) {
            LOGGER.error("Impossible d 'enregistrer la photo", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Méthode permettant de récupérer une photo")
    @GetMapping(value = "/download/{id}", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<byte[]> getImageWithMediaType(@PathVariable(value = "id") Long idPhoto){
        Photo photo = photoService.getPhoto(idPhoto);
        if(photo != null && photo.getPath() != null) {
            File file = new File(photo.getPath());
            final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.put(HttpHeaders.CONTENT_TYPE, Collections.singletonList(photo.getContentType()));
            try (FileInputStream fileInputStream = new FileInputStream(file)) {
                return new ResponseEntity<>(fileInputStream.readAllBytes(), headers, HttpStatus.OK);
            } catch (IOException e) {
                LOGGER.error("Impossible de trouver la photo", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @ApiOperation("Méthode permettant de récupérer les ids des photos d'un dispositif")
    @GetMapping(value = "/download/byDispositif/{id}")
    public ResponseEntity<List<PhotoDto>> getListId(@PathVariable(value = "id") Long idDispositif){
        List<PhotoDto> photos = photoService.getPhotosByDispositif(idDispositif);
        return new ResponseEntity<>(photos, HttpStatus.OK);
    }

    @GetMapping(value = "/download/thumb/{id}", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<byte[]> getThumbWithMediaType(@PathVariable(value = "id") Long idPhoto){
        try {
            Photo photo = photoService.getPhoto(idPhoto);
            byte[] thumbnail = photoService.getThumb(idPhoto);
            final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.put(HttpHeaders.CONTENT_TYPE, Collections.singletonList(photo.getContentType()));
            return new ResponseEntity<>(thumbnail, headers, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Impossible de trouver la photo", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Méthode permettant de récupérer les photos d'un dispositif")
    @GetMapping(value = "/file/byDispositif/{id}")
    public ResponseEntity<List<PhotoDto>> getPhotoFiles(@PathVariable(value = "id") Long idDispositif){
        try {
            List<PhotoDto> photos = photoService.getPhotosByDispositif(idDispositif);
            return new ResponseEntity<>(photos, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Impossible de trouver les photos", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @ApiOperation("Méthode permettant de supprimer logiquement une photo définie par son id")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable(value = "id") Long id){
        Boolean deleted = photoService.deleteById(id);
        return new ResponseEntity<>(deleted, HttpStatus.OK);
    }
}