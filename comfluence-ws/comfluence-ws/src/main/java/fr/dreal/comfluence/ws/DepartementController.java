package fr.dreal.comfluence.ws;

import fr.dreal.comfluence.business.dto.DepartementDto;
import fr.dreal.comfluence.business.dto.FullDepartementDto;
import fr.dreal.comfluence.service.DepartementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author SopraGroup
 */
@RestController
@Controller
@RequestMapping("/departement")
@Api(value = "Api pour interragir avec les départements")
public class DepartementController {
	private final DepartementService departementService;
	
	public DepartementController(DepartementService departementService) {
		this.departementService = departementService;
	}
	
	@ApiOperation(value = "Permet de récupérer tous les départements présents dans l'applicaiton", response = DepartementDto.class, responseContainer = "List")
	@GetMapping("")
	public ResponseEntity<List<DepartementDto>> findAll() {
		List<DepartementDto> result = departementService.findAll();
		return ResponseEntity.ok(result);
	}

    @ApiOperation(value = "Permet de récupérer tous les départements présents dans l'application" +
            " avec toute leur configuration", response = DepartementDto.class, responseContainer = "List")
    @GetMapping("/full")
    public ResponseEntity<List<FullDepartementDto>> findAllFull() {
        List<FullDepartementDto> result = departementService.findAllFull();
        return ResponseEntity.ok(result);
    }
	
	@ApiOperation(value = "Sauvegarde un département présent dans l'application", response = DepartementDto.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
	@PostMapping("")
	public ResponseEntity<DepartementDto> save(@RequestBody DepartementDto departementDto) {
		DepartementDto result = departementService.save(departementDto);
		return ResponseEntity.ok(result);
	}

    @ApiOperation(value = "Sauvegarde un département présent dans l'application" +
            " avec toute sa configuration", response = DepartementDto.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("/full")
    public ResponseEntity<FullDepartementDto> saveFull(@RequestBody FullDepartementDto departementDto) {
        FullDepartementDto result = departementService.saveFull(departementDto);
        return ResponseEntity.ok(result);
    }
}
