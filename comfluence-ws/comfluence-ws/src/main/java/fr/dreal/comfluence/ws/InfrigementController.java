package fr.dreal.comfluence.ws;

import fr.dreal.comfluence.business.dto.dispositif.properties.InfrigementDTO;
import fr.dreal.comfluence.service.InfrigementService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/infrigement")
public class InfrigementController {
	
	private InfrigementService infrigementService;
	
	public InfrigementController(InfrigementService infrigementService) {
		this.infrigementService = infrigementService;
	}
	
	@GetMapping("")
	public ResponseEntity<List<InfrigementDTO>> findAll() {
		final List<InfrigementDTO> body = infrigementService.findAll();
		return ResponseEntity.ok(body);
	}
}
