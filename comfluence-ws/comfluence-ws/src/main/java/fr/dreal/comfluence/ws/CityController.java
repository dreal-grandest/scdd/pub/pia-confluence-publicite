package fr.dreal.comfluence.ws;

import fr.dreal.comfluence.business.dto.CityDto;
import fr.dreal.comfluence.business.dto.ControlDto;
import fr.dreal.comfluence.service.CityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/city")
@Api(value = "Api city")
public class CityController {
	
	private final CityService cityService;
	
	public CityController(CityService cityService) {
		this.cityService = cityService;
	}
	
	
	@ApiOperation(value = "Permet de récupérer les villes contenues dans les Geometry", response = ControlDto.class)
	@PostMapping("/byGeometries")
	public ResponseEntity<List<CityDto>> findByGeometries(@RequestBody List<String> geometries) {
        List<CityDto> result;
        if(geometries == null || geometries.isEmpty()) {
            result = Collections.emptyList();
        } else {
            result = cityService.findAllIntersect(geometries);
        }
		if (result != null) {
			return ResponseEntity.ok(result);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
