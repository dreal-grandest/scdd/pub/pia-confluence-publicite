package fr.dreal.comfluence.configuration;

import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger configuration, on y accède via http://localhost:8080/swagger-ui.html
 *
 */

@Configuration
@EnableSwagger2
public class Swagger2Config {
    
    private final BuildProperties buildProperties;
    
    public Swagger2Config(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }
    
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors
                        .basePackage("fr.dreal.comfluence.ws"))
                .paths(PathSelectors.regex("/.*"))
                .build().apiInfo(apiEndPointsInfo());
    }
    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("Spring Boot REST API")
                .description("Comfluence REST API")
     //           .contact(new Contact("Ramesh Fadatare", "www.javaguides.net", "ramesh24fadatare@gmail.com"))
     //           .license("Apache 2.0")
     //           .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .version(buildProperties.getVersion())
                .build();
    }
}