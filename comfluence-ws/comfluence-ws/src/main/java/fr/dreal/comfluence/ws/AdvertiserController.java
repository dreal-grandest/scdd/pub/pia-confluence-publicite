package fr.dreal.comfluence.ws;

import fr.dreal.comfluence.business.dto.AdvertiserDto;
import fr.dreal.comfluence.service.AdvertiserService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/advertiser")
@Api(value = "Api advertiser")
public class AdvertiserController {
	
	private final AdvertiserService advertiserService;
	
	public AdvertiserController(AdvertiserService advertiserService) {
		this.advertiserService = advertiserService;
	}
	
	@GetMapping(value = "")
	public ResponseEntity<List<AdvertiserDto>> findAll() {
		final List<AdvertiserDto> advertisers = this.advertiserService.findAll();
		return ResponseEntity.ok(advertisers);
	}
	
	
	@GetMapping(value = "/byDepartement/{code}")
	public ResponseEntity<List<AdvertiserDto>> findAllByDepartement(@PathVariable("code") Long code) {
		final List<AdvertiserDto> advertisers = this.advertiserService.findAllByDepartement(code);
		return ResponseEntity.ok(advertisers);
	}
}
