package fr.dreal.comfluence.configuration;

import fr.dreal.comfluence.business.dto.UserDto;
import fr.dreal.comfluence.business.entities.Role;
import fr.dreal.comfluence.business.entities.User;
import fr.dreal.comfluence.configuration.oauth.ConnectedUserDetails;
import fr.dreal.comfluence.mapper.UserMapper;
import fr.dreal.comfluence.repository.UserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailsService extends JdbcUserDetailsManager {
	
	private final UserRepository userRepository;
	private PasswordEncoder passwordEncoder;
	
	public CustomUserDetailsService(DataSource dataSource, UserRepository userRepository) {
		super(dataSource);
		this.userRepository = userRepository;
    }
	
	@Override
	public UserDetails loadUserByUsername(String username) {
		User user = userRepository.findFirstByMailAndEnabledTrueAndDeletedFalse(username.trim().toLowerCase());
		if (user == null) {
			this.logger.debug("Query returned no results for user '" + username + "'");
			
			throw new UsernameNotFoundException(
					this.messages.getMessage("JdbcDaoImpl.notFound",
							new Object[] { username }, "Username {0} not found"));
		}
		if(!user.getPwdEncrypted()) {
		    this.logger.error("Mot de passe en claire détecté pour l'utilisateur "+user.getMail()+". On le chiffre.");
		    user.setPassword(passwordEncoder.encode(user.getPassword()));
		    user.setPwdEncrypted(true);
		    userRepository.save(user);
        }

        UserDto userDto = UserMapper.INSTANCE.entityToDto(user);

        List<SimpleGrantedAuthority> grantedAuthorityList = new ArrayList<>();
        if(user.getRoles() != null) {
            for(Role role: user.getRoles()) {
                grantedAuthorityList.add(new SimpleGrantedAuthority("ROLE_"+role.getName().toUpperCase()));
            }
        }
		return new ConnectedUserDetails(userDto, user.getPassword(), grantedAuthorityList);
	}

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public PasswordEncoder getPasswordEncoder() {
        return passwordEncoder;
    }
}
