package fr.dreal.comfluence.configuration.oauth;

import fr.dreal.comfluence.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.stereotype.Service;

/**
 * @author SopraGroup
 */
@Service
public class ComfluenceTokenStore extends JwtTokenStore {
    private static final Logger LOGGER = LoggerFactory.getLogger(ComfluenceTokenStore.class);
    private final UserService userService;
    public ComfluenceTokenStore(@Lazy JwtAccessTokenConverter jwtTokenEnhancer, @Lazy UserService userService) {
        super(jwtTokenEnhancer);
        this.userService = userService;
    }

    @Override
    public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
        if(authentication.isAuthenticated() && authentication.getPrincipal() instanceof ConnectedUserDetails) {
            ConnectedUserDetails connectedUserDetails = (ConnectedUserDetails) authentication.getPrincipal();
            super.storeAccessToken(token, authentication);
            userService.storeTokenWithMail(connectedUserDetails.getUser().getMail(), token.getValue());

        }
    }

    @Override
    public OAuth2AccessToken readAccessToken(String tokenValue) {
        if(!userService.isTokenValid(tokenValue)) {
            throw new InvalidTokenException("Token non trouvé dans la base de données");
        }
        return super.readAccessToken(tokenValue);
    }
}
