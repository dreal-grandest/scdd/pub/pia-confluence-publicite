package fr.dreal.comfluence.ws;

import fr.dreal.comfluence.business.dto.InformationDto;
import fr.dreal.comfluence.business.dto.dispositif.DispositifDTO;
import fr.dreal.comfluence.business.dto.log.LoggableAction;
import fr.dreal.comfluence.business.entities.CourrierTemplate;
import fr.dreal.comfluence.business.entities.Information;
import fr.dreal.comfluence.document.dto.FileWrapper;
import fr.dreal.comfluence.document.service.CsvService;
import fr.dreal.comfluence.document.service.DocumentService;
import fr.dreal.comfluence.mapper.InformationMapper;
import fr.dreal.comfluence.service.DispositifService;
import fr.dreal.comfluence.service.InformationService;
import fr.dreal.comfluence.service.LogService;
import fr.dreal.comfluence.service.ProcedureService;
import fr.dreal.comfluence.ws.request.InformationRequestDto;
import fr.dreal.comfluence.ws.request.UpdateFileRequestDto;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/document")
public class DocumentController {
	
	
	/**
	 * Permet l'utilisation de logBack à travers le LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentController.class);
	
	
	private final InformationService informationService;
	private final DocumentService documentService;
	private final DispositifService dispositifService;
	private final ProcedureService procedureService;
	private final LogService logService;
	private final String outputDir;
	private final CsvService csvService;
	
	
	/**
	 * Initialisation du user service.
	 *
	 * @param informationService, service de gestion de dispositif.
	 * @param csvService
	 */
	public DocumentController(InformationService informationService,
			@Value("${comfluence.upload.files.dir:/tmp}") String outputDir, DocumentService documentService,
			DispositifService dispositifService, ProcedureService procedureService, LogService logService,
			CsvService csvService) {
		this.informationService = informationService;
		this.documentService = documentService;
		this.dispositifService = dispositifService;
		this.outputDir = outputDir;
		this.procedureService = procedureService;
		this.logService = logService;
		this.csvService = csvService;
	}
	
	/**
	 * Permet la sauvegarde d'un dispositif dans la base de données.
	 *
	 * @param informationDto, le dispositif à sauvegarder.
	 * @return ResponseEntity<ResponseUserDTO>
	 */
	@ApiOperation(value = "Sauvegarde un commentaire simple", response = InformationRequestDto.class)
	@PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<InformationDto> save(@ModelAttribute InformationRequestDto informationDto, Principal principal) {
        Information informationForRollback = null;
        if(informationDto.getId() != null){
            informationForRollback = informationService.getEntity(informationDto.getId());
        }
		InformationDto saveInfo = informationService.save(informationDto);
		Long idDevice = saveInfo.getIdDevice();
		DispositifDTO dispositif = dispositifService.getDispositifById(saveInfo.getIdDevice());
		Information informationComplete = informationService.getEntity(saveInfo.getId());
		if (informationComplete.getProcedure() != null) {
			try {
				FileWrapper document = documentService.generateDocumentFromTemplate(informationComplete.getId(),
						informationComplete.getProcedure().getId(), dispositif, principal.getName());
				// Faire qqchose si le document est null
				informationComplete.setPath(document.getFile().getAbsolutePath());
				informationComplete.setContentType("application/vnd.oasis.opendocument.text");
				informationComplete.setOriginalFileName(document.getName());
				informationComplete = informationService.save(informationComplete, idDevice);
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				rollback(informationForRollback, saveInfo.getId());
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		
		final MultipartFile multipartFile = informationDto.getFile();
		if (multipartFile != null && !multipartFile.isEmpty()) {
			File file = Paths.get(outputDir, idDevice.toString()).toFile();
			if (!file.exists() && !file.mkdirs()) {
				LOGGER.error("Can't create directory : {}", file.getPath());
                rollback(informationForRollback, saveInfo.getId());
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			try {
				final Path filePath = Paths
						.get(outputDir, String.valueOf(idDevice), "doc_" + informationComplete.getDispositif());
				multipartFile.transferTo(filePath);
				informationComplete.setPath(filePath.toFile().getAbsolutePath());
				informationComplete.setContentType(multipartFile.getContentType());
				informationComplete.setOriginalFileName(multipartFile.getOriginalFilename());
			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);
                rollback(informationForRollback, saveInfo.getId());
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			informationComplete = informationService.save(informationComplete, idDevice);
		}
		logService.logActionOnProcedure("", LoggableAction.Type.MODIFY, "Commentaire sauvé", idDevice,
				informationComplete.getId());
		return new ResponseEntity<>(InformationMapper.INSTANCE.entityToDto(informationComplete), HttpStatus.CREATED);
	}

	private void rollback(Information information, Long idInfo) {
	    if(information == null) {
	        this.informationService.remove(idInfo);
        } else {
	        this.informationService.update(information);
        }
    }
	
	@ApiOperation(value = "Récupère les commentaires associés à un dispositif", response = InformationDto.class, responseContainer = "List")
	@GetMapping(value = "/byDevice/{id}")
	public ResponseEntity<List<InformationDto>> findAllByDevice(@PathVariable(value = "id") Long id) {
		List<InformationDto> informationDtoResponse = informationService.findAllByDevice(id);
		return new ResponseEntity<>(informationDtoResponse, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Retourne le document associé à un dispositif", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@GetMapping(value = "/download/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> getDocument(@PathVariable Long id,
			@RequestParam(required = false) String contentType) {
		try {
			Information information = informationService.getEntity(id);
			final byte[] document = getDocumentAsByteArray(information, contentType);
			
			return prepareHeaderResponseOk(contentType != null ? contentType : information.getContentType())
					.header(HttpHeaders.CONTENT_DISPOSITION,
							"attachment; filename=\"" + information.getOriginalFileName() + "\"")
                    .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION).body(document);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ApiOperation(value = "Retourne le document associé à un dispositif", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@GetMapping(value = "/preview/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> getDocumentPreview(@PathVariable Long id,
			@RequestParam(required = false) String contentType) {
		try {
			Information information = informationService.getEntity(id);
			final byte[] document = getDocumentAsByteArray(information, contentType);
			
			return prepareHeaderResponseOk(contentType != null ? contentType : information.getContentType())
					.body(document);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	private static ResponseEntity.BodyBuilder prepareHeaderResponseOk(String contentType) {
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, contentType);
	}
	
	private byte[] getDocumentAsByteArray(Information information, String contentType) throws IOException {
		Path path = Paths.get(information.getPath());
		
		byte[] document = FileUtils.readFileToByteArray(path.toFile());
		if (contentType != null) {
			if (MediaType.APPLICATION_PDF_VALUE.equalsIgnoreCase(contentType)) {
				if (!MediaType.APPLICATION_PDF_VALUE.equalsIgnoreCase(information.getContentType())) {
					document = documentService.convertDocumentToPdf(document);
				}
			} else {
				LOGGER.warn("Content-Type not supported : {}.", contentType);
			}
		}
		return document;
	}
	
	@ApiOperation(value = "Permet de clore une procédure en fonction de l'id du dispositif et de l'id de la procédure")
	@PutMapping(value = "/{idDevice}/{idDoc}/close")
	public ResponseEntity<Boolean> closeProcedure(@PathVariable("idDevice") Long idDevice,
			@PathVariable("idDoc") Long idDoc, @RequestParam(required = false) Date closeDate, @RequestParam(required = false) Boolean sansSuite) {
		Boolean result = informationService.closeProcedure(idDevice, idDoc, closeDate, sansSuite);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Permet de mettre à jour un document d'une procédure", response = InformationDto.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@PostMapping(value = "/{idDevice}/{idInfo}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<InformationDto> updateProcDocument(@PathVariable("idDevice") Long idDevice,
			@PathVariable("idInfo") Long idInfo, @ModelAttribute UpdateFileRequestDto updateFileRequestDto) {
		Information information = informationService.getEntity(idInfo);
		if (information == null || information.getProcedure() == null || information
				.getDispositif() == null || !information.getDispositif().getId().equals((idDevice))) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		if (information.getCloseDate() != null || updateFileRequestDto.getFile() == null) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		List<CourrierTemplate> courrierTemplateList = procedureService
				.findAllAssociatedTemplateById(information.getProcedure().getId());
		if (courrierTemplateList != null && !courrierTemplateList.isEmpty()) {
			logService.logActionOnProcedure("", LoggableAction.Type.MODIFY, "Mise à jour du document de la procédure",
					idDevice, idInfo);
			
			FileWrapper fileWrapper = documentService
					.updateProcDocument(information.getPath(), courrierTemplateList.get(0),
							updateFileRequestDto.getFile());
			
			if (fileWrapper != null) {
				information.setPath(fileWrapper.getFile().getAbsolutePath());
				information.setContentType(updateFileRequestDto.getFile().getContentType());
				information.setOriginalFileName(updateFileRequestDto.getFile().getOriginalFilename());
				information = informationService.save(information, idDevice);
			}
		}
		return new ResponseEntity<>(InformationMapper.INSTANCE.entityToDto(information), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Permet de mettre les champs d'un document d'une procédure", response = InformationDto.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@PatchMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<InformationDto> patchProcedure(@PathVariable Long id,
			@RequestBody InformationDto informationDto) {
		if (id == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(this.informationService.update(id, informationDto));
	}
	
	@ApiOperation(value = "Retourne l'export LICORNE du jour ?", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@GetMapping(value = "/licorne/{depCode}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> getLicorneCsv(@PathVariable(value = "depCode") Long departementCode,
			@RequestParam(value = "dateMin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate localDateMin,
			@RequestParam(value = "dateMax") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate localDateMax) {
		try {
			FileWrapper document = csvService.generateLicorneDocument(departementCode, localDateMin, localDateMax);
			
			return prepareHeaderResponseOk("text/csv")
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + document.getName() + "\"")
					.body(FileUtils.readFileToByteArray(document.getFile()));
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
