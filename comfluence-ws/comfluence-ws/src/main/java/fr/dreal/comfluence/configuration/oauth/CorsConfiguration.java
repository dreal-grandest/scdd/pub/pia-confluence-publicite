package fr.dreal.comfluence.configuration.oauth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletContextInitializerBeans;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.List;

/**
 * We add this WebSecurityConfigurerAdapter to get the preflight OPTIONS request before connecting to not respond with 401
 */
@Configuration
@Order(-1)
public class CorsConfiguration extends WebSecurityConfigurerAdapter {
    /**
     * list the allowed origins in application.properties separated by coma
     * Don't use space in the string
     * Fallback to default "*" if not defined
     */
    @Value("#{'${comfluence.oauth.authorizedUrl:*}'.split(',')}")
    private List<String> allowedOrigins;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors(Customizer.withDefaults())
                .requestMatchers(requestMatchers ->
                        requestMatchers.antMatchers(HttpMethod.OPTIONS, "/oauth/token")
                );
    }

    /**
     * Define a filter for the CORS rules
     * It is added to the List<ServletContextInitializer> inside {@link ServletContextInitializerBeans} at the top {@link Order}
     */
    @Bean
    @Order(value = Ordered.HIGHEST_PRECEDENCE)
    CorsFilter corsFilter() {
        org.springframework.web.cors.CorsConfiguration configuration = new org.springframework.web.cors.CorsConfiguration();
        configuration.setAllowedOrigins(allowedOrigins);
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return new CorsFilter(source);
    }
}
