package fr.dreal.comfluence.ws;

import fr.dreal.comfluence.business.dto.FullUserDto;
import fr.dreal.comfluence.business.dto.UserDto;
import fr.dreal.comfluence.service.UserService;
import fr.dreal.comfluence.ws.request.CreateUserDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;
import java.util.List;

/**
 * @author SopraGroup
 */
@RestController
@RequestMapping("/user")
@Api(value = "Api pour interragir avec les utilisateurs")
public class UsersController {
    private final UserService userService;
    private final ConsumerTokenServices consumerTokenServices;

    public UsersController(UserService userService, ConsumerTokenServices consumerTokenServices) {
        this.userService = userService;
        this.consumerTokenServices = consumerTokenServices;
    }

    @ApiOperation(value = "Permet de récupérer la liste des utilisateurs", response = List.class)
    @GetMapping("")
    public ResponseEntity<List<UserDto>> getAllUser() {
        List<UserDto> result = userService.getAllUsers();
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value = "Permet de récupérer la liste des utilisateurs (toute les propriétés)", response = List.class)
    @GetMapping("/full")
    public ResponseEntity<List<FullUserDto>> getAllFullUser(@RequestParam(value = "showDisabled", required = false) Boolean showDisabled) {
        List<FullUserDto> result = userService.getAllFullUsers(showDisabled);
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value = "Permet de créer un utilisateur", response = UserDto.class)
    @PostMapping("")
    public ResponseEntity<FullUserDto> createUser(@RequestBody CreateUserDto createUserDto) {
        if(StringUtils.isBlank(createUserDto.getMail()) || StringUtils.isBlank(createUserDto.getPwd()) || createUserDto.getDep() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        FullUserDto result = userService.addUser(createUserDto.getFirstName(), createUserDto.getLastName(), createUserDto.getMail(),
                createUserDto.getDep(), createUserDto.getPwd(), createUserDto.getRoles());
        if(result != null) {
            return ResponseEntity.ok(result);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Permet de modifier un utilisateur", response = Boolean.class)
    @PutMapping("/{id}")
    public ResponseEntity<Boolean> updateUser(@PathVariable("id") long id, @RequestBody FullUserDto userDto) {
        boolean result = userService.updateUser(id, userDto);
        if(result) {
            return ResponseEntity.ok(result);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Permet d'activer ou non un utilisateur", response = UserDto.class)
    @PutMapping("/{id}/enable")
    public ResponseEntity<Boolean> enableUser(@PathVariable("id") long id, @FormParam("enable") Boolean enable) {
        if(enable == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        boolean result = userService.enableUser(id, enable);
        if(result) {
            if(!enable) {
                revokeToke(id);
            }
            return ResponseEntity.ok(result);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Permet d'associer un utilisateur à un département", response = UserDto.class)
    @PutMapping("/{email}/associateDep")
    public ResponseEntity<UserDto> associateDep(@PathVariable("email")String email, @QueryParam("depCode") Long depCode) {
        UserDto userDto = userService.associateDepartement(email, depCode);
        if(userDto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.ok(userDto);
        }
    }

    @ApiOperation(value = "Permet de récupérer la liste des utilisateurs d'un département", response = UserDto.class, responseContainer = "List")
    @GetMapping("/byDepartmentCode/{departmentCode}")
    public ResponseEntity<List<UserDto>> findAllByDepartment(@PathVariable("departmentCode") Long departmentCode) {
        List<UserDto> userDtos = userService.findAllByDepartment(departmentCode);
        if(userDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.ok(userDtos);
        }
    }

    @ApiOperation(value = "Permet de récupérer l'utilisateur par son mail", response = UserDto.class, responseContainer = "List")
    @GetMapping("/byMail/{mail}")
    public ResponseEntity<UserDto> findByMail(@PathVariable("mail") String mail) {
        UserDto userDto = userService.findByMail(mail);
        if(userDto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.ok(userDto);
        }
    }

    @ApiOperation(value = "Permet de modifier le mot de passe d'un utilisateur")
    @PutMapping("/{id}/pwd")
    public ResponseEntity<Boolean> updatePwd(@PathVariable("id") Long id, @FormParam("pwd") String pwd) {
        if(StringUtils.isBlank(pwd)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        boolean result = userService.updatePwd(id, pwd);
        if(result) {
            return ResponseEntity.ok(result);
        } else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Supprime un utilisateur")
    @DeleteMapping("/{id}")
    public ResponseEntity updatePwd(@PathVariable("id") Long id) {
        userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private void revokeToke(long id) {
        List<String> oldTokenList = userService.revokeToken(id);
    }
}
