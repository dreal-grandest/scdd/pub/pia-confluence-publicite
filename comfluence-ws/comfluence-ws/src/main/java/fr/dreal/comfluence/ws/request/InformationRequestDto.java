package fr.dreal.comfluence.ws.request;

import fr.dreal.comfluence.business.dto.InformationDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

public class InformationRequestDto extends InformationDto {
	
	@Getter @Setter
	MultipartFile file;
	
}
