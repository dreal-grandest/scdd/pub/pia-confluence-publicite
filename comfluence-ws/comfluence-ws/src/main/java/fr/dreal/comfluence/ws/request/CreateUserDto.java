package fr.dreal.comfluence.ws.request;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author SopraGroup
 */
@Getter @Setter
public class CreateUserDto {
    private String firstName;
    private String lastName;
    private String mail;
    private String pwd;
    private Long dep;
    private List<Long> roles;
}
