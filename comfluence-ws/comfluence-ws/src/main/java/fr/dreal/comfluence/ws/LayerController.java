package fr.dreal.comfluence.ws;

import fr.dreal.comfluence.business.dto.LayerDto;
import fr.dreal.comfluence.externalapi.dto.WXSResponse;
import fr.dreal.comfluence.externalapi.ws.LayerWs;
import fr.dreal.comfluence.service.LayerService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/layer")
public class LayerController {
	
	
	/**
	 * Permet l'utilisation de logBack à travers le LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(LayerController.class);
	
	private final LayerService layerService;
	private final LayerWs layerWs;
	
	public LayerController(LayerService layerService, LayerWs layerWs) {
		this.layerService = layerService;
		this.layerWs = layerWs;
	}
	
	
	/**
	 * Permet la récupération de la définition de toutes les couches cartographiques pour un département.
	 * En cas d'échec renvoit une liste vide.
	 *
	 * @return ResponseEntity<List<LayerDto>>
	 */
	@ApiOperation(value = "Retourne toutes les couches cartographiques WMS associées à un département",
			response = LayerDto.class, responseContainer = "List")
	@GetMapping(value = "/byDepartement/{code}")
	public ResponseEntity<List<LayerDto>> getLayersBy(@PathVariable(value = "code") Long code) {
		List<LayerDto> layerList = layerService.findWMSByCodeDepartement(code);
		String logInfo = String.format("Layers List : %1$s.", layerList.toString());
		LOGGER.debug(logInfo);
		return new ResponseEntity<>(layerList, HttpStatus.OK);
	}

    /**
     * Permet la récupération de la définition de toutes les couches cartographiques.
     * En cas d'échec renvoit une liste vide.
     *
     * @return ResponseEntity<List<LayerDto>>
     */
    @ApiOperation(value = "Retourne toutes les couches cartographiques WMS",
            response = LayerDto.class, responseContainer = "List")
    @GetMapping(value = "/")
    public ResponseEntity<List<LayerDto>> getAllLayers() {
        List<LayerDto> layerList = layerService.findAllWMS();
        return new ResponseEntity<>(layerList, HttpStatus.OK);
    }

    @ApiOperation(value = "Permet de faire 'proxy' entre l'application et les services de couches.")
    @GetMapping(value = "/proxy/{id}", produces = MediaType.ALL_VALUE)
    public ResponseEntity<? extends Object> proxyLayer(@PathVariable("id")Long id, @RequestParam Map<String, String> queryMap) {
        LayerDto layerDto = layerService.findOneById(id);
        if(layerDto == null) {
            return ResponseEntity.notFound().build();
        } else {
            WXSResponse response = this.layerWs.proxyLayer(layerDto.getUrl(), queryMap);
            if(response == null) {
                return ResponseEntity.noContent().build();
            } else {
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.add("Content-Type",response.getContentType().toString());

                return new ResponseEntity(response.getContent(), responseHeaders,HttpStatus.OK);
            }
        }
    }

    @ApiOperation(value = "Recherche les restrictions associées à un point donnée.")
    @GetMapping(value = "/searchRestriction/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> searchRestriction(@PathVariable("code") Long code, @RequestParam Double x, @RequestParam Double y) {
        List<String> restrictionList = layerService.searchRestriction(code, x, y);
        return ResponseEntity.ok(restrictionList);
    }
}
