package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.data.dispositif.DispositifStatus;
import fr.dreal.comfluence.business.data.dispositif.Type;
import fr.dreal.comfluence.business.dto.dispositif.StatisticsDispositifsDTO;
import fr.dreal.comfluence.business.entities.Control;
import fr.dreal.comfluence.business.entities.StatisticsControl;
import fr.dreal.comfluence.business.entities.dispositif.Dispositif;
import fr.dreal.comfluence.business.entities.dispositif.properties.Coordinates;
import fr.dreal.comfluence.repository.mock.DispositifRespositoryImpl;
import fr.dreal.comfluence.repository.mock.InformationRespositoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

@RunWith(JUnit4.class)
public class StatisticsServiceTests {
	
	private final DispositifRespositoryImpl dispositifRespository = new DispositifRespositoryImpl();
	private StatisticsService statisticsService;
	
	@Before
	public void initStatisticsService() {
		this.statisticsService = new StatisticsService(dispositifRespository, new InformationRespositoryImpl());
		Assert.assertNotNull(this.statisticsService);
	}
	
	@Test
	public void findDispositifsStatistiquesTest() {
		final int compliant = 15;
		final int nonCompliant = 8;
		final int undefined = 7;
		List<Dispositif> dispositifs = randomDispositifs(compliant, (disp) -> disp.setStatus(DispositifStatus.COMPLIANT));
		dispositifs.addAll(randomDispositifs(nonCompliant, (disp) -> disp.setStatus(DispositifStatus.NONCOMPLIANT)));
		dispositifs.addAll(randomDispositifs(undefined, (disp) -> disp.setStatus(DispositifStatus.TODO)));
		dispositifRespository.saveAll(dispositifs);
		
		StatisticsDispositifsDTO statisticsDispositifs = this.statisticsService.findDispositifsStatistiques();
		Assert.assertNotNull(statisticsDispositifs);
		Assert.assertEquals((Long) Integer.toUnsignedLong(compliant), statisticsDispositifs.getCompliantDevices());
		Assert.assertEquals((Long) Integer.toUnsignedLong(nonCompliant), statisticsDispositifs.getNonCompliantDevices());
		Assert.assertEquals((Long) Integer.toUnsignedLong(undefined), statisticsDispositifs.getTodoDevices());
		
		dispositifRespository.deleteAll();
		
		final int ad = 15;
		final int presignboard = 8;
		final int signboard = 7;
		dispositifs = randomDispositifs(ad, (disp) -> disp.setType(Type.AD));
		dispositifs.addAll(randomDispositifs(presignboard, (disp) -> disp.setType(Type.PRESIGNBOARD)));
		dispositifs.addAll(randomDispositifs(signboard, (disp) -> disp.setType(Type.SIGNBOARD)));
		dispositifRespository.saveAll(dispositifs);
		
		statisticsDispositifs = this.statisticsService.findDispositifsStatistiques();
		Assert.assertEquals((Long) Integer.toUnsignedLong(ad), statisticsDispositifs.getAdDevices());
		Assert.assertEquals((Long) Integer.toUnsignedLong(presignboard), statisticsDispositifs.getPreSignboardDevices());
		Assert.assertEquals((Long) Integer.toUnsignedLong(signboard), statisticsDispositifs.getSignboardDevices());
		
		Assert.assertEquals(LocalDate.now(), statisticsDispositifs.getLastCall());
	}
	
	@Test
	public void updateStatistics() {
		final Control control = new Control();
		final ArrayList<Dispositif> dispositifs = new ArrayList<>();
		control.setDispositifs(dispositifs);
		Assert.assertNull(control.getStatisticsControl());
		
		statisticsService.updateStatistics(control);
		StatisticsControl statisticsControl = control.getStatisticsControl();
		Assert.assertNotNull(statisticsControl);
		Assert.assertEquals((Double) 0D, statisticsControl.getTraveledDistance());
		Assert.assertEquals((Long) 0L, statisticsControl.getDuration());
		Assert.assertEquals((Integer) 0, statisticsControl.getControlledDevices());
		Assert.assertNull(statisticsControl.getStartDate());
		
		Calendar calendar = Calendar.getInstance();
		final Date start = calendar.getTime();
		dispositifs.add(mockDispositif(0, (disp) -> {
			disp.setOperationDate(start);
			final Coordinates coordinates = new Coordinates();
			coordinates.setLatitude(48.582844);
			coordinates.setLongitude(7.751215);
			disp.setCoordinates(coordinates);
		}));
		statisticsService.updateStatistics(control);
		statisticsControl = control.getStatisticsControl();
		Assert.assertNotNull(statisticsControl);
		Assert.assertEquals((Double) 0D, statisticsControl.getTraveledDistance());
		Assert.assertEquals((Long) 5L, statisticsControl.getDuration());
		Assert.assertEquals((Integer) 1, statisticsControl.getControlledDevices());
		Assert.assertEquals(start, statisticsControl.getStartDate());
		
		calendar.add(Calendar.HOUR, 1);
		final Date secondDate = calendar.getTime();
		dispositifs.add(mockDispositif(1, (disp) -> {
			disp.setOperationDate(secondDate);
			final Coordinates coordinates = new Coordinates();
			coordinates.setLatitude(48.585118);
			coordinates.setLongitude(7.754557);
			disp.setCoordinates(coordinates);
		}));
		statisticsService.updateStatistics(control);
		Assert.assertEquals(448, (int) statisticsControl.getTraveledDistance().doubleValue());
		Assert.assertEquals((Long) ((secondDate.getTime() - start.getTime()) /  (1000 * 60)), statisticsControl.getDuration());
		Assert.assertEquals((Integer) 2, statisticsControl.getControlledDevices());
		Assert.assertEquals(start, statisticsControl.getStartDate());
		
		calendar.add(Calendar.HOUR, 1);
		final Date last = calendar.getTime();
		dispositifs.add(mockDispositif(1, (disp) -> {
			disp.setOperationDate(last);
			final Coordinates coordinates = new Coordinates();
			coordinates.setLatitude(48.591098);
			coordinates.setLongitude(7.761556);
			disp.setCoordinates(coordinates);
		}));
		statisticsService.updateStatistics(control);
		Assert.assertEquals(1467, (int) statisticsControl.getTraveledDistance().doubleValue());
		Assert.assertEquals((Long) ((last.getTime() - start.getTime()) /  (1000 * 60)), statisticsControl.getDuration());
		Assert.assertEquals((Integer) 3, statisticsControl.getControlledDevices());
		Assert.assertEquals(start, statisticsControl.getStartDate());
	}
	
	private List<Dispositif> randomDispositifs(int numberToInsert, SetterMethod<Dispositif> setter) {
		List<Dispositif> dispositifs = new ArrayList<>();
		IntStream.range(0, numberToInsert).forEach(value -> {
			dispositifs.add(mockDispositif(value, setter));
		});
		return dispositifs;
	}
	
	private Dispositif mockDispositif(int value, SetterMethod<Dispositif> setter) {
		final Dispositif mockDispositif = new Dispositif();
		mockDispositif.setId(Integer.toUnsignedLong(value));
		mockDispositif.setType(Type.AD);
		mockDispositif.setStatus(DispositifStatus.COMPLIANT);
		setter.set(mockDispositif);
		return mockDispositif;
	}
	
	@FunctionalInterface
	interface SetterMethod<T> {
		void set(T object);
	}
}
