package fr.dreal.comfluence.repository.mock;

import fr.dreal.comfluence.business.data.DispositifFilter;
import fr.dreal.comfluence.business.entities.Information;
import fr.dreal.comfluence.repository.InformationRespository;
import org.springframework.data.domain.Sort;

import java.util.List;

public class InformationRespositoryImpl extends BaseRespositoryMock<Information, Long> implements InformationRespository {
	@Override
	public List<Information> findAllSortBy(Sort sort) {
		return null;
	}
	
	@Override
	public List<Information> findAllByIdDevice(Long id) {
		return null;
	}
	
	@Override
	public List<Information> findAllByFilter(DispositifFilter filter) {
		return null;
	}
	
	@Override
	public List<Information> findAllOpenProcByDevice(Long idDispositif) {
		return null;
	}
}
