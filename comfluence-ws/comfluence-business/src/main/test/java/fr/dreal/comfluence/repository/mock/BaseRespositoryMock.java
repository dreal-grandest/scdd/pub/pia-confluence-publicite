package fr.dreal.comfluence.repository.mock;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BaseRespositoryMock<T, K> implements JpaRepository<T, K> {
	
	protected List<T> objects = new ArrayList<>();
	
	@Override
	public List<T> findAll() {
		return null;
	}
	
	@Override
	public List<T> findAll(Sort sort) {
		return null;
	}
	
	@Override
	public List<T> findAllById(Iterable<K> iterable) {
		return null;
	}
	
	@Override
	public <S extends T> List<S> saveAll(Iterable<S> iterable) {
		iterable.forEach(this::save);
		return (List<S>) this.objects;
	}
	
	@Override
	public void flush() {
	
	}
	
	@Override
	public <S extends T> S saveAndFlush(S s) {
		return null;
	}
	
	@Override
	public void deleteInBatch(Iterable<T> iterable) {
	
	}
	
	@Override
	public void deleteAllInBatch() {
	
	}
	
	@Override
	public T getOne(K k) {
		return null;
	}
	
	@Override
	public <S extends T> List<S> findAll(Example<S> example) {
		return null;
	}
	
	@Override
	public <S extends T> List<S> findAll(Example<S> example, Sort sort) {
		return null;
	}
	
	@Override
	public Page<T> findAll(Pageable pageable) {
		return null;
	}
	
	@Override
	public <S extends T> S save(S s) {
		this.objects.add(s);
		return s;
	}
	
	@Override
	public Optional<T> findById(K k) {
		return Optional.empty();
	}
	
	@Override
	public boolean existsById(K k) {
		return false;
	}
	
	@Override
	public long count() {
		return 0;
	}
	
	@Override
	public void deleteById(K k) {
	
	}
	
	@Override
	public void delete(T t) {
	
	}
	
	@Override
	public void deleteAll(Iterable<? extends T> iterable) {
	
	}
	
	@Override
	public void deleteAll() {
		this.objects.clear();
	}
	
	@Override
	public <S extends T> Optional<S> findOne(Example<S> example) {
		return Optional.empty();
	}
	
	@Override
	public <S extends T> Page<S> findAll(Example<S> example, Pageable pageable) {
		return null;
	}
	
	@Override
	public <S extends T> long count(Example<S> example) {
		return 0;
	}
	
	@Override
	public <S extends T> boolean exists(Example<S> example) {
		return false;
	}
}
