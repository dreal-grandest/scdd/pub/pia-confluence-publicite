package fr.dreal.comfluence.business.dto.dispositif.properties;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class EnvironmentalConstraintDto {
	
	private Long id;
	
	private Long label;
}
