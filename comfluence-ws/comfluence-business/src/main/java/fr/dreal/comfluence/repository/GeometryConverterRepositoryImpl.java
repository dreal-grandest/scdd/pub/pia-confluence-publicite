package fr.dreal.comfluence.repository;

import org.geolatte.geom.Geometry;
import org.hibernate.query.NativeQuery;
import org.hibernate.spatial.GeolatteGeometryType;
import org.hibernate.spatial.dialect.postgis.PGGeometryTypeDescriptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class GeometryConverterRepositoryImpl implements GeometryConverterRepository {
	public static final String CONVERT_GEOMETRY_TO_GEOJSON = "GeometryConverterRepository_CONVERT_GEOMETRY_TO_GEOJSON";
	
	private final EntityManager entityManager;
	
	@Autowired
	public GeometryConverterRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public String convertGeometryToGeoJson(Long idControl) {
		Query query = entityManager.createNamedQuery(GeometryConverterRepositoryImpl.CONVERT_GEOMETRY_TO_GEOJSON);
		query.setParameter("idControl", idControl);
		return (String) query.getSingleResult();
	}
	
	@Override
	public Geometry convertGeoJsonToGeometry(List<String> geoJsonParts) {
		if(geoJsonParts == null || geoJsonParts.isEmpty()) {
			return null;
		}
		String queryString = "(SELECT st_makevalid(ST_Multi(ST_Union(f.the_geom))) as geometry FROM ( select ST_GeomFromGeoJSON(UNNEST( ARRAY [:geoJsons] )) as the_geom, 1 as groupingAll ) As f GROUP BY groupingAll);";
		
		StringBuilder geoJsonsStringBuilder = new StringBuilder();
		
		geoJsonParts.forEach(geometry -> {
			if(geoJsonsStringBuilder.length() != 0){
				geoJsonsStringBuilder.append(',');
			}
			geoJsonsStringBuilder.append('\'').append(geometry).append('\'');
		});
		
		Query query = entityManager.createNativeQuery(queryString.replace(":geoJsons", geoJsonsStringBuilder))
				.unwrap(NativeQuery.class)
				.addScalar("geometry", new GeolatteGeometryType(PGGeometryTypeDescriptor.INSTANCE));
		return (Geometry) query.getSingleResult();
	}
}
