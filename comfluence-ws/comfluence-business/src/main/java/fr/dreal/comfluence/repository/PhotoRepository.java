package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.data.dispositif.PhotoType;
import fr.dreal.comfluence.business.entities.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository pour les objets métier "Photo"
 */
@Repository
public interface PhotoRepository extends JpaRepository<Photo, Long> {
    List<Photo> findAllByIdDispositifAndIsArchivedIsFalse(Long id);
    List<Photo> findAllByIdDispositifAndPhotoTypeAndIsArchivedIsFalse(Long id, PhotoType photoType);
}
