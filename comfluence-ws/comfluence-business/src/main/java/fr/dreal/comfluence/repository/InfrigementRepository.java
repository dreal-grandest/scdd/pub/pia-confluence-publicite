package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.entities.dispositif.properties.Infrigement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InfrigementRepository extends JpaRepository<Infrigement, Long> {

}
