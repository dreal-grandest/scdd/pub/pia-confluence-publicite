package fr.dreal.comfluence.business.data;

public enum ChangeType {
	MODIFIED,
	ADDED,
    REMOVED,
	SUSPENDED_INFRIGEMENT
}
