package fr.dreal.comfluence.business.entities;

import fr.dreal.comfluence.repository.CityRepositoryImpl;
import javax.persistence.Column;
import lombok.Getter;
import lombok.Setter;
import org.geolatte.geom.Geometry;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
@Entity(name = "commune")
@Getter @Setter
@NamedNativeQuery(name = CityRepositoryImpl.INTERSECTION_GEOMETRY_CITIES,
		query= "select c.* from commune c where not ST_IsEmpty(ST_Intersection(" +
                "ST_Transform(ST_SetSRID(:geometryParam, 4326),2154), " +
                "ST_SetSRID(c.geom,2154))" +
                ")")
public class City {
	@Id
    @Column(name = "gid")
	private Long id;

	@Column(name = "nom_com")
	private String name;

	@Column(name = "geom")
	private Geometry geometry;
}
