package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.data.DispositifFilter;
import fr.dreal.comfluence.business.data.dispositif.DispositifStatus;
import fr.dreal.comfluence.business.data.dispositif.Location;
import fr.dreal.comfluence.business.data.dispositif.Type;
import fr.dreal.comfluence.business.entities.Information;
import fr.dreal.comfluence.business.entities.dispositif.Dispositif;
import fr.dreal.comfluence.repository.InformationRespository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

/**
 * @author SopraGroup
 */
@Service
public class FilterService {
	private InformationRespository informationRespository;
	
	public FilterService(InformationRespository informationRespository) {
		this.informationRespository = informationRespository;
	}
	
	public Set<DispositifFilter> retrieveAllFiltersForDisp(Dispositif dispositif) {
		List<Information> procedureList = informationRespository.findAllOpenProcByDevice(dispositif.getId());
		Set<DispositifFilter> filters = new HashSet<>();
		if (procedureList != null) {
			for (Information information : procedureList) {
				/** Ajoute les tags par type de procédure **/
				final Collection<DispositifFilter> dispositifFilters = information.getProcedure()
						.getDispositifFilters();
				if (dispositifFilters != null && !dispositif.isRemoved()) {
					filters.addAll(dispositifFilters);
				}
				/** Ajoute le Tag "hors délai" si une procédure est dépassée **/
				if (information.getProcedure().getDelay() != null && !filters.contains(DispositifFilter.HORS_DELAI)) {
					LocalDate limitDay = LocalDate.now().plus(-information.getProcedure().getDelay(), ChronoUnit.DAYS);
					if (information.getAddedDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore(
							limitDay)) {
						filters.add(DispositifFilter.HORS_DELAI);
					}
				}
				
			}
		}
		filterDefinitions.forEach(filterDefinition -> {
			if (filterDefinition.isAppliedTo(dispositif)) {
				filters.add(filterDefinition.getFilter());
			}
		});
		return filters;
	}
	
	private static List<FilterDefinition> filterDefinitions = new ArrayList<>();
	
	static {
		filterDefinitions.add(new FilterDefinition<>(
				(dispositif) -> dispositif.getContext() != null ? dispositif.getContext().getLocation() : null,
				Location.AGGLOMERATION, DispositifFilter.AGGLOMERATION));
		filterDefinitions.add(new FilterDefinition<>(
				(dispositif) -> dispositif.getContext() != null ? dispositif.getContext().getLocation() : null,
				Location.COUNTRYSIDE, DispositifFilter.COUNTRYSIDE));
		filterDefinitions.add(new FilterDefinition<>(
				(dispositif) -> (dispositif.getContext() != null && dispositif.getContext().getEnvironmentalConstraints() != null) ?
						!dispositif.getContext().getEnvironmentalConstraints().isEmpty() : null,
				Location.AGGLOMERATION, DispositifFilter.WITH_ENVIRONMENTAL_CONSTRAINT));
		filterDefinitions.add(
				new FilterDefinition<>(Dispositif::getStatus, DispositifStatus.TODO, DispositifFilter.TO_DETERMINE));
		filterDefinitions.add(
				new FilterDefinition<>(Dispositif::getStatus, DispositifStatus.COMPLIANT, DispositifFilter.CONFORME));
		filterDefinitions.add(new FilterDefinition<>(Dispositif::getStatus, DispositifStatus.NONCOMPLIANT,
				DispositifFilter.NON_CONFORME));
		filterDefinitions.add(new FilterDefinition<>(
				dispositif -> dispositif.getInfrigements() != null && !dispositif.getInfrigements().isEmpty(),
				Boolean.TRUE, DispositifFilter.WITH_INFRIGEMENT));
		filterDefinitions.add(
				new FilterDefinition<>(Dispositif::isRemoved, Boolean.TRUE, DispositifFilter.DISPOSITIF_REMOVED));
		filterDefinitions.add(
				new FilterDefinition<>(Dispositif::getType, Type.AD, DispositifFilter.ADVERTISEMENT));
		filterDefinitions.add(
				new FilterDefinition<>(Dispositif::getType, Type.SIGNBOARD, DispositifFilter.SIGNBOARD));
		filterDefinitions.add(
				new FilterDefinition<>(Dispositif::getType, Type.PRESIGNBOARD, DispositifFilter.PRESIGNBOARD));
	}
	
	private static class FilterDefinition<R> {
		
		private Function<Dispositif, R> supplier;
		private R expectedValue;
		private DispositifFilter filter;
		
		public FilterDefinition(Function<Dispositif, R> supplier, R expectedResult, DispositifFilter filter) {
			this.supplier = supplier;
			this.expectedValue = expectedResult;
			this.filter = filter;
		}
		
		public boolean isAppliedTo(Dispositif dispositif) {
			return expectedValue.equals(supplier.apply(dispositif));
		}
		
		public DispositifFilter getFilter() {
			return filter;
		}
	}
}
