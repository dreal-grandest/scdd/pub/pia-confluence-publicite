package fr.dreal.comfluence.business.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
@Entity
@Getter @Setter
public class StatisticsControl {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	/**
	 * devices controlled by the controller during the control of the day
	 */
	private Integer controlledDevices;
	
	/**
	 * in km
	 */
	private Double traveledDistance;
	
	/**
	 * in minutes
	 */
	private Long duration;
	
	/**
	 * start of the control (timestamp of 1st device creation)
	 */
	private Date startDate;
}
