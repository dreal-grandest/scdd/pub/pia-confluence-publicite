package fr.dreal.comfluence.business.data;

/**
 * @author SopraGroup
 */
public enum ProcedureGroupe {
    GROUPEMENT_A(0, "Groupement A",-1),
    GROUPEMENT_B(1, "Groupement B",0),
    GROUPEMENT_C(2, "Groupement C",1),
    GROUPEMENT_D(3, "Groupement D",0),
    GROUPEMENT_E(4, "Groupement E",0),
    ;
    private String name;
    private int id;
    private int previousId;
    ProcedureGroupe(int id, String name, int previousId) {
        this.id = id;
        this.name = name;
        this.previousId = previousId;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getPreviousId() {
        return previousId;
    }
}
