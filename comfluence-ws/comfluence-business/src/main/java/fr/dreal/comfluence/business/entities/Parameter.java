package fr.dreal.comfluence.business.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author SopraGroup
 */
@Entity
@Getter @Setter
public class Parameter {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private long id;
    @Column(nullable = false, length = 128, unique = true)
    private String name;
    @Column(length = 255)
    private String description;
    private Integer intValue;
    private Long longValue;
    @Column(length = 512)
    private String stringValue;
    private LocalDate dateValue;
    private LocalDateTime dateTimeValue;
    private boolean booleanValue;
    private Double doubleValue;
}
