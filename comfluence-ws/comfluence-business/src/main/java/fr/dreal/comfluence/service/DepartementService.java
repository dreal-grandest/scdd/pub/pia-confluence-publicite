package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.dto.DepartementDto;
import fr.dreal.comfluence.business.dto.FullDepartementDto;
import fr.dreal.comfluence.business.dto.TerritorialDirectionDto;
import fr.dreal.comfluence.business.entities.Departement;
import fr.dreal.comfluence.business.entities.TerritorialDirection;
import fr.dreal.comfluence.mapper.DepartementMapper;
import fr.dreal.comfluence.mapper.FullDepartementMapper;
import fr.dreal.comfluence.mapper.TerritorialDirectionMapper;
import fr.dreal.comfluence.repository.DepartementRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartementService {
	
	private final DepartementRepository departementRepository;
	
	public DepartementService(DepartementRepository departementRepository) {
		this.departementRepository = departementRepository;
	}
	
	public List<DepartementDto> findAll() {
		return this.departementRepository.findAll().stream().map(DepartementMapper.INSTANCE::entityToDto).collect(
				Collectors.toList());
	}

    public List<FullDepartementDto> findAllFull() {
        return this.departementRepository.findAll().stream().map(FullDepartementMapper.INSTANCE::entityToDto).collect(
                Collectors.toList());
    }
	
	public DepartementDto getOne(Long codeDepartement) {
		return DepartementMapper.INSTANCE.entityToDto(this.departementRepository.getOne(codeDepartement));
	}
	
	public TerritorialDirectionDto getTerritorialDirection(Long departementCode) {
		final TerritorialDirection territorialDirection = this.departementRepository.getOneWithTerritorialDirection(
				departementCode).getTerritorialDirection();
		return TerritorialDirectionMapper.INSTANCE.entityToDto(territorialDirection);
	}
	
	public DepartementDto save(DepartementDto departementDto) {
        Departement departement = this.departementRepository.save(DepartementMapper.INSTANCE.dtoToEntity(departementDto));
        return DepartementMapper.INSTANCE.entityToDto(departement);
    }

    public FullDepartementDto saveFull(FullDepartementDto departementDto) {
        Departement departement = this.departementRepository.save(FullDepartementMapper.INSTANCE.dtoToEntity(departementDto));
        return FullDepartementMapper.INSTANCE.entityToDto(departement);
    }
}
