package fr.dreal.comfluence.business.entities.dispositif.properties;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class Coordinates {

    /**
     * identifiant de base de données
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter @Setter
    private Long id;
    
    /**
     * range(-90, +90)
     */
    @Getter @Setter
    @Column(precision = 17, scale = 15)
    private Double latitude;
    
    /**
     * range(-180, +180)
     */
    @Getter @Setter
    @Column(precision = 18, scale = 15)
    private Double longitude;
}
