package fr.dreal.comfluence.business.dto.dispositif;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

/**
 * DTO représentant les informations numériques sur les dispositifs
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class StatisticsDispositifsDTO {
	
	/**
	 * nombre de dispositifs conformes
	 **/
	private Long compliantDevices;
	
	/**
	 * nombre de dispositifs non conformes
	 **/
	private Long nonCompliantDevices;
	
	/**
	 * nombre de dispositifs à définir
	 **/
	private Long todoDevices;
	
	/**
	 * nombre de dispositifs de type AD
	 **/
	private Long adDevices;
	
	/**
	 * nombre de dispositifs de type SIGNBOARD
	 **/
	private Long signboardDevices;
	
	/**
	 * nombre de dispositifs de type PRESIGNBOARD
	 **/
	private Long preSignboardDevices;
	
	/**
	 * date de mise à jour des nombres de dispositifs conformes,
	 * non conformes, à définir
	 **/
	private LocalDate lastCall;

    /**
     * Identifiant du département
     */
	private Long departementId;
}
