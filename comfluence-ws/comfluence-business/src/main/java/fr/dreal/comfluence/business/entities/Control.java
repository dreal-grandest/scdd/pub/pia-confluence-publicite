package fr.dreal.comfluence.business.entities;

import fr.dreal.comfluence.business.entities.dispositif.Dispositif;
import fr.dreal.comfluence.repository.GeometryConverterRepositoryImpl;
import lombok.Getter;
import lombok.Setter;
import org.geolatte.geom.Geometry;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;


@Entity
@Getter
@Setter
@NamedNativeQuery(name = GeometryConverterRepositoryImpl.CONVERT_GEOMETRY_TO_GEOJSON,
		query = "(select row_to_json(fc) :::: text from ( " +
					"select 'FeatureCollection' as \"type\", " +
					"array_to_json(array_agg(f)) as \"features\" from ( " +
						"select 'Feature' as \"type\", " +
						"ST_AsGeoJSON((ST_Dump(geometryT.geometry)).geom) :::: json as \"geometry\", " +
						"null as \"properties\" " +
						"from ( " +
							"select co.geometry from \"control\" co where id = :idControl " +
						") as geometryT " +
					") as f " +
				") as fc);"
)
public class Control {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	private User controller;
	
	@ManyToOne
	private User attendant;
	
	@ManyToOne
	private Departement departement;
	
	private Geometry geometry;
	
	private LocalDate controlDate;
	
	@ManyToMany
	private Collection<Dispositif> dispositifs;
	
	@OneToOne(orphanRemoval = true, cascade = CascadeType.ALL)
	private StatisticsControl statisticsControl;
	
	@ManyToMany
	private Collection<City> cities;
}
