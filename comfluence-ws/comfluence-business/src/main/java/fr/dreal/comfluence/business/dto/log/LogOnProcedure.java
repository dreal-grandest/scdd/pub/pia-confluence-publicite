package fr.dreal.comfluence.business.dto.log;

import lombok.Getter;

/**
 * @author SopraGroup
 */
@Getter
public class LogOnProcedure extends LogOnDispositif implements LoggableAction {
    private Long idProcedure;
    public LogOnProcedure(String utilisateur, Type type, String action, Long idDevice, Long idProcedure) {
        super(utilisateur, type, action, idDevice);
        this.idProcedure = idProcedure;
    }

    @Override
    protected StringBuilder getStrBuilder() {
        return super.getStrBuilder().append(" - procédure[").append(idProcedure).append(']');
    }

    @Override
    public String toString() {
        return getStrBuilder().toString();
    }
}
