package fr.dreal.comfluence.business.dto.dispositif;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * DTO représentant les informations numériques sur les procédures
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StatisticsProceduresDTO {

    @Getter @Setter
    private String label;
    @Getter @Setter
    private Integer quantity = 0;
    @Getter @Setter
    private List<Long> filterId;
}
