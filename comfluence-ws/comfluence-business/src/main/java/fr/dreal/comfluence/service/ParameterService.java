package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.data.DispositifFilter;
import fr.dreal.comfluence.business.entities.Parameter;
import fr.dreal.comfluence.repository.ParameterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

/**
 * @author SopraGroup
 */
@Service
public class ParameterService {
    public static final String MAX_TIME_FOR_OK_DEVICE = "maxTimeForOkDevice";
    public static final String MAX_TIME_FOR_OK_DEVICE_TYPE = "maxTimeForOkDeviceType";

    private static final Logger LOGGER = LoggerFactory.getLogger(ParameterService.class);
    private final ParameterRepository parameterRepository;

    public ParameterService(ParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }

    @Cacheable(value = CacheServiceForService.PARAMCACHE_NAME_MAX_TIME_FOR_OK_DEVICE)
    public Date findMaxOkDeviceTime() {
        Calendar calendar = Calendar.getInstance();
        Optional<Parameter> maxTimeForOkDevice = parameterRepository.findFirstByName(MAX_TIME_FOR_OK_DEVICE);
        Optional<Parameter> maxTimeForOkDeviceType = parameterRepository.findFirstByName(MAX_TIME_FOR_OK_DEVICE_TYPE);
        if(maxTimeForOkDevice.isPresent() && maxTimeForOkDeviceType.isPresent()) {
            calendar.add(maxTimeForOkDeviceType.get().getIntValue(), -maxTimeForOkDevice.get().getIntValue());
        } else {
            calendar.setTimeInMillis(0L);
        }
        return calendar.getTime();
    }

    @Cacheable(value = CacheServiceForService.PARAM_FILTER_CACHE_NAME)
    public String fillNameOfParametrizedFilter(DispositifFilter dispositifFilter) {
        String result = dispositifFilter.getLabel();
        if(DispositifFilter.SHOW_ALL == dispositifFilter) {
            Optional<Parameter> maxTimeForOkDevice = parameterRepository.findFirstByName(MAX_TIME_FOR_OK_DEVICE);
            Optional<Parameter> maxTimeForOkDeviceType = parameterRepository.findFirstByName(MAX_TIME_FOR_OK_DEVICE_TYPE);
            if (maxTimeForOkDevice.isPresent() && maxTimeForOkDeviceType.isPresent()) {
                result = String.format(dispositifFilter.getLabel(), maxTimeForOkDevice.get().getIntValue(),
                        convertTypeDate(maxTimeForOkDeviceType.get().getIntValue()));
            }
        }
        return result;
    }

    private String convertTypeDate(int typeDate) {
        String result;
        switch (typeDate) {
            case Calendar.YEAR:
                result = "année(s)";
                break;
            case Calendar.MONTH:
                result = "mois";
                break;
            case Calendar.DATE:
                result = "jour(s)";
                break;
            case Calendar.HOUR:
                result = "heure(s)";
                break;
            default:
                result = "???";
                break;
        }
        return result;
    }
}
