package fr.dreal.comfluence.business.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

/**
 * @author SopraGroup
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class ControlDto {
    private String controllerMail;
    private String attendantMail;
    private LocalDate controlDate;
	private Long id;
	private String geoJson;
	private List<String> geoJsonParts;
	private StatisticsControlDto statistics;
	private List<CityDto> cities;
}
