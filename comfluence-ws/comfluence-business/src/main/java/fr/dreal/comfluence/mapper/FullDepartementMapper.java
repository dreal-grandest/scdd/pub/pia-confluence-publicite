package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.FullDepartementDto;
import fr.dreal.comfluence.business.entities.Departement;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;


/**
 * Mapper entre les différents objets concernant les tâches.
 */
@Mapper(uses = {TerritorialDirectionMapper.class, DepartementMapper.class})
public abstract class FullDepartementMapper {
	
	public static final FullDepartementMapper INSTANCE = Mappers.getMapper(FullDepartementMapper.class);
	
	public abstract FullDepartementDto entityToDto(Departement departement);

    @Mappings({
            @Mapping(source = "box.minLat", target = "boundYMin"),
            @Mapping(source = "box.maxLat", target = "boundYMax"),
            @Mapping(source = "box.minLong", target = "boundXMin"),
            @Mapping(source = "box.maxLong", target = "boundXMax"),
    })
	public abstract Departement dtoToEntity(FullDepartementDto departementDto);
}

