package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.entities.City;
import org.geolatte.geom.Geometry;

import java.util.List;

public interface CityRepositoryCustom {
	List<City> findAllCityIntersecting(Geometry geometry);
}
