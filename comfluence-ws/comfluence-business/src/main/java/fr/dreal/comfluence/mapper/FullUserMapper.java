package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.FullUserDto;
import fr.dreal.comfluence.business.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


/**
 * Mapper entre les différents objets concernant les tâches.
 */
@Mapper(uses = {DepartementMapper.class, RoleMapper.class})
public abstract class FullUserMapper {
	
	public static final FullUserMapper INSTANCE = Mappers.getMapper(FullUserMapper.class);

	public abstract FullUserDto entityToDto(User user);
	
	public abstract User dtoToEntity(FullUserDto userDto);
}

