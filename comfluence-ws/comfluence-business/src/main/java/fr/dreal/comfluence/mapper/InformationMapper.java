package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.data.InformationType;
import fr.dreal.comfluence.business.dto.InformationDto;
import fr.dreal.comfluence.business.entities.Information;
import fr.dreal.comfluence.business.entities.Procedure;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;


/**
 * Mapper entre les différents objets concernant les tâches.
 */
@Mapper(uses = {DispositifFilterMapper.class})
public abstract class InformationMapper {

    public static final InformationMapper INSTANCE = Mappers.getMapper( InformationMapper.class );

    @Mapping(target = "procedure.incompatibleProcedures", ignore = true)
    @Mapping(source = "dispositif.id", target = "idDevice")
    @Mapping(source = "path", target = "hasDoc", qualifiedByName = "transformHasDoc")
    @Mapping(target = "comment", qualifiedByName = "transformComment", source = "")
    @Mapping(source = "procedure", target = "type", qualifiedByName = "transformType")
    public abstract InformationDto entityToDto(Information information);

    @Named("transformHasDoc")
    protected Boolean transformHasDoc(String path) {
        return StringUtils.isNotBlank(path);
    }
    @AfterMapping
    protected void transformComment(@MappingTarget InformationDto informationDto, Information information) {
        String result = null;
        if(information != null) {
            if(information.getProcedure() != null) {
                result = information.getProcedure().getLabel();
            } else {
                result = information.getComment();
            }
        }
        informationDto.setComment(result);
    }
    @Named("transformType")
    protected InformationType transformType(Procedure procedure) {
        InformationType informationType = InformationType.COMMENT;
        if(procedure != null) {
            informationType = InformationType.PROCEDURE;
        }
        return informationType;
    }
    
    public abstract Information dtoToEntity(InformationDto information);
    
}

