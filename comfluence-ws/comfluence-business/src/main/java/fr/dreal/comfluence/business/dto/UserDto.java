package fr.dreal.comfluence.business.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;
import java.util.Set;

/**
 * @author SopraGroup
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class UserDto {
    private String firstName;
    private String lastName;
    private String mail;
    private DepartementDto departement;
    private Set<RoleDto> roles;
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDto userDto = (UserDto) o;
        return Objects.equals(mail, userDto.mail);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(mail);
    }
}
