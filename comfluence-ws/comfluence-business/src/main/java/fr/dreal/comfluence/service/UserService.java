package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.dto.FullUserDto;
import fr.dreal.comfluence.business.dto.UserDto;
import fr.dreal.comfluence.business.entities.Departement;
import fr.dreal.comfluence.business.entities.User;
import fr.dreal.comfluence.mapper.FullUserMapper;
import fr.dreal.comfluence.mapper.RoleMapper;
import fr.dreal.comfluence.mapper.UserMapper;
import fr.dreal.comfluence.repository.DepartementRepository;
import fr.dreal.comfluence.repository.RoleRepository;
import fr.dreal.comfluence.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author SopraGroup
 */
@Service
public class UserService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
	
	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final DepartementRepository departementRepository;
	private final CacheServiceForService cacheServiceForService;
	private final RoleRepository roleRepository;
	
	public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder,
			DepartementRepository departementRepository, CacheServiceForService cacheServiceForService,
                        RoleRepository roleRepository) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.departementRepository = departementRepository;
		this.cacheServiceForService = cacheServiceForService;
		this.roleRepository = roleRepository;
	}
	
	public List<UserDto> getAllUsers() {
		List<User> userList = userRepository.findAllByEnabledTrueAndDeletedIsFalse();
		List<UserDto> result;
		if (userList != null) {
			result = userList.stream().map(UserMapper.INSTANCE::entityToDto).collect(Collectors.toList());
		} else {
			result = new ArrayList<>();
		}
		return result;
	}

    public List<FullUserDto> getAllFullUsers(Boolean showDisabled) {
        List<User> userList;
        if(showDisabled == null || !showDisabled) {
            userList = userRepository.findAllByEnabledTrueAndDeletedIsFalse();
        } else {
            userList = userRepository.findAllByDeletedIsFalse();
        }
        List<FullUserDto> result;
        if (userList != null) {
            result = userList.stream().map(FullUserMapper.INSTANCE::entityToDto).collect(Collectors.toList());
        } else {
            result = new ArrayList<>();
        }
        return result;
    }
	
	public FullUserDto addUser(String firstName, String lastName, String email, Long codeDep, String pwd, List<Long> roles) {
        FullUserDto result = null;
		try {
			if (userRepository.countByMailAndDeletedIsFalse(email) <= 0) {
			    Optional<Departement> departement = departementRepository.findById(codeDep);
			    if(departement.isPresent()) {
                    User newUser = new User();
                    newUser.setDepartement(departement.get());
                    newUser.setPassword(passwordEncoder.encode(pwd));
                    newUser.setEnabled(true);
                    newUser.setFirstName(firstName);
                    newUser.setLastName(lastName);
                    newUser.setMail(email.trim().toLowerCase());
                    if(roles != null) {
                        newUser.setRoles(roleRepository.searchRoles(roles));
                    }
                    newUser = userRepository.save(newUser);
                    result = FullUserMapper.INSTANCE.entityToDto(newUser);
                }
			}
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage(), e);
		}
		return result;
	}

	public boolean enableUser(long id, boolean enable) {
		boolean result = false;
		try {
			userRepository.updateEnable(id, enable);
			result = true;
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage(), e);
		}
		return result;
	}
	
	public boolean updatePwd(Long id, String newPwd) {
		boolean result = false;
		try {
			userRepository.updatePwd(id, newPwd);
			result = true;
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage(), e);
		}
		return result;
	}
	
	public boolean updateUser(long id, FullUserDto newUser) {
		boolean result = false;
		try {
		    String mail = newUser.getMail().trim().toLowerCase();
            Optional<User> userToUpdate = userRepository.findById(id);

            if(userToUpdate.isPresent()) {
                long nbMailUsed = userRepository.countByMailAndDeletedIsFalse(mail);
                User user = userToUpdate.get();
                if (nbMailUsed == 0 || (user.getMail().equals(mail))) {
                    user.setMail(newUser.getMail().trim().toLowerCase());
                    user.setLastName(newUser.getLastName());
                    user.setFirstName(newUser.getFirstName());
                    if(newUser.getDepartement() != null && newUser.getDepartement().getCode() != user.getDepartement().getCode()) {
                        Optional<Departement> findNewDep = departementRepository.findById(newUser.getDepartement().getCode());
                        if(findNewDep.isPresent()) {
                            user.setDepartement(findNewDep.get());
                        }
                    }
                    if(newUser.getRoles() != null) {
                        newUser.setRoles(newUser.getRoles().stream().filter(roleDto -> roleDto != null).collect(Collectors.toSet()));
                    }
                    revokeTokenIfNeeded(newUser, user);
                    if(newUser.getRoles() != null) {
                        user.setRoles(newUser.getRoles().stream().map(RoleMapper.INSTANCE::dtoToEntity).collect(Collectors.toSet()));
                    } else {
                        user.setRoles(null);
                    }
                    userRepository.save(user);
                    result = true;

                } else {
                    LOGGER.error("Il y a plus d'un utilisateur avec l'email {}", mail);
                }
            } else {
                LOGGER.error("Il n'y a aucun utilisateur actif avec l'email {}", mail);
            }
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage(), e);
		}
		return result;
	}

	private void revokeTokenIfNeeded(FullUserDto newUser, User oldUser) {
	    boolean needToRevoke = false;
	    if(newUser.getRoles() == null ||
                (oldUser.getRoles() != null && oldUser.getRoles().size() != newUser.getRoles().size())) {
	        needToRevoke = true;
        }

	    if(needToRevoke) {
	        revokeToken(oldUser.getId());
        }
    }
	public UserDto associateDepartement(String email, Long departementCode) {
		UserDto userDto = null;
		User user = userRepository.findFirstByMailAndEnabledTrue(email);
		if (user != null) {
			Optional<Departement> departement = departementRepository.findById(departementCode);
			if (departement.isPresent()) {
				user.setDepartement(departement.get());
				userDto = UserMapper.INSTANCE.entityToDto(userRepository.save(user));
			}
		}
		return userDto;
	}
	
	public List<UserDto> findAllByDepartment(Long departmentCode) {
		List<User> users = userRepository.findAllByDepartementCodeAndDeletedIsFalse(departmentCode);
		if (users == null || users.isEmpty()) {
			return Collections.emptyList();
		}
		return users.stream()
				.map(UserMapper.INSTANCE::entityToDto)
				.collect(Collectors.toList());
	}
	
	public UserDto findByMail(String mail) {
		Optional<User> user = userRepository.findByMailAndDeletedIsFalse(mail);
		return UserMapper.INSTANCE.entityToDto(user.orElseThrow());
	}


	public void deleteUser(long id) {
	    revokeToken(id);
	    userRepository.delete(id);
    }

    public List<String> revokeToken(long id) {
	    List<String> result = new ArrayList<>();
        Optional<User> user = this.userRepository.findById(id);
        if(user.isPresent()) {
            User userEntity = user.get();
            if(userEntity.getToken() != null) {
                for(String token: userEntity.getToken()) {
                    cacheServiceForService.clearAllTokenCache(token);
                }
            }
            result.addAll(userEntity.getToken());
            userEntity.setToken(null);
            userRepository.save(userEntity);
        }
        return result;
    }

    public void storeTokenWithMail(String mail, String token) {
        Optional<User> user = this.userRepository.findByMailAndDeletedIsFalse(mail);
        if(user.isPresent()) {
            storeToken(user.get(), token);
        }
    }
    public void storeToken(long id, String token) {
        Optional<User> user = this.userRepository.findById(id);
        if(user.isPresent()) {
            storeToken(user.get(), token);
        }
    }

    private void storeToken(User userEntity, String token) {
        if(userEntity.getToken() == null) {
            userEntity.setToken(new ArrayList<>());
        }
        if(!userEntity.getToken().contains(token)) {
            userEntity.getToken().add(token);
            userRepository.save(userEntity);
        }
    }
    @Cacheable(CacheServiceForService.TOKEN_CACHE_NAME)
    public boolean isTokenValid(String token) {
	    Long nbTokenFound = userRepository.countByAccessToken(token);
	    boolean result;
	    if(nbTokenFound != null && nbTokenFound > 0) {
	        result = true;
        } else {
	        result = false;
        }
	    return result;
    }
}
