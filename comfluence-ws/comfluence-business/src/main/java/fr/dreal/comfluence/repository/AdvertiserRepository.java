package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.entities.Advertiser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AdvertiserRepository extends JpaRepository<Advertiser, Long> {
	
	List<Advertiser> findAllByDepartements_Code(Long code);
	
	Optional<Advertiser> findByNameIgnoreCase(String name);
}
