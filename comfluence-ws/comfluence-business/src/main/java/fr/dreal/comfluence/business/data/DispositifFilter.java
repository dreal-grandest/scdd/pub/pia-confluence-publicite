package fr.dreal.comfluence.business.data;

import lombok.Getter;

/**
 * @author SopraGroup
 */
@Getter
public enum DispositifFilter {
	AMIABLE(0L, "Phase préalable amiable", false),
	A_ADM(1L, "Amendes administratives", false),
	PV(2L, "Procès verbaux", false),
	HORS_DELAI(3L, "Hors délai", false),
	S_ADM(4L, "Sanction ADM", false),
	SIGNAL(5L, "Signalement", false),
	CONFORME(6L, "Conforme", false),
	NON_CONFORME(7L, "Non Conforme", false),
	TO_DETERMINE(8L, "A déterminer", false),
	SIGNBOARD(9L, "Enseigne", false),
	PRESIGNBOARD(10L, "Préenseigne", false),
	ADVERTISEMENT(11L, "Publicité", false),
	AGGLOMERATION(12L, "En Agglomération", false),
	COUNTRYSIDE(13L, "Hors Agglomération", false),
	DISPOSITIF_REMOVED(15L, "Déposé", false),
	WITH_INFRIGEMENT(16L, "Avec infraction(s)", false),
	WITH_ENVIRONMENTAL_CONSTRAINT(17L, "Avec contrainte(s) environnementale(s)", false),
	SHOW_ALL(18L, "Voir tous les dispositifs conforme de plus %d %s", true),
    FORMAL_NOTICE(19L, "Arrêté(s) de mise en demeure", false),
    PRIOR_ADMINISTRATIVE_FINE(20L, "Phase(s) contradictoire(s) préalable(s) à l’amende administrative", false),
    PENALTY(21L, "Arrêté(s) d’astreinte", false);
    
    DispositifFilter(Long id, String label, boolean needReload) {
		this.id = id;
		this.label = label;
		this.needReload = needReload;
	}
	
	private Long id;
	
	private String label;
	
	private boolean needReload;
}
