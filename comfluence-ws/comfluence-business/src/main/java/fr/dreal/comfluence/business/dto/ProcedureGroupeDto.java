package fr.dreal.comfluence.business.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author SopraGroup
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
public class ProcedureGroupeDto {
    private int id;
    private String name;
    private int previousId;
}
