package fr.dreal.comfluence.business.data.dispositif;

public enum Location {
	AGGLOMERATION,
	COUNTRYSIDE,
	UNDEFINED_LOCATION
}
