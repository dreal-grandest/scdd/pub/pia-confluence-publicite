package fr.dreal.comfluence.business.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class TerritorialDirectionDto {
    private Long id;
    
	private String prefecture;
	
	private String service;
	
	private String pole;
	
	private String city;
	
	private String director;
	
	private String direction;
	
	private String phoneNumber;
	
	private String faxNumber;
	
	private String address;
	
	private String signingAuthority;
	
	private String firstNameSigningAuthority;
	
	private String lastNameSigningAuthority;
}
