package fr.dreal.comfluence.repository;

import org.geolatte.geom.Geometry;

import java.util.List;

public interface GeometryConverterRepository {
	
	String convertGeometryToGeoJson(Long idControl);
	
	Geometry convertGeoJsonToGeometry(List<String> geoJsonParts);
}
