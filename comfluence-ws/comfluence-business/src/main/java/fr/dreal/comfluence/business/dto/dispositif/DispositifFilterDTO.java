package fr.dreal.comfluence.business.dto.dispositif;

import lombok.Getter;
import lombok.Setter;

/**
 * Filtres Disponibles pour les dispositifs
 *
 * @author SopraGroup
 */
public class DispositifFilterDTO {
    @Getter @Setter
    private Long id;
    @Getter @Setter
    private String label;
    @Getter @Setter
    private boolean needReload;
}
