package fr.dreal.comfluence.business.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * @author SopraGroup
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class FullUserDto extends UserDto {
    private Long id;
    private boolean enabled;
}
