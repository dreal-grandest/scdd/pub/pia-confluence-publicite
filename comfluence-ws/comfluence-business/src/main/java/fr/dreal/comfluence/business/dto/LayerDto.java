package fr.dreal.comfluence.business.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LayerDto {
	
	@Getter @Setter
	private Long id;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private String url;
	
	@Getter @Setter
	private ParametersDto parameters;
	@Getter @Setter
    private Boolean needProxy;
}
