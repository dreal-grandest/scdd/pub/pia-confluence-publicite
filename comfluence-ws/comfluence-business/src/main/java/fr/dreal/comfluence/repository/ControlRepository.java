package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.data.dispositif.DispositifStatus;
import fr.dreal.comfluence.business.entities.Control;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ControlRepository extends PagingAndSortingRepository<Control, Long> {
	
	List<Control> findAllByDepartementCodeAndControlDateBefore(Long departmentCode, LocalDate controlDate, Pageable pageable);
	List<Control> findAllByDepartementCodeAndControlDateBefore(Long departmentCode, LocalDate controlDate, Sort sort);
	List<Control> findAllByDepartementCode(Long departmentCode, Pageable pageable);
	List<Control> findAllByDepartementCode(Long departmentCode);
	
	List<Control> findByControllerMailAndControlDateEquals(String controllerMail, LocalDate controlDate);
	
	List<Control> findAllByDispositifsId(Long id);

	List<Control> findAllByDepartementCodeAndControlDateBetweenAndDispositifs_StatusNot(Long departementCode, LocalDate localDateMin,
			LocalDate localDateMax, DispositifStatus status);
}
