package fr.dreal.comfluence.business.dto;

import fr.dreal.comfluence.business.dto.dispositif.DispositifFilterDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;


public class ProcedureDto {
	
	@Getter @Setter
	private Long id;
	
	@Getter @Setter
	private String label;
	
	@Getter @Setter
	private Integer delay;
	
	@Getter @Setter
	private Collection<ProcedureDto> incompatibleProcedures;
	
	@Getter @Setter
	private Collection<DispositifFilterDTO> dispositifFilters;

	@Getter @Setter
	private int groupeId;
}
