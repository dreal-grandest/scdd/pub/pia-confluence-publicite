package fr.dreal.comfluence.business.dto.dispositif.properties;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.dreal.comfluence.business.data.dispositif.Category;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class SizeDTO {

    private Long id;

    private Double height;

    private Double width;

    private Category category;
    
    private Double deviceGroundDistance;
    
    private Double mastWidth;
    
    private Double protrusionGroundDistance;
}
