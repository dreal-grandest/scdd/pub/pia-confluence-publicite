package fr.dreal.comfluence.business.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author SopraGroup
 */
@Getter
@Setter
public class DepartementDto {
    private Long code;
    private String name;
    private Box box;
}
