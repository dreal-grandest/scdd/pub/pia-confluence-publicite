package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.photo.PhotoDto;
import fr.dreal.comfluence.business.entities.Photo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class PhotoMapper {

    public static final PhotoMapper INSTANCE = Mappers.getMapper(PhotoMapper.class);

    public abstract PhotoDto entityToDto(Photo dispositif);
}
