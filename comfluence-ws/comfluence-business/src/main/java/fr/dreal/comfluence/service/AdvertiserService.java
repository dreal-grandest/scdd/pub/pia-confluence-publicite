package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.dto.AdvertiserDto;
import fr.dreal.comfluence.business.entities.Advertiser;
import fr.dreal.comfluence.business.entities.Departement;
import fr.dreal.comfluence.mapper.AdvertiserMapper;
import fr.dreal.comfluence.repository.AdvertiserRepository;
import fr.dreal.comfluence.repository.DepartementRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AdvertiserService {
	
	private final AdvertiserRepository advertiserRepository;
	private final DepartementRepository departementRepository;
	
	public AdvertiserService(AdvertiserRepository advertiserRepository, DepartementRepository departementRepository) {
		this.advertiserRepository = advertiserRepository;
		this.departementRepository = departementRepository;
	}
	
	public List<AdvertiserDto> findAll() {
		final List<Advertiser> advertisers = this.advertiserRepository.findAll();
		return advertisers.stream().map(AdvertiserMapper.INSTANCE::entityToDto).collect(Collectors.toList());
	}
	
	public List<AdvertiserDto> findAllByDepartement(Long code) {
		final List<Advertiser> advertisers = this.advertiserRepository.findAllByDepartements_Code(code);
		return advertisers.stream().map(AdvertiserMapper.INSTANCE::entityToDto).collect(Collectors.toList());
	}
	
	public AdvertiserDto addDepartement(AdvertiserDto advertiserDto, Long departementCode) {
		// find on name in case it already exists on different departement
		Optional<Advertiser> oAdvertiser = this.advertiserRepository.findByNameIgnoreCase(advertiserDto.getName());
		Advertiser advertiser;
		if (oAdvertiser.isEmpty()) {
			final Advertiser entity = AdvertiserMapper.INSTANCE.dtoToEntity(advertiserDto);
			entity.setDepartements(new ArrayList<>());
			advertiser = this.advertiserRepository.save(entity);
		} else {
			advertiser = oAdvertiser.get();
		}
		Departement departement = this.departementRepository.getOne(departementCode);
		if (advertiser.getDepartements().stream().noneMatch(dep -> dep.getCode().equals(departementCode))) {
			advertiser.getDepartements().add(departement);
		}
		return AdvertiserMapper.INSTANCE.entityToDto(this.advertiserRepository.save(advertiser));
	}
}
