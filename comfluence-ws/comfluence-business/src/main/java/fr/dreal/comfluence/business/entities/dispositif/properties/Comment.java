package fr.dreal.comfluence.business.entities.dispositif.properties;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class Comment {

    /**
     * identifiant de base de données
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter @Setter
    private Long id;

    @Column(length = 511)
    @Getter @Setter
    private String text;
}
