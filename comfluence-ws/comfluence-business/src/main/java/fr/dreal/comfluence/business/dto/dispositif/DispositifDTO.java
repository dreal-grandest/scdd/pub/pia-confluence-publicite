package fr.dreal.comfluence.business.dto.dispositif;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.dreal.comfluence.business.data.dispositif.DispositifStatus;
import fr.dreal.comfluence.business.data.dispositif.Guidance;
import fr.dreal.comfluence.business.data.dispositif.Type;
import fr.dreal.comfluence.business.dto.dispositif.properties.AddressDTO;
import fr.dreal.comfluence.business.dto.dispositif.properties.CommentDTO;
import fr.dreal.comfluence.business.dto.dispositif.properties.ContextDTO;
import fr.dreal.comfluence.business.dto.dispositif.properties.CoordinatesDTO;
import fr.dreal.comfluence.business.dto.dispositif.properties.DescriptionDTO;
import fr.dreal.comfluence.business.dto.dispositif.properties.InfrigementDTO;
import fr.dreal.comfluence.business.dto.photo.PhotoDto;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class DispositifDTO {
    private Long id;

    /** reference du dispositif **/
    private String reference;

    /** status du dispositif **/
    private DispositifStatus status;

    private Type type;
    
    private Guidance guidance;
    
    private AddressDTO address;
    
    private CoordinatesDTO coordinates;
    
    private ContextDTO context;
    
    private DescriptionDTO description;
    
    private List<InfrigementDTO> infrigements;
    
    private CommentDTO comment;
    
    private Date operationDate;
    
    private List<PhotoDto> photos;
    
    private List<DispositifFilterDTO> tags;
    
    private boolean removed;
    
    private boolean sync = true;

    private Set<Integer> lastProcGroupId;
}
