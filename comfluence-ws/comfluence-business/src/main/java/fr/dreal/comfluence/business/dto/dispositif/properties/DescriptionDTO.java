package fr.dreal.comfluence.business.dto.dispositif.properties;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.dreal.comfluence.business.data.dispositif.DeragotoryType;
import fr.dreal.comfluence.business.data.dispositif.Position;
import fr.dreal.comfluence.business.dto.AdvertiserDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class DescriptionDTO {

    private Long id;

    private List<Position> positions;

    private AdvertiserDto advertiser;

    private String announcer;

    private DimensionDTO dimension;
    
    private DeragotoryType derogatoryType;
}
