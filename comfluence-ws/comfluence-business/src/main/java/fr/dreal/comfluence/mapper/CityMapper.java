package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.CityDto;
import fr.dreal.comfluence.business.entities.City;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class CityMapper {
	public static final CityMapper INSTANCE = Mappers.getMapper(CityMapper.class);
	
	public abstract CityDto entityToDto(City city);
}
