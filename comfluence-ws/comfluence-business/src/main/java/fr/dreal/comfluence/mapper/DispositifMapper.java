package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.dispositif.DispositifDTO;
import fr.dreal.comfluence.business.entities.Information;
import fr.dreal.comfluence.business.entities.dispositif.Dispositif;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


/**
 * Mapper entre les différents objets concernant les tâches.
 */
@Mapper(uses = {DispositifFilterMapper.class})
public abstract class DispositifMapper {

    public static final DispositifMapper INSTANCE = Mappers.getMapper( DispositifMapper.class );

    @Mapping(source = "informations", target = "lastProcGroupId", qualifiedByName = "lastGroupeId")
    public abstract DispositifDTO entityToDto(Dispositif dispositif);

    @Named("lastGroupeId")
    public static Set<Integer> lastGroupeId(Collection<Information> informationList) {
        Set<Integer> result = new HashSet<>();
        if(informationList != null) {
            for(Information information: informationList) {
                if(information.getProcedure() != null && information.getProcedure().getGroupe() != null) {
                    result.add(information.getProcedure().getGroupe().getId());
                }
            }
        }
        return result;
    }
    
    public abstract Dispositif dtoToEntity(DispositifDTO dispositif);
}

