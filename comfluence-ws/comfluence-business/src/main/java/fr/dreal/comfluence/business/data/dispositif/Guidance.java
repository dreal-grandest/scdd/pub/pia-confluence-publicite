package fr.dreal.comfluence.business.data.dispositif;

public enum Guidance{
    AUTHORIZATION,
    DECLARATION,
    OUTSTATE
}
