package fr.dreal.comfluence.business.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * User table alias as "users" for {@link org.springframework.security.provisioning.JdbcUserDetailsManager} support
 */
@Entity(name = "users")
public class User {
	
	/**
	 * identifiant de base de données
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter
	private Long id;


	@Column(nullable = false, length = 512)
	@Getter @Setter
	private String mail;

    @Column(nullable = false, length = 512)
	@Getter @Setter
	private String password;

    @Column(nullable = false, length = 512)
	@Getter @Setter
	private String lastName;

    @Column(nullable = false, length = 512)
	@Getter @Setter
	private String firstName;
	
	@Getter @Setter
	private boolean enabled;

	@Getter @Setter
    @ManyToOne
	private Departement departement;

	@Getter @Setter
	private Boolean pwdEncrypted = Boolean.TRUE;
	@Getter @Setter
	private Boolean deleted = Boolean.FALSE;

	@ElementCollection(fetch = FetchType.EAGER)
	@Column(length = 1024)
    @Getter @Setter
    private List<String> token;

	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @Getter @Setter
	private Set<Role> roles;
}
