package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.entities.CourrierTemplate;
import fr.dreal.comfluence.business.entities.Procedure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository pour les objets métier "dispositif"
 */
@Repository
@Component
public interface ProcedureRespository extends JpaRepository<Procedure, Long> {
	
	@Query("select p.courrierTemplates from Procedure p where p.id = :id")
	List<CourrierTemplate> findAllAssociatedTemplateById(@Param("id") Long id);
	
	@Query("select distinct inf.impliedProcedure from Dispositif d join d.infrigements inf where d.id = :id")
	List<Procedure> findAllLinkedToInfrigementsDevice(Long id);
	
	@Query("select distinct inf.impliedProcedure from Infrigement inf where inf.impliedProcedure is not null")
	List<Procedure> findAllLinkedToInfrigements();
}
