package fr.dreal.comfluence.business.data.dispositif;

import lombok.Getter;

public enum PhotoType {
    ALL(0),
    NEAR(1),
    BUTEAU(2),
    DOUBLE_FACE(3),
    ;
    PhotoType(int order) {
        this.order = order;
    }
    @Getter
    private int order;
}
