package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface RoleRepository extends JpaRepository<Role, Long> {
    @Query("select r from Role r where r.id IN (:idList)")
    Set<Role> searchRoles(@Param("idList") List<Long> idList);
}
