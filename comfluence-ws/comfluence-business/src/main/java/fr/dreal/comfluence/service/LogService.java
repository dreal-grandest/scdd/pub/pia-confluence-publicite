package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.data.ChangeType;
import fr.dreal.comfluence.business.dto.log.LogOnDispositif;
import fr.dreal.comfluence.business.dto.log.LogOnProcedure;
import fr.dreal.comfluence.business.dto.log.LoggableAction;
import fr.dreal.comfluence.business.entities.Information;
import fr.dreal.comfluence.business.entities.dispositif.Dispositif;
import fr.dreal.comfluence.repository.DispositifRespository;
import fr.dreal.comfluence.repository.InformationRespository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author SopraGroup
 */
@Service
public class LogService {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogService.class);
    private final DispositifRespository dispositifRespository;
    private final InformationRespository informationRespository;
    public LogService(DispositifRespository dispositifRespository, InformationRespository informationRespository) {
        this.dispositifRespository = dispositifRespository;
        this.informationRespository = informationRespository;
    }

    /**
     * Log une action sur un dispositif et l'ajoute à la main courante
     * @param utilisateur utilisateur ayant initié l'action
     * @param type type de modification
     * @param action description de l'action
     * @param idDevice id du dispositif
     */
    public void registerActionOnDispositif(String utilisateur, LoggableAction.Type type, String action, Long idDevice){
        logAndRegisterActionOnDispositif(utilisateur, type, action, idDevice, true);
    }
    /**
     * Log une action sur une procédure et l'ajoute à la main courante
     * @param utilisateur utilisateur ayant initié l'action
     * @param type type de modification
     * @param action description de l'action
     * @param idDevice id du dispositif
     * @param idProcedure id de la procédure ciblée
     */
    public void registerActionOnProcedure(String utilisateur, LoggableAction.Type type, String action, Long idDevice, Long idProcedure) {
        logAndRegisterActionOnProcedure(utilisateur, type, action, idDevice, idProcedure, true);
    }

    /**
     * Log une action sur un dispositif
     * @param utilisateur utilisateur ayant initié l'action
     * @param type type de modification
     * @param action description de l'action
     * @param idDevice id du dispositif
     */
    public void logActionOnDispositif(String utilisateur, LoggableAction.Type type, String action, Long idDevice){
        logAndRegisterActionOnDispositif(utilisateur, type, action, idDevice, false);
    }
    /**
     * Log une action sur une procédure
     * @param utilisateur utilisateur ayant initié l'action
     * @param type type de modification
     * @param action description de l'action
     * @param idDevice id du dispositif
     * @param idProcedure id de la procédure ciblée
     */
    public void logActionOnProcedure(String utilisateur, LoggableAction.Type type, String action, Long idDevice, Long idProcedure) {
        logAndRegisterActionOnProcedure(utilisateur, type, action, idDevice, idProcedure, false);
    }

    private Information logAndRegisterActionOnProcedure(String utilisateur, LoggableAction.Type type, String action, Long idDevice, Long idProcedure, boolean addAction) {
        LogOnProcedure logOnProcedure = new LogOnProcedure(utilisateur, type, action, idDevice, idProcedure);
        if(LOGGER.isInfoEnabled()) {
            LOGGER.info(logOnProcedure.toString());
        }
        Information information = null;
        if(addAction) {
            information = logAction(logOnProcedure);
        }
        return information;
    }
    public Information logAndRegisterActionOnDispositif(String utilisateur, LoggableAction.Type type, String action, Long idDevice, boolean addAction){
        LogOnDispositif logOnDispositif = new LogOnDispositif(utilisateur, type, action, idDevice);
        if(LOGGER.isInfoEnabled()) {
            LOGGER.info(logOnDispositif.toString());
        }
        Information information = null;
        if(addAction) {
            information = logAction(logOnDispositif);
        }
        return information;
    }
    public Information logAction(LoggableAction loggableAction) {
        Information result = null;
        if(loggableAction.getType() != null) {
            Information information = new Information();
            Dispositif dispositif = dispositifRespository.getOne(loggableAction.getIdDispositif());
            information.setComment(loggableAction.getComment());

            switch (loggableAction.getType()) {
                case ADD:
                    information.setChangeType(ChangeType.ADDED);
                    break;
                case MODIFY:
                    information.setChangeType(ChangeType.MODIFIED);
                    break;
                case REMOVE:
                    information.setChangeType(ChangeType.REMOVED);
                    break;
            }
            information.setAddedDate(loggableAction.getDate());
            information.setDispositif(dispositif);
            result = informationRespository.save(information);
        }
        return result;
    }
}
