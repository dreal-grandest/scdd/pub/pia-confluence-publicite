package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.entities.Departement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartementRepository extends JpaRepository<Departement, Long> {
	
	@Query("select d from Departement d join fetch d.territorialDirection where d.code = :code")
	Departement getOneWithTerritorialDirection(@Param("code") Long code);
}
