package fr.dreal.comfluence.business.entities;

import fr.dreal.comfluence.business.data.DispositifFilter;
import fr.dreal.comfluence.business.data.ProcedureGroupe;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Procedure {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter @Setter
	private Long id;
	
	@Getter @Setter
	private String label;
	
	@Getter @Setter
	private Integer delay;
	
	@ManyToMany
	@Getter @Setter
	private Collection<Procedure> incompatibleProcedures;

	@ElementCollection(targetClass = DispositifFilter.class)
    @Enumerated(EnumType.STRING)
    @Getter @Setter
	private Collection<DispositifFilter> dispositifFilters;

	@ManyToMany
    @Getter @Setter
	private Collection<CourrierTemplate> courrierTemplates;

	@Enumerated(EnumType.STRING)
    @Getter @Setter
	private ProcedureGroupe groupe;

	@Getter @Setter
	private Boolean isPv;
}
