package fr.dreal.comfluence.business.dto;

import fr.dreal.comfluence.business.data.ChangeType;
import fr.dreal.comfluence.business.data.InformationType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter @Setter
public class InformationDto {
	
	private Long id;
	
	private String comment;
	
	private Integer delay;
	
	private String contentType;
	
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date addedDate;
	
	private ChangeType changeType = ChangeType.ADDED;
	
	private Long idDevice;
	
	private InformationType type = InformationType.COMMENT;
	
	private ProcedureDto procedure;
	
	private Boolean hasDoc = Boolean.FALSE;
	
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date closeDate;
}
