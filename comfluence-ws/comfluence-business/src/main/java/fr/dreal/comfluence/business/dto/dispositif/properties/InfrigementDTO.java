package fr.dreal.comfluence.business.dto.dispositif.properties;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.dreal.comfluence.business.dto.ProcedureDto;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class InfrigementDTO {
    
    private Long id;
    
    private String natinfCode;
    
    private String offenseNature;
    
    private String codeArticle;
    
    private String codeArticleReprimand;
    
    private ProcedureDto impliedProcedure;
}
