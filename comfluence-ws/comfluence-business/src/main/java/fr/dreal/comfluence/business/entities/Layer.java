package fr.dreal.comfluence.business.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Entity
public class Layer {
	
	@Id
	@Getter @Setter
	private Long id;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
    @Column(length = 512)
	private String url;
	
	@Getter @Setter
	private String map;
	
	@Getter @Setter
	private String service;
	
	@Getter @Setter
	private String version;
	
	@Getter @Setter
	private String request;
	
	@Getter @Setter
	private String typeName;
	
	@Getter @Setter
	private String outputFormat;
	
	@Getter @Setter
	private String srsName;

	@ManyToMany
	@Getter @Setter
	private Collection<Departement> departements;
    @Getter @Setter
    private Boolean needProxy;
    @Getter @Setter
    private Boolean usedForRestriction;
    @Getter @Setter
    private String categories;
    @Getter @Setter
    private String categoriesNamespace;
}
