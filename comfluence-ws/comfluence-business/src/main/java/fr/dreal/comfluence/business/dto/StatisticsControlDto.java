package fr.dreal.comfluence.business.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class StatisticsControlDto {
	/**
	 * devices controlled by the controller during the control of the day
	 */
	private Integer controlledDevices;
	
	/**
	 * in km
	 */
	private Double traveledDistance;
	
	/**
	 * in minutes
	 */
	private Long duration;
	
	/**
	 * start of the control (timestamp of 1st device creation)
	 */
	private Date startDate;
}
