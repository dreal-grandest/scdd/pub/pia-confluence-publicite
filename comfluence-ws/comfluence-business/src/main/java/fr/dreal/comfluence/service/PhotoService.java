package fr.dreal.comfluence.service;


import fr.dreal.comfluence.business.data.dispositif.PhotoType;
import fr.dreal.comfluence.business.dto.photo.PhotoDto;
import fr.dreal.comfluence.business.entities.Photo;
import fr.dreal.comfluence.mapper.PhotoMapper;
import fr.dreal.comfluence.repository.PhotoRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.persistence.EntityNotFoundException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Implémentation du service user permettant la gestion des photos des dispositifs
 */
@Service(value = "photoService")
public class PhotoService {
	
	/**
	 * Permet l'utilisation de logBack à travers le LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PhotoService.class);
	
	/**
	 * repository des dispositifs
	 **/
	private final PhotoRepository photoRepository;
	
	@Value("${comfluence.upload.dir:/tmp}")
	private String photoUploadFolder;
	
	/**
	 * @param repository repository
	 */
	public PhotoService(PhotoRepository repository) {
		this.photoRepository = repository;
	}
	
	/**
	 * @param idDispositif  name of file
	 * @param multipartFile MultipartFile
	 * @return the photo created
	 */
	public PhotoDto savePhoto(Long idDispositif, MultipartFile multipartFile, PhotoType type, Boolean active) throws IOException {
		if(PhotoType.BUTEAU.equals(type) && Boolean.FALSE.equals(active)){
			Photo photo = prepareInactiveButeauPhoto(idDispositif, type);
			archivedPhotosByType(photo.getIdDispositif(), photo.getPhotoType());
			
			photo = photoRepository.save(photo);
			final PhotoDto photoDto = PhotoMapper.INSTANCE.entityToDto(photo);
			photoDto.setImageLinked(false);
			return photoDto;
		} else {
			Photo photo = preparePhoto(idDispositif, type, multipartFile);
			//Pass to archive photos
			archivedPhotosByType(photo.getIdDispositif(), photo.getPhotoType());
			
			photoRepository.save(photo);
			
			byte[] data = multipartFile.getBytes();
			String uniqueFileName = photo.getId() + "_" + photo.getOriginalFileName();
			
			// Copie du fichier sur le sysème de fichier
			File img = copyFile(photo, data, uniqueFileName);
			if (img != null) {
				photo.setPath(img.getAbsolutePath());
				//Create thumb

				int thumbnailWidth = 200;
				int thumbnailHeight = 150;
				File thumb = createFileThumbnail(img, thumbnailWidth, thumbnailHeight);
				if (thumb != null) {
					photo.setPathThumb(thumb.getAbsolutePath());
				} else {
					LOGGER.error("Error thumbnail is null");
				}
				
				//Saved path
				photoRepository.save(photo);
			} else {
				LOGGER.error("Error photo is null");
			}
			//Prepare Dto
			PhotoDto dto = PhotoMapper.INSTANCE.entityToDto(photo);
			dto.setUniqueFileName(uniqueFileName);
			dto.setImageLinked(true);
			return dto;
		}
	}
	
	private Photo prepareInactiveButeauPhoto(Long idDispositif, PhotoType type) {
		Photo photo = new Photo();
		photo.setIdDispositif(idDispositif);
		photo.setPhotoDate(LocalDateTime.now());
		photo.setPhotoType(type);
		photo.setIsArchived(false);
		photo.setActive(false);
		return photo;
	}
	
	public Photo getPhoto(Long idPhoto) {
		Optional<Photo> photoFound = photoRepository.findById(idPhoto);
		if (photoFound.isEmpty()) {
			throw new EntityNotFoundException("Didn't find a photo with id" + idPhoto);
		}
		return photoFound.get();
	}
	
	public byte[] getThumb(Long idPhoto) {
		Photo photo = this.getPhoto(idPhoto);
		if(photo != null && StringUtils.isNotBlank(photo.getPathThumb())) {
            File file = new File(photo.getPathThumb());
            try (FileInputStream fileInputStream = new FileInputStream(file)) {
                return fileInputStream.readAllBytes();
            } catch (IOException e) {
                LOGGER.error("Impossible de trouver le thumbnail", e);
                return new byte[0];
            }
        } else {
            return new byte[0];
        }
	}
	
	/**
	 * Creates a thumnail of provided image
	 *
	 * @param inputImgFile    The input image file
	 * @param thumnailWidth   Desired width of the output thumbnail
	 * @param thumbnailHeight Desired height of thr output thumnail
	 */
	private static File createFileThumbnail(File inputImgFile, int thumnailWidth, int thumbnailHeight) {
		File outputFile;
		try {

			// get original size
			BufferedImage originalImg = ImageIO.read(inputImgFile);
			double widthRatio = (double)thumnailWidth / (double)originalImg.getWidth();
			if (thumnailWidth > originalImg.getWidth()) {
				widthRatio = 1.0d;
			}
			double heightRatio = (double)thumbnailHeight / (double)originalImg.getHeight();
			if (thumbnailHeight > originalImg.getHeight()) {
				heightRatio = 1.0d;
			}
			double ratio = Math.min(widthRatio, heightRatio);
			int newWidth = (int)  (originalImg.getWidth() * ratio);
			int newHeight = (int) (originalImg.getHeight() * ratio);


			BufferedImage img = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
			img.createGraphics().drawImage(
					ImageIO.read(inputImgFile).getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH), 0,
					0, null);
			outputFile = new File(inputImgFile.getParentFile() + File.separator + "thumb_" + inputImgFile.getName());
			ImageIO.write(img, "jpg", outputFile);
			return outputFile;
		} catch (IOException e) {
			String message = String.format("Exception while generating file %s", e.getMessage());
			LOGGER.error(message, e);
			return null;
		}
	}
	
	public List<PhotoDto> getPhotosByDispositif(Long idDispositif) {
		List<Photo> photos = photoRepository.findAllByIdDispositifAndIsArchivedIsFalse(idDispositif);
		List<PhotoDto> photoDtos = new ArrayList<>();
		photos.forEach(item -> {
		    PhotoDto convertedPhoto = PhotoMapper.INSTANCE.entityToDto(item);
		    convertedPhoto.setImageLinked(StringUtils.isNotBlank(item.getPath()));
		    photoDtos.add(convertedPhoto);
		});
		return photoDtos;
	}
	
	private File copyFile(Photo photo, byte[] data, String uniqueFileName) {
		try {
			File folder = new File(photoUploadFolder + File.separator + photo.getIdDispositif());
			if (!folder.exists() && folder.mkdirs()) {
				LOGGER.info("folder created");
			}
			String pathFile = folder.getAbsolutePath() + File.separator + uniqueFileName;
			Path path = Paths.get(pathFile);
			Files.write(path, data);
			return new File(pathFile);
		} catch (IOException e) {
			String message = String.format("Exception while generating file %s", e.getMessage());
			LOGGER.error(message, e);
			return null;
		}
		
	}
	
	private void archivedPhotosByType(Long idDispositif, PhotoType type) {
		List<Photo> photosToArchived = photoRepository.
				findAllByIdDispositifAndPhotoTypeAndIsArchivedIsFalse(idDispositif, type);
		photosToArchived.forEach(item -> item.setIsArchived(true));
		photoRepository.saveAll(photosToArchived);
	}
	
	private Photo preparePhoto(Long idDispositif, PhotoType type, MultipartFile multipartFile) throws IOException {
		byte[] data = multipartFile.getBytes();
		String originalFilename = multipartFile.getOriginalFilename();
		String content = multipartFile.getContentType();
		Photo photo = new Photo();
		photo.setIdDispositif(idDispositif);
		photo.setOriginalFileName(originalFilename);
		photo.setPhotoDate(LocalDateTime.now());
		photo.setSha256(DigestUtils.sha256Hex(data));
		photo.setContentType(content);
		photo.setPhotoType(type);
		photo.setIsArchived(false);
		photo.setActive(true);
		return photo;
	}
	
	public Boolean deleteById(Long id) {
		Photo photo = photoRepository.getOne(id);
		List<PhotoType> types = Arrays.asList(PhotoType.ALL, PhotoType.BUTEAU, PhotoType.NEAR);
		if(types.contains(photo.getPhotoType())){
			LOGGER.error("Cannot delete mandatory photo : [id = %s, photoType = %s]", photo.getId(), photo.getPath());
			return Boolean.FALSE;
		}
		photoRepository.delete(photo);
		return Boolean.TRUE;
	}
}
