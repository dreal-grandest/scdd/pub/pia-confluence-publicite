package fr.dreal.comfluence.business.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author SopraGroup
 */
@Getter @Setter
public class Box {
    // format EPSG:2154 RGF93 / Lambert-93
    private Double minLat;
    private Double maxLat;
    private Double minLong;
    private Double maxLong;
}
