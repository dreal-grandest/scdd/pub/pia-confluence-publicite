package fr.dreal.comfluence.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.FileSystemUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class FileHelper {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileHelper.class);
	
	private String fileOutputDir;
	private String photoOutputDir;
	private String generatedOutputDir;
	
	public FileHelper(@Value("${comfluence.upload.files.dir:/tmp}") String FileOutputDir,
			@Value("${comfluence.upload.dir:/tmp}") String PHOTO_OUTPUT_DIR,
			@Value("${comfluence.document.generation.dir:/tmp}") String GENERATED_OUTPUT_DIR) {
		this.fileOutputDir = FileOutputDir;
		this.photoOutputDir = PHOTO_OUTPUT_DIR;
		this.generatedOutputDir = GENERATED_OUTPUT_DIR;
	}
	
	public void deleteAllFilesDispositif(Long idDisp) {
		this.deletePhysical(Paths.get(getFileOutputDir(), String.valueOf(idDisp)));
		this.deletePhysical(Paths.get(getGeneratedOutputDir(), String.valueOf(idDisp)));
		this.deletePhysical(Paths.get(getPhotoOutputDir(), String.valueOf(idDisp)));
	}
	
	public void deletePhysical(Path path) {
		if(Files.exists(path)) {
			try {
				FileSystemUtils.deleteRecursively(path);
			} catch (IOException e) {
				LOGGER.error("Directory in path : {} can't be deleted.", path);
			}
		}
	}
	
	public String getFileOutputDir() {
		return fileOutputDir;
	}
	
	public String getPhotoOutputDir() {
		return photoOutputDir;
	}
	
	public String getGeneratedOutputDir() {
		return generatedOutputDir;
	}
}
