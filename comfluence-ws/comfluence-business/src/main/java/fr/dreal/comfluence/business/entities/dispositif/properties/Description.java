package fr.dreal.comfluence.business.entities.dispositif.properties;

import fr.dreal.comfluence.business.data.dispositif.DeragotoryType;
import fr.dreal.comfluence.business.data.dispositif.Position;
import fr.dreal.comfluence.business.entities.Advertiser;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Getter @Setter
public class Description {

    /**
     * identifiant de base de données
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(value = EnumType.ORDINAL)
    @ElementCollection
    private Collection<Position> positions;

    @ManyToOne
    private Advertiser advertiser;

    private String announcer;

    @OneToOne(cascade=CascadeType.ALL)
    private Dimension dimension;
    
    @Enumerated(value = EnumType.ORDINAL)
    private DeragotoryType derogatoryType;
}
