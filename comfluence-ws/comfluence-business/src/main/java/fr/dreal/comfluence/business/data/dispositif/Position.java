package fr.dreal.comfluence.business.data.dispositif;

public enum Position{
    WALL,
    FLOOR,
    BRIGHT,
    NUMERIC,
    TREE,
    POLE,
    PUBLIC_FURNITURE,
    URBAN_FURNITURE,
    TARPAULIN,
    VEHICULE,
    PLANTATION,
    OTHER_SUPPORT,
    FACADE,
    BLIND_WALL,
    NOT_BLIND_WALL,
    CEMETERY,
    PUBLIC_GARDEN,
    BLIND_FENCE,
    NOT_BLIND_FENCE,
    ROOF
}
