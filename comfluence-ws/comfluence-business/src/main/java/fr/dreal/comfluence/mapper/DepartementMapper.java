package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.Box;
import fr.dreal.comfluence.business.dto.DepartementDto;
import fr.dreal.comfluence.business.entities.Departement;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;


/**
 * Mapper entre les différents objets concernant les tâches.
 */
@Mapper
public abstract class DepartementMapper {
	
	public static final DepartementMapper INSTANCE = Mappers.getMapper(DepartementMapper.class);
	
	public abstract DepartementDto entityToDto(Departement departement);
	
	@Mappings({
			@Mapping(source = "box.minLat", target = "boundYMin"),
			@Mapping(source = "box.maxLat", target = "boundYMax"),
			@Mapping(source = "box.minLong", target = "boundXMin"),
			@Mapping(source = "box.maxLong", target = "boundXMax"),
	})
	public abstract Departement dtoToEntity(DepartementDto departementDto);
	
	
	@AfterMapping
	protected void transformBBox(@MappingTarget DepartementDto departementDto, Departement departement) {
		Box box = new Box();
		box.setMinLat(departement.getBoundYMin());
		box.setMaxLat(departement.getBoundYMax());
		box.setMinLong(departement.getBoundXMin());
		box.setMaxLong(departement.getBoundXMax());
		departementDto.setBox(box);
	}
}

