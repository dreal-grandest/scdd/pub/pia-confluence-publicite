package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.entities.Parameter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author SopraGroup
 */
@Repository
public interface ParameterRepository extends JpaRepository<Parameter, Long> {
    Optional<Parameter> findFirstByName(String name);
}
