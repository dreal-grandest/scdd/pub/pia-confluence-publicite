package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.entities.City;
import org.geolatte.geom.Geometry;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class CityRepositoryImpl implements CityRepositoryCustom {
	public static final String INTERSECTION_GEOMETRY_CITIES = "CityRepository_INTERSECTION_GEOMETRY_CITIES";
	
	private final EntityManager entityManager;
	
	public CityRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public List<City> findAllCityIntersecting(Geometry geometry) {
		Query query = entityManager.createNamedQuery(CityRepositoryImpl.INTERSECTION_GEOMETRY_CITIES)
				.unwrap(NativeQuery.class)
				.addEntity(City.class);
		query.setParameter("geometryParam", geometry);
		return (List<City>) query.getResultList();
	}
}
