package fr.dreal.comfluence.business.data.dispositif;

public enum Type {
    SIGNBOARD,
    PRESIGNBOARD,
    AD
}
