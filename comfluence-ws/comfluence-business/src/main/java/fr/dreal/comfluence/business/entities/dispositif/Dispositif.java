package fr.dreal.comfluence.business.entities.dispositif;

import fr.dreal.comfluence.business.data.dispositif.DispositifStatus;
import fr.dreal.comfluence.business.data.dispositif.Guidance;
import fr.dreal.comfluence.business.data.dispositif.Type;
import fr.dreal.comfluence.business.entities.Information;
import fr.dreal.comfluence.business.entities.dispositif.properties.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;

/**
 * Représente l'objet métier Dispositif
 */
@Entity
@Getter
@Setter
public class Dispositif {
	
	/**
	 * identifiant de base de données
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
    /** reference du dispositif **/
    private String reference;
	/**
	 * status du dispositif
	 **/
	@Enumerated(value = EnumType.ORDINAL)
	private DispositifStatus status;
	
	@Enumerated(value = EnumType.ORDINAL)
	private Type type;
	
	@Enumerated(value = EnumType.ORDINAL)
	private Guidance guidance;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Address address;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Coordinates coordinates;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Context context;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Description description;
	
	@ManyToMany
	private Collection<Infrigement> infrigements;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Comment comment;
	
	private Date operationDate;
	
	@OneToMany(mappedBy = "dispositif")
	private Collection<Information> informations;
	
	private LocalDate insertedDate;
	
	/**
	 * Dispositif déposé ou non
	 **/
	private boolean removed;
	
	private Date removedDate;
}
