package fr.dreal.comfluence.business.data.dispositif;

public enum DeragotoryType {
	MH,
	CULTURAL_ACTIVITY,
	TO_DETERMINE,
	TEMPORARY
}
