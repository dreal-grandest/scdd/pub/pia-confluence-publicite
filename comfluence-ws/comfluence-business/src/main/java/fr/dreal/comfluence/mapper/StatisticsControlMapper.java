package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.StatisticsControlDto;
import fr.dreal.comfluence.business.entities.StatisticsControl;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


/**
 * Mapper entre les différents objets concernant les tâches.
 */
@Mapper
public abstract class StatisticsControlMapper {

    public static final StatisticsControlMapper INSTANCE = Mappers.getMapper( StatisticsControlMapper.class );

    public abstract StatisticsControlDto entityToDto(StatisticsControl dispositif);
}

