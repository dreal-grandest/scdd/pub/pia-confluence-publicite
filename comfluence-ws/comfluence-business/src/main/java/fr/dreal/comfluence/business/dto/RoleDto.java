package fr.dreal.comfluence.business.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author SopraGroup
 */
@Getter @Setter
public class RoleDto {
    private Long id;
    private String name;
}
