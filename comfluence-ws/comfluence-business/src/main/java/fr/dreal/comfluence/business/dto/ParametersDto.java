package fr.dreal.comfluence.business.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

public class ParametersDto {
	
	@Getter @Setter
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String map;
	
	@Getter @Setter
	private String service;
	
	@Getter @Setter
	private String version;
	
	@Getter @Setter
	private String request;
	
	@Getter @Setter
	private String typeName;
	
	@Getter @Setter
	private String outputFormat;
	
	@Getter @Setter
	private String srsName;
}
