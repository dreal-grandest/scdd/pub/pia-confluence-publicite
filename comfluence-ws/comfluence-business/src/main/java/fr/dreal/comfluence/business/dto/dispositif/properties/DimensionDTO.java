package fr.dreal.comfluence.business.dto.dispositif.properties;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DimensionDTO {

    @Getter @Setter
    private Long id;

    @Getter @Setter
    private Boolean isEstimated;

    @Getter @Setter
    private SizeDTO size;

}
