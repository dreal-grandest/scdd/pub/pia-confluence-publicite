package fr.dreal.comfluence.business.dto.dispositif.properties;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CoordinatesDTO {

    @Getter @Setter
    private Long id;

    @Getter @Setter
    private Double latitude;

    @Getter @Setter
    private Double longitude;
}
