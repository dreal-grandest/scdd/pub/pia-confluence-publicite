package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.dto.CityDto;
import fr.dreal.comfluence.mapper.CityMapper;
import fr.dreal.comfluence.repository.CityRepository;
import fr.dreal.comfluence.repository.GeometryConverterRepository;
import org.geolatte.geom.Geometry;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CityService {
	
	private final CityRepository cityRepository;
	private final GeometryConverterRepository geometryConverterRepository;
	
	public CityService(CityRepository cityRepository, GeometryConverterRepository geometryConverterRepository) {
		this.cityRepository = cityRepository;
		this.geometryConverterRepository = geometryConverterRepository;
	}
	
	public List<CityDto> findAllIntersect(List<String> geometries) {
		Geometry geometry = geometryConverterRepository.convertGeoJsonToGeometry(geometries);
		return cityRepository.findAllCityIntersecting(geometry).stream()
				.map(CityMapper.INSTANCE::entityToDto)
				.collect(Collectors.toList());
	}
}
