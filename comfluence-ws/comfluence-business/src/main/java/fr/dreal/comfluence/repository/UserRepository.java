package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.entities.User;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findFirstByMailAndEnabledTrue(String mail);
    User findFirstByMailAndEnabledTrueAndDeletedFalse(String mail);
	
	Optional<User> findByMailAndDeletedIsFalse(String mail);
	
	List<User> findAllByEnabledTrueAndDeletedIsFalse();
    List<User> findAllByDeletedIsFalse();
	
	List<User> findAllByDepartementCodeAndDeletedIsFalse(Long departmentCode);
	
	long countByEnabledTrue();
	
	long countByMailAndEnabledTrue(String mail);
	long countByMailAndDeletedIsFalse(String mail);

	@Query("select count(u) from users u where :token MEMBER OF u.token")
	long countByAccessToken(@Param("token") String token);
	
	@Modifying
	@Query("update users u set u.password = :pwd where u.mail = :mail ")
	int updatePwd(@Param("mail") String mail, @Param("pwd") String pwd);
    @Modifying
    @Query("update users u set u.password = :pwd where u.id = :id ")
    int updatePwd(@Param("id") Long id, @Param("pwd") String pwd);

    @Modifying
    @Query("update users u set u.enabled = :enable where u.id = :id")
    int updateEnable(@Param("id") long id, @Param("enable") boolean enable);

    @Modifying
    @Query("update users u set u.deleted = true where u.id = :id")
    int delete(@Param("id") long id);
}
