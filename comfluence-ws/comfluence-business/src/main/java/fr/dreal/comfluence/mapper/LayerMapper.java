package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.LayerDto;
import fr.dreal.comfluence.business.dto.ParametersDto;
import fr.dreal.comfluence.business.entities.Layer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class LayerMapper {
	public static final LayerMapper INSTANCE = Mappers.getMapper(LayerMapper.class);
	
	public LayerDto entityToDto(Layer layer) {
		final LayerDto layerDto = new LayerDto();
		layerDto.setId(layer.getId());
		layerDto.setUrl(layer.getUrl());
		layerDto.setName(layer.getName());
		layerDto.setParameters(this.entityToParametersDto(layer));
		layerDto.setNeedProxy(layer.getNeedProxy());
		return layerDto;
	}
	
	public abstract ParametersDto entityToParametersDto(Layer layer);
}
