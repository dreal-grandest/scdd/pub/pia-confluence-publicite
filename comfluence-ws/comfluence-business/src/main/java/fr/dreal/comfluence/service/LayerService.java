package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.dto.LayerDto;
import fr.dreal.comfluence.business.entities.Layer;
import fr.dreal.comfluence.externalapi.dto.WFSRequest;
import fr.dreal.comfluence.externalapi.ws.LayerWs;
import fr.dreal.comfluence.mapper.LayerMapper;
import fr.dreal.comfluence.repository.LayerRepository;
import org.locationtech.jts.geom.Coordinate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class LayerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(LayerService.class);
	private LayerRepository layerRepository;
	private LayerWs layerWs;
	
	public LayerService(LayerRepository layerRepository, LayerWs layerWs) {
		this.layerRepository = layerRepository;
		this.layerWs = layerWs;
	}

	public List<LayerDto> findAllWMS() {
	    List<Layer> layers = layerRepository.findAllByService(Service.WMS.getValue());
	    return layers.stream().map(LayerMapper.INSTANCE::entityToDto).collect(Collectors.toList());
    }
	
	public List<LayerDto> findWMSByCodeDepartement(Long idDepartement) {
		List<Layer> layers = layerRepository.findAllByDepartements_CodeAndService(idDepartement, Service.WMS.getValue());
		if(layers == null) {
		    layers = new ArrayList<>();
        }
		layers.addAll(layerRepository.findAllByDepartementsEmptyAndService(Service.WMS.getValue()));
		return layers.stream().map(LayerMapper.INSTANCE::entityToDto).collect(Collectors.toList());
	}

    public @NotNull List<Layer> findWFSForRestriction(Long idDepartement) {
        List<Layer> layers = layerRepository.findAllByDepartements_CodeAndServiceAndUsedForRestrictionTrue(idDepartement, Service.WFS.getValue());
        if(layers == null) {
            layers = new ArrayList<>();
        }
        layers.addAll(layerRepository.findAllByDepartementsEmptyAndServiceAndUsedForRestrictionTrue(Service.WFS.getValue()));
        return layers;
    }

    public List<String> searchRestriction(Long idDepartement, Double x, Double y) {
        Coordinate coordinate = new Coordinate(x, y);
        List<CompletableFuture<List<String>>> paralleleCallResult = new ArrayList<>();
        List<Layer> wfsLayer = findWFSForRestriction(idDepartement);
        wfsLayer.forEach(layer -> {
            paralleleCallResult.add(searchRestrictionForOneLayer(layer, coordinate));
        });

        List<String> result = new ArrayList<>();
        paralleleCallResult.forEach(callResult -> {
            try {
                result.addAll(callResult.get());
            } catch (ExecutionException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            } catch (InterruptedException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
                Thread.currentThread().interrupt();
            }
        });
        return result;
    }

    @Async
    protected CompletableFuture<List<String>> searchRestrictionForOneLayer(Layer layer, Coordinate coordinate) {
        WFSRequest wfsRequest = new WFSRequest();
        wfsRequest.setVersion(layer.getVersion());
        wfsRequest.setUrl(layer.getUrl());
        wfsRequest.setService(layer.getService());
        wfsRequest.setRequest(layer.getRequest());
        wfsRequest.setMap(layer.getMap());
        wfsRequest.setNameSpace(layer.getCategoriesNamespace());
        List<String> restrictionList = layerWs.findRestriction(coordinate,wfsRequest, layer.getTypeName(), layer.getCategories().split(","));
        return CompletableFuture.completedFuture(restrictionList);
    }

	@Cacheable(CacheServiceForService.LAYER_CACHE_NAME)
	public LayerDto findOneById(Long id) {
	    LayerDto result = null;
	    Optional<Layer> layer = layerRepository.findById(id);
	    if(layer.isPresent()) {
	        result = LayerMapper.INSTANCE.entityToDto(layer.get());
        }
	    return result;
    }

    private enum Service {
	    WMS("WMS"),
        WFS("WFS");

        private final String value;

        Service(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
