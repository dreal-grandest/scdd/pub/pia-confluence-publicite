package fr.dreal.comfluence.business.entities.dispositif.properties;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class Address {

    /**
     * identifiant de base de données
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter @Setter
    private Long id;

    /** status du dispositif **/
    @Getter @Setter
    private String number;
    /** status du dispositif **/
    @Getter @Setter
    private String street;
    /** status du dispositif **/
    @Getter @Setter
    private Integer code;
    /** status du dispositif **/
    @Getter @Setter
    private String city;
    /** code insee de la commune de l'adresse **/
    @Getter @Setter
    private String inseeCode;
    /**
     * Code du département
     */
    @Getter @Setter
    private Long codeDepartement;
}
