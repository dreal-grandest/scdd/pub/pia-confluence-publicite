package fr.dreal.comfluence.business.data;

public enum InformationType {
	COMMENT,
	PROCEDURE
}
