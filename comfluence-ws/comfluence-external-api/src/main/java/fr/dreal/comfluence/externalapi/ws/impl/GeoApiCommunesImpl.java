package fr.dreal.comfluence.externalapi.ws.impl;

import fr.dreal.comfluence.externalapi.dto.GeoApiCommune;
import fr.dreal.comfluence.externalapi.ws.GeoApiCommunesWs;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author SopraGroup
 */
@Service
public class GeoApiCommunesImpl implements GeoApiCommunesWs {
	private final WebTarget geoApiTarget;
	private static final String COMMUNES = "/communes";
	
	public GeoApiCommunesImpl(@Value("${comfluence.geoapi.root}") String geoapiUrl) {
		Client client = ClientBuilder.newClient();
		this.geoApiTarget = client.target(geoapiUrl + COMMUNES);
	}
	
	@Override
	public List<GeoApiCommune> searchCommunes(String codePostal) {
		return geoApiTarget.queryParam("codePostal", codePostal).request(MediaType.APPLICATION_JSON).get(
				new GenericType<>() {});
	}
	
	@Override
	public GeoApiCommune commune(String codeInsee, List<String> fields) {
		return geoApiTarget.path(codeInsee).queryParam("fields", fields).request(MediaType.APPLICATION_JSON).get(
				GeoApiCommune.class);
	}
}
