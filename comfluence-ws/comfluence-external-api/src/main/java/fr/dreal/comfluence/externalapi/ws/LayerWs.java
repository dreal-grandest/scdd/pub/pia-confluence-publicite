package fr.dreal.comfluence.externalapi.ws;

import fr.dreal.comfluence.externalapi.dto.WFSRequest;
import fr.dreal.comfluence.externalapi.dto.WXSResponse;
import org.locationtech.jts.geom.Coordinate;

import java.util.List;
import java.util.Map;

/**
 * @author SopraGroup
 */
public interface LayerWs {
    WXSResponse proxyLayer(String url, Map<String, String> queryMap);
    List<String> findRestriction(Coordinate coordinate, WFSRequest wfsRequest, String typeName, String... propertyNameList);
}
