package fr.dreal.comfluence.externalapi.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author SopraGroup
 */
@Getter @Setter
public class WFSRequest {
    private String url;
    private String service;
    private String version;
    private String request;
    private String map;
    private String nameSpace;
}
