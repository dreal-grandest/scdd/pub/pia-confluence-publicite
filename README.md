Dépôt de l'application Confluence dont le projet est aujourd'hui abandonnée suite au report des attributions concernant la publicité de la DREAL vers les DDT.

Déplacé le 4 octobre 2022 dans https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-grandest/scdd/pia-confluence-publicite/ --> https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-grandest/scdd/pub/pia-confluence-publicite/ 

**A FAIRE: Une fois le feu vert de migration envoyé à la Dnum pour changer l'architecture de dépôt sur la Forge GitLab du MTE : Changer la visibilité Interne --> public**
