#!/bin/bash
DIR_NAME=`date +"%Y%m%d%H%M%S"`
BRANCHE="$1"
API_TOKEN=`cat gitlab-token`
SCRIPT_ARTIFACT_NAME="$DIR_NAME/scripts.zip"
UI_ARTIFAT_NAME="$DIR_NAME/confluence-ui.zip"
WS_ARTIFACT_NAME="$DIR_NAME/confluence-ws.zip"

SCRIPT_PRJ_ID="31867"
UI_PRJ_ID="19621"
WS_PRJ_ID="19430"

mkdir "$DIR_NAME"

echo "Downloading Script"
curl "https://innersource.soprasteria.com/api/v4/projects/$SCRIPT_PRJ_ID/jobs/artifacts/$BRANCHE/download?job=package" -H "Private-Token: $API_TOKEN" -o $SCRIPT_ARTIFACT_NAME

echo "Downloading ui package"
curl "https://innersource.soprasteria.com/api/v4/projects/$UI_PRJ_ID/jobs/artifacts/$BRANCHE/download?job=build-prod" -H "Private-Token: $API_TOKEN" -o $UI_ARTIFAT_NAME

echo "Downloading ws package"
curl "https://innersource.soprasteria.com/api/v4/projects/$WS_PRJ_ID/jobs/artifacts/$BRANCHE/download?job=build" -H "Private-Token: $API_TOKEN" -o $WS_ARTIFACT_NAME

echo "All files are downloaded"
