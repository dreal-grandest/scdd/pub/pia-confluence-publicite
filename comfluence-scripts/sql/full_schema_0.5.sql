--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4
-- Dumped by pg_dump version 11.4

-- Started on 2020-02-18 21:28:29

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

-- DROP DATABASE if exists comfluence;
--
-- TOC entry 4593 (class 1262 OID 16394)
-- Name: comfluence; Type: DATABASE; Schema: -; Owner: -
--

-- CREATE DATABASE comfluence WITH TEMPLATE = template0 ENCODING = 'UTF8';



SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2 (class 3079 OID 35981)
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- TOC entry 4594 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_with_oids = false;

--
-- TOC entry 216 (class 1259 OID 46137)
-- Name: address; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.address (
    id bigint NOT NULL,
    city character varying(255),
    code integer,
    code_departement bigint,
    insee_code character varying(255),
    number character varying(255),
    street character varying(255)
);


--
-- TOC entry 253 (class 1259 OID 52350)
-- Name: advertiser; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.advertiser (
    id bigint NOT NULL,
    name character varying(255)
);


--
-- TOC entry 254 (class 1259 OID 52355)
-- Name: advertiser_departements; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.advertiser_departements (
    advertiser_id bigint NOT NULL,
    departements_code bigint NOT NULL
);


--
-- TOC entry 217 (class 1259 OID 46145)
-- Name: city; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.city (
    id bigint NOT NULL,
    geometry public.geometry,
    name character varying(255)
);


--
-- TOC entry 218 (class 1259 OID 46153)
-- Name: comment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.comment (
    id bigint NOT NULL,
    text character varying(511)
);


--
-- TOC entry 258 (class 1259 OID 52404)
-- Name: commune; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.commune (
    gid bigint NOT NULL,
    id character varying(24),
    statut character varying(24),
    insee_com character varying(5),
    insee_arr character varying(2),
    insee_dep character varying(3),
    insee_reg character varying(2),
    code_epci character varying(9),
    nom_com_m character varying(50),
    population bigint,
    type character varying(200),
    nom_com character varying(45),
    geom public.geometry(MultiPolygon)
);


--
-- TOC entry 260 (class 1259 OID 80411)
-- Name: commune_carto; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.commune_carto (
    gid integer NOT NULL,
    id character varying(24),
    statut character varying(24),
    insee_com character varying(5),
    insee_arr character varying(2),
    insee_dep character varying(3),
    insee_reg character varying(2),
    code_epci character varying(9),
    nom_com_m character varying(50),
    population bigint,
    type character varying(200),
    nom_com character varying(45),
    geom public.geometry(MultiPolygon)
);


--
-- TOC entry 259 (class 1259 OID 80409)
-- Name: commune_carto_gid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.commune_carto_gid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4595 (class 0 OID 0)
-- Dependencies: 259
-- Name: commune_carto_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.commune_carto_gid_seq OWNED BY public.commune_carto.gid;


--
-- TOC entry 257 (class 1259 OID 52402)
-- Name: commune_gid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.commune_gid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4596 (class 0 OID 0)
-- Dependencies: 257
-- Name: commune_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.commune_gid_seq OWNED BY public.commune.gid;


--
-- TOC entry 219 (class 1259 OID 46161)
-- Name: context; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.context (
    id bigint NOT NULL,
    location integer,
    population integer,
    town character varying(255),
    unit character varying(255)
);


--
-- TOC entry 220 (class 1259 OID 46169)
-- Name: context_environmental_constraints; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.context_environmental_constraints (
    context_id bigint NOT NULL,
    environmental_constraints_id bigint NOT NULL,
    environmental_constraints character varying(255)
);


--
-- TOC entry 221 (class 1259 OID 46172)
-- Name: control; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.control (
    id bigint NOT NULL,
    control_date date,
    geometry public.geometry,
    attendant_id bigint,
    controller_id bigint,
    departement_code bigint,
    statistics_control_id bigint
);


--
-- TOC entry 222 (class 1259 OID 46180)
-- Name: control_cities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.control_cities (
    control_id bigint NOT NULL,
    cities_gid bigint NOT NULL
);


--
-- TOC entry 223 (class 1259 OID 46183)
-- Name: control_dispositifs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.control_dispositifs (
    control_id bigint NOT NULL,
    dispositifs_id bigint NOT NULL
);


--
-- TOC entry 224 (class 1259 OID 46186)
-- Name: coordinates; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.coordinates (
    id bigint NOT NULL,
    latitude double precision,
    longitude double precision
);


--
-- TOC entry 225 (class 1259 OID 46191)
-- Name: courrier_template; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.courrier_template (
    id bigint NOT NULL,
    generated_name character varying(255),
    template_name character varying(255)
);


--
-- TOC entry 226 (class 1259 OID 46199)
-- Name: departement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.departement (
    code bigint NOT NULL,
    boundxmax double precision,
    boundxmin double precision,
    boundymax double precision,
    boundymin double precision,
    name character varying(255),
    territorial_direction_id bigint
);


--
-- TOC entry 227 (class 1259 OID 46204)
-- Name: description; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.description (
    id bigint NOT NULL,
    advertiser character varying(255),
    announcer character varying(255),
    dimension_id bigint,
    derogatory_type integer,
    advertiser_id bigint
);


--
-- TOC entry 228 (class 1259 OID 46212)
-- Name: description_positions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.description_positions (
    description_id bigint NOT NULL,
    positions integer
);


--
-- TOC entry 229 (class 1259 OID 46215)
-- Name: dimension; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dimension (
    id bigint NOT NULL,
    is_estimated boolean,
    size_id bigint
);


--
-- TOC entry 230 (class 1259 OID 46220)
-- Name: dispositif; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dispositif (
    id bigint NOT NULL,
    guidance integer,
    inserted_date date,
    operation_date timestamp without time zone,
    reference character varying(255),
    removed boolean NOT NULL,
    removed_date timestamp without time zone,
    status integer,
    type integer,
    address_id bigint,
    comment_id bigint,
    context_id bigint,
    coordinates_id bigint,
    description_id bigint
);


--
-- TOC entry 198 (class 1259 OID 18619)
-- Name: dispositif_information_list; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dispositif_information_list (
    dispositif_id bigint NOT NULL,
    information_list_id bigint NOT NULL
);


--
-- TOC entry 199 (class 1259 OID 35769)
-- Name: dispositif_informations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dispositif_informations (
    dispositif_id bigint NOT NULL,
    informations_id bigint NOT NULL
);


--
-- TOC entry 231 (class 1259 OID 46225)
-- Name: dispositif_infrigements; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dispositif_infrigements (
    dispositif_id bigint NOT NULL,
    infrigements_id bigint NOT NULL
);


--
-- TOC entry 197 (class 1259 OID 16569)
-- Name: dispostif_filter; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dispostif_filter (
    id bigint NOT NULL,
    actif boolean,
    label character varying(255)
);


--
-- TOC entry 232 (class 1259 OID 46228)
-- Name: environmental_constraint; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.environmental_constraint (
    id bigint NOT NULL,
    label bigint
);


--
-- TOC entry 252 (class 1259 OID 46348)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 233 (class 1259 OID 46233)
-- Name: information; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.information (
    id bigint NOT NULL,
    added_date timestamp without time zone,
    change_type integer,
    comment character varying(255),
    content_type character varying(255),
    opened_procedure boolean,
    original_file_name character varying(255),
    path character varying(255),
    dispositif_id bigint,
    procedure_id bigint,
    close_date timestamp without time zone,
    delay integer
);


--
-- TOC entry 234 (class 1259 OID 46241)
-- Name: infrigement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.infrigement (
    id bigint NOT NULL,
    natinf_code character varying(255),
    offense_nature character varying(255),
    implied_procedure_id bigint,
    code_article character varying(255),
    code_article_reprimand character varying(255)
);


--
-- TOC entry 261 (class 1259 OID 83926)
-- Name: layer; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.layer (
    id bigint NOT NULL,
    map character varying(255),
    name character varying(255),
    need_proxy boolean,
    output_format character varying(255),
    request character varying(255),
    service character varying(255),
    srs_name character varying(255),
    type_name character varying(255),
    url character varying(512),
    version character varying(255),
    categories character varying(255),
    used_for_restriction boolean,
    categories_namespace character varying(255)
);


--
-- TOC entry 262 (class 1259 OID 83950)
-- Name: layer_departements; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.layer_departements (
    layer_id bigint NOT NULL,
    departements_code bigint NOT NULL
);


--
-- TOC entry 236 (class 1259 OID 46262)
-- Name: parameter; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.parameter (
    id bigint NOT NULL,
    boolean_value boolean NOT NULL,
    date_time_value timestamp without time zone,
    date_value date,
    description character varying(255),
    double_value double precision,
    int_value integer,
    long_value bigint,
    name character varying(128) NOT NULL,
    string_value character varying(512)
);


--
-- TOC entry 235 (class 1259 OID 46260)
-- Name: parameter_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4597 (class 0 OID 0)
-- Dependencies: 235
-- Name: parameter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.parameter_id_seq OWNED BY public.parameter.id;


--
-- TOC entry 238 (class 1259 OID 46273)
-- Name: photo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.photo (
    id bigint NOT NULL,
    active boolean,
    content_type character varying(255),
    id_dispositif bigint,
    is_archived boolean,
    original_file_name character varying(255),
    path character varying(255),
    path_thumb character varying(255),
    photo_date timestamp without time zone,
    photo_type integer,
    sha256 character varying(255)
);


--
-- TOC entry 237 (class 1259 OID 46271)
-- Name: photo_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.photo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4598 (class 0 OID 0)
-- Dependencies: 237
-- Name: photo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.photo_id_seq OWNED BY public.photo.id;


--
-- TOC entry 239 (class 1259 OID 46282)
-- Name: procedure; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.procedure (
    id bigint NOT NULL,
    delay integer,
    groupe character varying(255),
    label character varying(255),
    start boolean,
    is_pv boolean
);


--
-- TOC entry 240 (class 1259 OID 46290)
-- Name: procedure_courrier_templates; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.procedure_courrier_templates (
    procedure_id bigint NOT NULL,
    courrier_templates_id bigint NOT NULL
);


--
-- TOC entry 241 (class 1259 OID 46293)
-- Name: procedure_dispositif_filters; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.procedure_dispositif_filters (
    procedure_id bigint NOT NULL,
    dispositif_filters character varying(255)
);


--
-- TOC entry 215 (class 1259 OID 44959)
-- Name: procedure_doc_template_list; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.procedure_doc_template_list (
    procedure_id bigint NOT NULL,
    doc_template_list_id bigint NOT NULL
);


--
-- TOC entry 242 (class 1259 OID 46296)
-- Name: procedure_incompatible_procedures; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.procedure_incompatible_procedures (
    procedure_id bigint NOT NULL,
    incompatible_procedures_id bigint NOT NULL
);


--
-- TOC entry 243 (class 1259 OID 46299)
-- Name: procedure_next_procedures; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.procedure_next_procedures (
    procedure_id bigint NOT NULL,
    next_procedures_id bigint NOT NULL
);


--
-- TOC entry 245 (class 1259 OID 46304)
-- Name: roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255)
);


--
-- TOC entry 244 (class 1259 OID 46302)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4599 (class 0 OID 0)
-- Dependencies: 244
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- TOC entry 246 (class 1259 OID 46310)
-- Name: size; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.size (
    id bigint NOT NULL,
    category integer,
    device_ground_distance double precision,
    height double precision,
    mast_width double precision,
    protrusion_ground_distance double precision,
    width double precision
);


--
-- TOC entry 247 (class 1259 OID 46315)
-- Name: statistics_control; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.statistics_control (
    id bigint NOT NULL,
    controlled_devices integer,
    duration bigint,
    start_date timestamp without time zone,
    traveled_distance double precision
);


--
-- TOC entry 255 (class 1259 OID 52358)
-- Name: territorial_direction; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.territorial_direction (
    id bigint NOT NULL,
    address character varying(255),
    city character varying(255),
    direction character varying(255),
    director character varying(255),
    fax_number character varying(255),
    first_name_signing_authority character varying(255),
    last_name_signing_authority character varying(255),
    phone_number character varying(255),
    pole character varying(255),
    prefecture character varying(255),
    service character varying(255),
    signing_authority character varying(255)
);


--
-- TOC entry 249 (class 1259 OID 46322)
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    deleted boolean,
    enabled boolean NOT NULL,
    first_name character varying(512) NOT NULL,
    last_name character varying(512) NOT NULL,
    mail character varying(512) NOT NULL,
    password character varying(512) NOT NULL,
    pwd_encrypted boolean,
    departement_code bigint
);


--
-- TOC entry 248 (class 1259 OID 46320)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4600 (class 0 OID 0)
-- Dependencies: 248
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 250 (class 1259 OID 46331)
-- Name: users_role_list; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users_role_list (
    users_id bigint NOT NULL,
    role_list_id bigint NOT NULL
);


--
-- TOC entry 256 (class 1259 OID 52366)
-- Name: users_roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users_roles (
    users_id bigint NOT NULL,
    roles_id bigint NOT NULL
);


--
-- TOC entry 251 (class 1259 OID 46336)
-- Name: users_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users_token (
    users_id bigint NOT NULL,
    token character varying(1024)
);


--
-- TOC entry 4345 (class 2604 OID 52407)
-- Name: commune gid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.commune ALTER COLUMN gid SET DEFAULT nextval('public.commune_gid_seq'::regclass);


--
-- TOC entry 4346 (class 2604 OID 80414)
-- Name: commune_carto gid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.commune_carto ALTER COLUMN gid SET DEFAULT nextval('public.commune_carto_gid_seq'::regclass);


--
-- TOC entry 4341 (class 2604 OID 46265)
-- Name: parameter id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parameter ALTER COLUMN id SET DEFAULT nextval('public.parameter_id_seq'::regclass);


--
-- TOC entry 4342 (class 2604 OID 46276)
-- Name: photo id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photo ALTER COLUMN id SET DEFAULT nextval('public.photo_id_seq'::regclass);


--
-- TOC entry 4343 (class 2604 OID 46307)
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- TOC entry 4344 (class 2604 OID 46325)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 4356 (class 2606 OID 46144)
-- Name: address address_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- TOC entry 4404 (class 2606 OID 52354)
-- Name: advertiser advertiser_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.advertiser
    ADD CONSTRAINT advertiser_pkey PRIMARY KEY (id);


--
-- TOC entry 4358 (class 2606 OID 46152)
-- Name: city city_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id);


--
-- TOC entry 4360 (class 2606 OID 46160)
-- Name: comment comment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT comment_pkey PRIMARY KEY (id);


--
-- TOC entry 4414 (class 2606 OID 80416)
-- Name: commune_carto commune_carto_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.commune_carto
    ADD CONSTRAINT commune_carto_pkey PRIMARY KEY (gid);


--
-- TOC entry 4411 (class 2606 OID 52409)
-- Name: commune commune_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.commune
    ADD CONSTRAINT commune_pkey PRIMARY KEY (gid);


--
-- TOC entry 4362 (class 2606 OID 46168)
-- Name: context context_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.context
    ADD CONSTRAINT context_pkey PRIMARY KEY (id);


--
-- TOC entry 4366 (class 2606 OID 46179)
-- Name: control control_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.control
    ADD CONSTRAINT control_pkey PRIMARY KEY (id);


--
-- TOC entry 4368 (class 2606 OID 46190)
-- Name: coordinates coordinates_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.coordinates
    ADD CONSTRAINT coordinates_pkey PRIMARY KEY (id);


--
-- TOC entry 4370 (class 2606 OID 46198)
-- Name: courrier_template courrier_template_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.courrier_template
    ADD CONSTRAINT courrier_template_pkey PRIMARY KEY (id);


--
-- TOC entry 4372 (class 2606 OID 46203)
-- Name: departement departement_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.departement
    ADD CONSTRAINT departement_pkey PRIMARY KEY (code);


--
-- TOC entry 4374 (class 2606 OID 46211)
-- Name: description description_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.description
    ADD CONSTRAINT description_pkey PRIMARY KEY (id);


--
-- TOC entry 4376 (class 2606 OID 46219)
-- Name: dimension dimension_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dimension
    ADD CONSTRAINT dimension_pkey PRIMARY KEY (id);


--
-- TOC entry 4378 (class 2606 OID 46224)
-- Name: dispositif dispositif_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispositif
    ADD CONSTRAINT dispositif_pkey PRIMARY KEY (id);


--
-- TOC entry 4348 (class 2606 OID 16573)
-- Name: dispostif_filter dispostif_filter_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispostif_filter
    ADD CONSTRAINT dispostif_filter_pkey PRIMARY KEY (id);


--
-- TOC entry 4380 (class 2606 OID 46232)
-- Name: environmental_constraint environmental_constraint_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.environmental_constraint
    ADD CONSTRAINT environmental_constraint_pkey PRIMARY KEY (id);


--
-- TOC entry 4382 (class 2606 OID 46240)
-- Name: information information_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.information
    ADD CONSTRAINT information_pkey PRIMARY KEY (id);


--
-- TOC entry 4384 (class 2606 OID 46248)
-- Name: infrigement infrigement_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.infrigement
    ADD CONSTRAINT infrigement_pkey PRIMARY KEY (id);


--
-- TOC entry 4416 (class 2606 OID 83933)
-- Name: layer layer_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.layer
    ADD CONSTRAINT layer_pkey PRIMARY KEY (id);


--
-- TOC entry 4386 (class 2606 OID 46270)
-- Name: parameter parameter_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parameter
    ADD CONSTRAINT parameter_pkey PRIMARY KEY (id);


--
-- TOC entry 4390 (class 2606 OID 46281)
-- Name: photo photo_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photo
    ADD CONSTRAINT photo_pkey PRIMARY KEY (id);


--
-- TOC entry 4392 (class 2606 OID 46289)
-- Name: procedure procedure_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.procedure
    ADD CONSTRAINT procedure_pkey PRIMARY KEY (id);


--
-- TOC entry 4394 (class 2606 OID 46309)
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 4396 (class 2606 OID 46314)
-- Name: size size_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.size
    ADD CONSTRAINT size_pkey PRIMARY KEY (id);


--
-- TOC entry 4398 (class 2606 OID 46319)
-- Name: statistics_control statistics_control_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.statistics_control
    ADD CONSTRAINT statistics_control_pkey PRIMARY KEY (id);


--
-- TOC entry 4406 (class 2606 OID 52365)
-- Name: territorial_direction territorial_direction_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.territorial_direction
    ADD CONSTRAINT territorial_direction_pkey PRIMARY KEY (id);


--
-- TOC entry 4364 (class 2606 OID 46343)
-- Name: context_environmental_constraints uk_9m95yq1qqgck0hiej3r053se; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.context_environmental_constraints
    ADD CONSTRAINT uk_9m95yq1qqgck0hiej3r053se UNIQUE (environmental_constraints_id);


--
-- TOC entry 4352 (class 2606 OID 35844)
-- Name: dispositif_informations uk_d6lrk9j3e62wl39tjrcjs4u70; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispositif_informations
    ADD CONSTRAINT uk_d6lrk9j3e62wl39tjrcjs4u70 UNIQUE (informations_id);


--
-- TOC entry 4388 (class 2606 OID 46347)
-- Name: parameter uk_ll10bbifmynb347ajltx7bi60; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parameter
    ADD CONSTRAINT uk_ll10bbifmynb347ajltx7bi60 UNIQUE (name);


--
-- TOC entry 4350 (class 2606 OID 18669)
-- Name: dispositif_information_list uk_ooy3rn2vnj4x0hnegi2glkd0y; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispositif_information_list
    ADD CONSTRAINT uk_ooy3rn2vnj4x0hnegi2glkd0y UNIQUE (information_list_id);


--
-- TOC entry 4400 (class 2606 OID 46330)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 4402 (class 2606 OID 46335)
-- Name: users_role_list users_role_list_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_role_list
    ADD CONSTRAINT users_role_list_pkey PRIMARY KEY (users_id, role_list_id);


--
-- TOC entry 4408 (class 2606 OID 52370)
-- Name: users_roles users_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT users_roles_pkey PRIMARY KEY (users_id, roles_id);


--
-- TOC entry 4412 (class 1259 OID 83917)
-- Name: commune_carto_geom_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX commune_carto_geom_idx ON public.commune_carto USING gist (geom);


--
-- TOC entry 4409 (class 1259 OID 80395)
-- Name: commune_geom_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX commune_geom_idx ON public.commune USING gist (geom);


--
-- TOC entry 4427 (class 2606 OID 46390)
-- Name: control_dispositifs fk1xqd2fgy6b7hbwrpx4xwokr75; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.control_dispositifs
    ADD CONSTRAINT fk1xqd2fgy6b7hbwrpx4xwokr75 FOREIGN KEY (dispositifs_id) REFERENCES public.dispositif(id);


--
-- TOC entry 4438 (class 2606 OID 46440)
-- Name: dispositif_infrigements fk2csiegdek290gf769tdr1b1m0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispositif_infrigements
    ADD CONSTRAINT fk2csiegdek290gf769tdr1b1m0 FOREIGN KEY (infrigements_id) REFERENCES public.infrigement(id);


--
-- TOC entry 4452 (class 2606 OID 46520)
-- Name: users_role_list fk2kd1ge8s04volll17utt44lco; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_role_list
    ADD CONSTRAINT fk2kd1ge8s04volll17utt44lco FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- TOC entry 4436 (class 2606 OID 46430)
-- Name: dispositif fk3ryvbm6r2gnvusvj8wf15wna8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispositif
    ADD CONSTRAINT fk3ryvbm6r2gnvusvj8wf15wna8 FOREIGN KEY (coordinates_id) REFERENCES public.coordinates(id);


--
-- TOC entry 4425 (class 2606 OID 80404)
-- Name: control_cities fk4b5kcuojrmj8vnahxpsqfndp7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.control_cities
    ADD CONSTRAINT fk4b5kcuojrmj8vnahxpsqfndp7 FOREIGN KEY (cities_gid) REFERENCES public.commune(gid);


--
-- TOC entry 4441 (class 2606 OID 46455)
-- Name: information fk4k18p1jac64a5rxbplrp2hv4i; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.information
    ADD CONSTRAINT fk4k18p1jac64a5rxbplrp2hv4i FOREIGN KEY (procedure_id) REFERENCES public.procedure(id);


--
-- TOC entry 4440 (class 2606 OID 46450)
-- Name: information fk5bhdxwn0j9141ayf0f2lacdmt; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.information
    ADD CONSTRAINT fk5bhdxwn0j9141ayf0f2lacdmt FOREIGN KEY (dispositif_id) REFERENCES public.dispositif(id);


--
-- TOC entry 4458 (class 2606 OID 83953)
-- Name: layer_departements fk5sp3c4yk9lf06ghy6ku22w6gj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.layer_departements
    ADD CONSTRAINT fk5sp3c4yk9lf06ghy6ku22w6gj FOREIGN KEY (departements_code) REFERENCES public.departement(code);


--
-- TOC entry 4434 (class 2606 OID 46420)
-- Name: dispositif fk60x5e7hax9otqveepc9xpooj1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispositif
    ADD CONSTRAINT fk60x5e7hax9otqveepc9xpooj1 FOREIGN KEY (comment_id) REFERENCES public.comment(id);


--
-- TOC entry 4430 (class 2606 OID 52386)
-- Name: description fk6gqny6fv40quoo0l13x4fc550; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.description
    ADD CONSTRAINT fk6gqny6fv40quoo0l13x4fc550 FOREIGN KEY (advertiser_id) REFERENCES public.advertiser(id);


--
-- TOC entry 4419 (class 2606 OID 46360)
-- Name: control fk6s3266o8nxxkgvh93rop44dhf; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.control
    ADD CONSTRAINT fk6s3266o8nxxkgvh93rop44dhf FOREIGN KEY (attendant_id) REFERENCES public.users(id);


--
-- TOC entry 4418 (class 2606 OID 46355)
-- Name: context_environmental_constraints fk7lq1r1ny95p87ag2m2pkrm4l1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.context_environmental_constraints
    ADD CONSTRAINT fk7lq1r1ny95p87ag2m2pkrm4l1 FOREIGN KEY (context_id) REFERENCES public.context(id);


--
-- TOC entry 4453 (class 2606 OID 46525)
-- Name: users_token fk7lyqeenu4cm2rn05rat4ee8x6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_token
    ADD CONSTRAINT fk7lyqeenu4cm2rn05rat4ee8x6 FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- TOC entry 4450 (class 2606 OID 46510)
-- Name: users fk7omfbr25lxqjsfy7jbpd09ywa; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk7omfbr25lxqjsfy7jbpd09ywa FOREIGN KEY (departement_code) REFERENCES public.departement(code);


--
-- TOC entry 4449 (class 2606 OID 46505)
-- Name: procedure_next_procedures fk7uhvgjdb5t9lo33v4y4qd3r7a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.procedure_next_procedures
    ADD CONSTRAINT fk7uhvgjdb5t9lo33v4y4qd3r7a FOREIGN KEY (procedure_id) REFERENCES public.procedure(id);

--
-- TOC entry 4432 (class 2606 OID 46410)
-- Name: dimension fk9awirmfbt68i05nus6g7j1vc1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dimension
    ADD CONSTRAINT fk9awirmfbt68i05nus6g7j1vc1 FOREIGN KEY (size_id) REFERENCES public.size(id);


--
-- TOC entry 4420 (class 2606 OID 46365)
-- Name: control fk9pi5s7g1faob3saepqimng78f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.control
    ADD CONSTRAINT fk9pi5s7g1faob3saepqimng78f FOREIGN KEY (controller_id) REFERENCES public.users(id);


--
-- TOC entry 4439 (class 2606 OID 46445)
-- Name: dispositif_infrigements fka1iq16kx75m72tlu3fmfx79lw; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispositif_infrigements
    ADD CONSTRAINT fka1iq16kx75m72tlu3fmfx79lw FOREIGN KEY (dispositif_id) REFERENCES public.dispositif(id);


--
-- TOC entry 4456 (class 2606 OID 52391)
-- Name: users_roles fka62j07k5mhgifpp955h37ponj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT fka62j07k5mhgifpp955h37ponj FOREIGN KEY (roles_id) REFERENCES public.roles(id);


--
-- TOC entry 4417 (class 2606 OID 46350)
-- Name: context_environmental_constraints fkbd9lt22c7i3n4uh8kxqrtbhpa; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.context_environmental_constraints
    ADD CONSTRAINT fkbd9lt22c7i3n4uh8kxqrtbhpa FOREIGN KEY (environmental_constraints_id) REFERENCES public.environmental_constraint(id);


--
-- TOC entry 4422 (class 2606 OID 46375)
-- Name: control fkbdpaqg5bl23wlrxsftwsqfwd6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.control
    ADD CONSTRAINT fkbdpaqg5bl23wlrxsftwsqfwd6 FOREIGN KEY (statistics_control_id) REFERENCES public.statistics_control(id);


--
-- TOC entry 4428 (class 2606 OID 52381)
-- Name: departement fkcswl1crbjoj620xysjmpul4ka; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.departement
    ADD CONSTRAINT fkcswl1crbjoj620xysjmpul4ka FOREIGN KEY (territorial_direction_id) REFERENCES public.territorial_direction(id);


--
-- TOC entry 4442 (class 2606 OID 46460)
-- Name: infrigement fkdrx61j9bp2aescwrx1ptdhr3n; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.infrigement
    ADD CONSTRAINT fkdrx61j9bp2aescwrx1ptdhr3n FOREIGN KEY (implied_procedure_id) REFERENCES public.procedure(id);


--
-- TOC entry 4454 (class 2606 OID 52371)
-- Name: advertiser_departements fkdvc4y7k7kut02e3lyngmv8owl; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.advertiser_departements
    ADD CONSTRAINT fkdvc4y7k7kut02e3lyngmv8owl FOREIGN KEY (departements_code) REFERENCES public.departement(code);


--
-- TOC entry 4447 (class 2606 OID 46495)
-- Name: procedure_incompatible_procedures fkel6ypo1aj72f9dyxnnl4g6ihm; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.procedure_incompatible_procedures
    ADD CONSTRAINT fkel6ypo1aj72f9dyxnnl4g6ihm FOREIGN KEY (procedure_id) REFERENCES public.procedure(id);


--
-- TOC entry 4424 (class 2606 OID 46385)
-- Name: control_cities fkgdd752242ha5m33cq6wmg0uc4; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.control_cities
    ADD CONSTRAINT fkgdd752242ha5m33cq6wmg0uc4 FOREIGN KEY (control_id) REFERENCES public.control(id);


--
-- TOC entry 4444 (class 2606 OID 46480)
-- Name: procedure_courrier_templates fkhcbu6m5ls0fjo5fu35vnm147n; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.procedure_courrier_templates
    ADD CONSTRAINT fkhcbu6m5ls0fjo5fu35vnm147n FOREIGN KEY (procedure_id) REFERENCES public.procedure(id);


--
-- TOC entry 4435 (class 2606 OID 46425)
-- Name: dispositif fkhdh3upv4nk8p1oiu91flibni7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispositif
    ADD CONSTRAINT fkhdh3upv4nk8p1oiu91flibni7 FOREIGN KEY (context_id) REFERENCES public.context(id);


--
-- TOC entry 4445 (class 2606 OID 46485)
-- Name: procedure_dispositif_filters fkhdiel2sqk80cj14pdtd1lpnrw; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.procedure_dispositif_filters
    ADD CONSTRAINT fkhdiel2sqk80cj14pdtd1lpnrw FOREIGN KEY (procedure_id) REFERENCES public.procedure(id);


--
-- TOC entry 4446 (class 2606 OID 46490)
-- Name: procedure_incompatible_procedures fkjjmsgqjs5mu8pd4gfu0f47hph; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.procedure_incompatible_procedures
    ADD CONSTRAINT fkjjmsgqjs5mu8pd4gfu0f47hph FOREIGN KEY (incompatible_procedures_id) REFERENCES public.procedure(id);


--
-- TOC entry 4443 (class 2606 OID 46475)
-- Name: procedure_courrier_templates fkjv88gbrap8xjple2edec0uvc2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.procedure_courrier_templates
    ADD CONSTRAINT fkjv88gbrap8xjple2edec0uvc2 FOREIGN KEY (courrier_templates_id) REFERENCES public.courrier_template(id);


--
-- TOC entry 4431 (class 2606 OID 46405)
-- Name: description_positions fklaxq1ny6mte27fhafvsfdj5pj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.description_positions
    ADD CONSTRAINT fklaxq1ny6mte27fhafvsfdj5pj FOREIGN KEY (description_id) REFERENCES public.description(id);


--
-- TOC entry 4451 (class 2606 OID 46515)
-- Name: users_role_list fkmiyd49pg34bwpevl3bsil347p; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_role_list
    ADD CONSTRAINT fkmiyd49pg34bwpevl3bsil347p FOREIGN KEY (role_list_id) REFERENCES public.roles(id);


--
-- TOC entry 4457 (class 2606 OID 52396)
-- Name: users_roles fkml90kef4w2jy7oxyqv742tsfc; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT fkml90kef4w2jy7oxyqv742tsfc FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- TOC entry 4448 (class 2606 OID 46500)
-- Name: procedure_next_procedures fkn1yosb4g99lyldnek2tkyicwl; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.procedure_next_procedures
    ADD CONSTRAINT fkn1yosb4g99lyldnek2tkyicwl FOREIGN KEY (next_procedures_id) REFERENCES public.procedure(id);


--
-- TOC entry 4437 (class 2606 OID 46435)
-- Name: dispositif fknmd9gcnminircqhv9j63mur6w; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispositif
    ADD CONSTRAINT fknmd9gcnminircqhv9j63mur6w FOREIGN KEY (description_id) REFERENCES public.description(id);


--
-- TOC entry 4455 (class 2606 OID 52376)
-- Name: advertiser_departements fknmso3qiolioycmygs6hjjfy0x; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.advertiser_departements
    ADD CONSTRAINT fknmso3qiolioycmygs6hjjfy0x FOREIGN KEY (advertiser_id) REFERENCES public.advertiser(id);


--
-- TOC entry 4429 (class 2606 OID 46400)
-- Name: description fkomfoqmyfb9xpahj8bx50irxgs; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.description
    ADD CONSTRAINT fkomfoqmyfb9xpahj8bx50irxgs FOREIGN KEY (dimension_id) REFERENCES public.dimension(id);


--
-- TOC entry 4421 (class 2606 OID 46370)
-- Name: control fkorre85ijifkq9gchqy9mpbhb8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.control
    ADD CONSTRAINT fkorre85ijifkq9gchqy9mpbhb8 FOREIGN KEY (departement_code) REFERENCES public.departement(code);


--
-- TOC entry 4426 (class 2606 OID 46395)
-- Name: control_dispositifs fkrctgsjtor5a3glsgcj4ucfi7v; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.control_dispositifs
    ADD CONSTRAINT fkrctgsjtor5a3glsgcj4ucfi7v FOREIGN KEY (control_id) REFERENCES public.control(id);


--
-- TOC entry 4459 (class 2606 OID 83958)
-- Name: layer_departements fktb5etatrjj7rov34u1y86u0h; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.layer_departements
    ADD CONSTRAINT fktb5etatrjj7rov34u1y86u0h FOREIGN KEY (layer_id) REFERENCES public.layer(id);


--
-- TOC entry 4433 (class 2606 OID 46415)
-- Name: dispositif fkti39ooc7hyqhcc3pg4mj5s970; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispositif
    ADD CONSTRAINT fkti39ooc7hyqhcc3pg4mj5s970 FOREIGN KEY (address_id) REFERENCES public.address(id);


-- Completed on 2020-02-18 21:28:30

--
-- PostgreSQL database dump complete
--

