-- noinspection SqlNoDataSourceInspectionForFile

INSERT INTO territorial_direction(
	id, address, city, direction, director, fax_number, first_name_signing_authority, last_name_signing_authority, phone_number, pole, prefecture, service, signing_authority)
	VALUES 
    (0, 'adresse', 'ville', 'direction', 'Directeur', 'fax:03030303', 'Signataire prénom', 'Signataire nom', 'tel: 0303033003', 'pole', 'préfecture', 'service', 'autorité signatrice'),
	(1, 'adresse', 'ville', 'direction', 'Directeur', 'fax:03030303', 'Signataire prénom', 'Signataire nom', 'tel: 0303033003', 'pole', 'préfecture', 'service', 'autorité signatrice'),
	(2, 'adresse', 'ville', 'direction', 'Directeur', 'fax:03030303', 'Signataire prénom', 'Signataire nom', 'tel: 0303033003', 'pole', 'préfecture', 'service', 'autorité signatrice'),
	(3, 'adresse', 'ville', 'direction', 'Directeur', 'fax:03030303', 'Signataire prénom', 'Signataire nom', 'tel: 0303033003', 'pole', 'préfecture', 'service', 'autorité signatrice'),
	(4, 'adresse', 'ville', 'direction', 'Directeur', 'fax:03030303', 'Signataire prénom', 'Signataire nom', 'tel: 0303033003', 'pole', 'préfecture', 'service', 'autorité signatrice'),
	(5, 'adresse', 'ville', 'direction', 'Directeur', 'fax:03030303', 'Signataire prénom', 'Signataire nom', 'tel: 0303033003', 'pole', 'préfecture', 'service', 'autorité signatrice'),
	(6, 'adresse', 'ville', 'direction', 'Directeur', 'fax:03030303', 'Signataire prénom', 'Signataire nom', 'tel: 0303033003', 'pole', 'préfecture', 'service', 'autorité signatrice'),
	(7, 'adresse', 'ville', 'direction', 'Directeur', 'fax:03030303', 'Signataire prénom', 'Signataire nom', 'tel: 0303033003', 'pole', 'préfecture', 'service', 'autorité signatrice'),
	(8, 'adresse', 'ville', 'direction', 'Directeur', 'fax:03030303', 'Signataire prénom', 'Signataire nom', 'tel: 0303033003', 'pole', 'préfecture', 'service', 'autorité signatrice'),
	(9, 'adresse', 'ville', 'direction', 'Directeur', 'fax:03030303', 'Signataire prénom', 'Signataire nom', 'tel: 0303033003', 'pole', 'préfecture', 'service', 'autorité signatrice');

INSERT INTO departement(
code, name, boundxmin, boundymin, boundxmax, boundymax, territorial_direction_id)
VALUES
(68, 'Haut-Rhin', 985717.12603459157980978,6709168.63384428434073925,1047308.59965480153914541,6811151.61733861733227968,0),
(67, 'Bas-Rhin', 986163.4410608250182122,6788277.97224415559321642,1084129.58931905752979219,6898294.62621069047600031,1),
(57, 'Moselle', 908504.62649621232412755,6831570.5297887958586216,1018521.28046274685766548,6941364.02624221425503492,2),
(54, 'Meurthe et Moselle', 874807.84201559028588235,6808362.1484246589243412,1006693.93226756167132407,6946273.49153078161180019,3),
(88, 'Vosges', 876146.78709429048467427,6751791.71884957514703274,1012272.87009547918569297,6833690.52616340480744839,4),
(52, 'Haute-Marne', 818795.30622329784091562,6720772.82452635280787945,917207.76950776390731335,6846633.66192417312413454,5),
(55, 'Meuse', 836424.74975951737724245,6812378.98366075940430164,911182.51665361283812672,6952745.05941116530448198,6),
(10,'Aube', 726184.93827986612450331,6756924.34165125899016857,839772.11245626793242991,6847749.44948975741863251,7),
(51, 'Marne', 723953.36314869916532189,6822867.38677724450826645,852268.93319080327637494,6925519.84281092789024115,8),
(8, 'Ardennes', 771515.28100111254025251,6901953.14775305893272161,874542.18987764185294509,7010825.55500556156039238,9);


INSERT INTO users(
	id, enabled, mail, password, first_name, last_name, departement_code, pwd_encrypted, deleted)
	VALUES
	(1, true, 'dimitri.reynie@soprasteria.com', 'dreynie', 'Dimitri', 'Reynié', 67, false, false), -- pw = dreynie
	(2, true, 'olivier.toth@soprasteria.com', 'ototh', 'Olivier', 'Toth', 68, false, false), -- pw = ototh
	(3, true, 'lucie.hubert@soprasteria.com', 'lhubert', 'Lucie', 'Hubert', 8, false, false), -- pw = lhubert
	(4, true, 'julia.galvez@vosges.gouv.fr', 'jgalvez', 'Julia', 'Galvez', 88, false, false), -- pw = jgalvez
	(5, true, 'guillaume.vaney@developpement-durable.gouv.fr', 'gvaney', 'Guillaume', 'Vaney', 55, false, false), -- pw = gvaney
	(6, true, 'francois.mathonnet@developpement-durable.gouv.fr', 'fmathonnet', 'Francois', 'Mathonnet', 10, false, false), -- pw = fmathonnet
	(7,true,'jacques.lantenois@ardennes.gouv.fr','jlantenois','Jacques','LANTENOIS',8,false, false),
    (8,true,'jean-michel.lamy@developpement-durable.gouv.fr','jlamy','Jean-Michel','LAMY',10,false, false),
    (9,true,'isabelle.loreaux@marne.gouv.fr','iloreaux','Isabelle','LOREAUX',51,false, false),
    (10,true,'patrick.luyer@marne.gouv.fr','pluyer','Patrick','LUYER',51,false, false),
    (11,true,'diane.demolliere@marne.gouv.fr','ddemolliere','Diane','DEMOLLIERE',51,false, false),
    (12,true,'dominique.cornot@haute-marne.gouv.fr','dcornot','Dominique','CORNOT',52,false, false),
    (13,true,'frederic.mauffre@haute-marne.gouv.fr','fmauffre','Frédéric','MAUFFRE',52,false, false),
    (14,true,'lionel.beaufils@haute-marne.gouv.fr','lbeaufils','Lionel','BEAUFILS',52,false, false),
    (15,true,'lucie.mathieu@haute-marne.gouv.fr','lmathieu','Lucie','MATHIEU',52,false, false),
    (16,true,'nathalie.cael@meurthe-et-moselle.gouv.fr','ncael','Nathalie','CAEL',54,false, false),
    (17,true,'solange.chognot@meurthe-et-moselle.gouv.fr','schognot','Solange','CHOGNOT',54,false, false),
    (18,true,'armelle.morlot@meurthe-et-moselle.gouv.fr','amorlot','Armelle','MORLOT',54,false, false),
    (19,true,'francine.pascual@meuse.gouv.fr','fpascual','Francine','PASCUAL',55,false, false),
    (20,true,'laurent.simonin@meuse.gouv.fr','lsimonin','Laurent','SIMONIN',55,false, false),
    (21,true,'stephanie.courtois@moselle.gouv.fr','scourtois','Stéphanie','COURTOIS',57,false, false),
    (22,true,'baghdad.seguer@moselle.gouv.fr','bseguer','Baghdad','SEGUER',57,false, false),
    (23,true,'michel.klam@moselle.gouv.fr','mklam','Michel','KLAM',57,false, false),
    (24,true,'claudine.burtin@bas-rhin.gouv.fr','cburtin','Claudine','BURTIN',67,false, false),
    (25,true,'rene.uhring@bas-rhin.gouv.fr','ruhring','René','UHRING',67,false, false),
    (26,true,'patrick.marvillier@bas-rhin.gouv.fr','pmarvillier','Patrick','MARVILLIER',67,false, false),
    (27,true,'raphael.bauche@haut-rhin.gouv.fr','rbauche','Raphael','BAUCHE',68,false, false),
    (28,true,'soledad.joos@haut-rhin.gouv.fr','sjoos','Solédad','JOOS',68,false, false),
    (29,true,'odile.prevot@haut-rhin.gouv.fr','oprevot','Odile','PREVOT',68,false, false),
    (30,true,'celine.elsaesser@haut-rhin.gouv.fr','celsaesser','Céline','ELSAESSER',68,false, false),
    (31,true,'nicolas.joly@vosges.gouv.fr','njoly','Nicolas','JOLY',88,false, false),
    (32,true,'cathy.christal@vosges.gouv.fr','cchristal','Cathy','CHRISTAL',88,false, false),
    (33,true,'michel.marchal@vosges.gouv.fr','mmarchal','Michel','MARCHAL',88,false, false),
    (34,true,'alain.remy@vosges.gouv.fr','aremy','Alain','REMY',88,false, false),
    (35,true,'sebastien.gaugry@vosges.gouv.fr','sgaugry','Sébastien','GAUGRY',88,false, false),
    (36,true,'christel.poinas@developpement-durable.gouv.fr','cpoinas','Christel','POINAS',88,false, false);

ALTER SEQUENCE users_id_seq RESTART WITH 37;

