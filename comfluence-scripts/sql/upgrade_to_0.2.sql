

insert into "procedure"
	(id, delay, "label")
	values (1,	15,	'Courrier amiable 15j'),
	(2, 30, 'Courrier amiable 30j');

insert into procedure_incompatible_procedures
	(procedure_id, incompatible_procedures_id)
	values (2 , 1);
insert into procedure_incompatible_procedures
	(procedure_id, incompatible_procedures_id)
	values (1 , 2);

